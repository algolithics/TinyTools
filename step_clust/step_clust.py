# compare with kmeans init ++
# add center dist measure for stop detection 

# MAJOR IMPORTS
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from scipy.spatial import distance_matrix
from math import sqrt,log

from numpy import argsort, var as np_var, mean as np_mean, std as np_std, sum as np_sum, min as np_min, max as np_max, sqrt as np_sqrt

from numpy import array as np_array, intersect1d as np_intersect1d , concatenate as np_concatenate, subtract as np_subtract, stack as np_stack, setdiff1d as np_setdiff1d, append as np_append


# TESTING PURPOSE IMPORTS
import matplotlib.pyplot as plt
import pickle



# based on
# dist distribution and derivatives
# clusters share %
# clusters sigma 


def step_clust(x_orig, max_clusters=5, min_clust_size=5, _debug=False, _enforceAssign=True): #, _merge=1): #_err_rate=1/128, 
	
	x=x_orig.copy() # work on copy
	
	# validate / overwrite params 
	min_clust_size=min_clust_size if min_clust_size>3 else 3
	max_clusters=min( 1 if max_clusters<1 else max_clusters, len(x_orig)//min_clust_size )
	
	
	_dim_sigma_multip=2**(len(x_orig[0])-1) # FOR DIM > 1 sigma containes << 68% results and factor of 2**DIM should increase to cover that more or less up to DIM=10
	_sigm_m= 1.5 # _sigm_m *  _dim_sigma_multip will give 3 sigma for dim = 2
	
	_ij=np_stack(range(len(x))) # ids/ indexes of points/features 
	_labels=np_stack(_ij) # labels to be calc at the end 
	centers,sigmas =(), ()
	clusters={}
	# total_removed=0
	iiter=0 # key for cluster 
	n=max_clusters
	
	potential_outliers=np_array([]) #[] # added after each fit, removed when fitted later : used for count leftover 
	
	while n>0: 
		if _debug: print('n',n)
		iiter+=1
		xlen=len(x)
		leftover=len(potential_outliers) #int(round(total_removed*_err_rate,0)) # protect better from matching leftover to final small clusters 
		
		if xlen <min_clust_size: # or xlen-leftover<4: 
			if _debug: print('xlen <min_clust_size', xlen, min_clust_size)
			break
			
		dm=distance_matrix(x, x) # DISTANCE MATRIX
		
		###########################################
		# SEARCHING MOST CONSISTENT CLUSTER
		# sum of sorted dist over max_p range is min
		###########################################
		minp=max(min_clust_size, (xlen-leftover)//max(n,2) ) # not less then mmin
		
		max_p=min(2*minp, xlen) # for more proper center finding 
		
		if _debug: print(f'minp {minp} max_p {max_p} xlen {xlen} leftover {leftover} max(n,2) {max(n,2)}' ) 
		
		sorted_dm=[]
		sorted_dm_jk=[]
		bestvv=None
		bestii=-1
		
		for ii,dmi in enumerate( dm):
		
			jk=argsort(dmi) # SORTED DIST
			
			sorted_dm.append( dmi[jk] )
			sorted_dm_jk.append( jk ) # need jk for later recognition of each point 
			
			# tmp1=sorted_dm[-1][:minp] #  # SORTED DIST per xi
			tmp2=sorted_dm[-1][:max_p] #  WILL IT PERFORM WELL ON EQUALL CLUST SIZE ?
			vv=np_sum(tmp2 )
			
			if bestii==-1 or vv<bestvv: #[-1]
				bestvv=vv #[-1]
				bestii=ii				
		
		dms_a=np_array(sorted_dm[bestii]) # FOUND MOST CONSISTENT CLUSTER at bestii
		_init_center=x[bestii] 
		if _debug: print(f'bestii {bestii} _ij[bestii] {_ij[bestii]} _init_center{_init_center}' ) # why 3.8 -9.1 not better ??
		
		
		
		###########################################
		# FOR the cluster find INIT STOP criterion
		# based on shared % of points 
		# stop when shared < 15%
		# OR shared < 50% and big drop in shared - comparing avg of calc params vs stdev
		###########################################
		
		_stop= (min_clust_size+minp)//2 
		
		_param=[]
		_use=minp #max_p
		for ii in range(_stop,xlen):  
			jk=sorted_dm_jk[bestii][ii] # current point
			tmp_clust_jk=sorted_dm_jk[jk][:_use] # this point nearest points
			_int=len( np_intersect1d(tmp_clust_jk,sorted_dm_jk[bestii][:ii]) ) # now check % points in init cluster - if majority - join 
			
			# TRY SIGNIF DROP
			# on range min min_clust_size split half 
			_param.append(2*_int/ (_use+ii))
			if  len(_param)>=min_clust_size : #min_clust_size:
				s1=_param[ :-min_clust_size//2] 
				s2=_param[-min_clust_size//2 :] 
				mean_dif=np_mean(s2)-np_mean(s1)
				std_sum=np_std(s1,ddof=1) #+np_std(s2,ddof=1)
				if mean_dif/std_sum<-3 and np_mean(s2)<0.5:
					if _debug: print(f'>>>> mean_dif {mean_dif} std_sum {std_sum} np_std(s1 ) {np_std(s1,ddof=1)} np_std(s2 ) {np_std(s2,ddof=1)} np_mean(s1) {np_mean(s1)} np_mean(s2) {np_mean(s2)}')
					_stop=ii -min_clust_size//2 #-1
					break
			
			_stop=ii # if _int/ sqrt(minp*ii)<0.5/_dim_sigma_multip: # ?? for more dim divide ??
			if _param[-1]<0.15: # ALSO ADD SIGNIF DROP ??
				_stop=ii-1
				if _debug: print(f'\nbreak < 0.15 len(_param) {len(_param)} _param[-1] {_param[-1]} ii {ii}')
				break
		
		if _debug: print('new stop',_stop) # HAVE STOP POINT OF CLUSTER 
		
		# ESTIMATE SIGMA
		sigm=sqrt(np_sum(dms_a[:_stop-1]*dms_a[:_stop-1])/(_stop-1))	
		if _debug: print('ESTIMATE SIGMA',sigm) 
		
		##############################################################
		# CALC CHANGES like derivatives, sort them , find n+1 biggest 
		##############################################################
		derv=(dms_a[1:]-dms_a[:-1]) # this unnecessary ? /sigm/_dim_sigma_multip
		derv[:min_clust_size//2]=0 # remove init min_clust_size//2 
		sorted_derv=sorted(derv,reverse=True) 
		
		derv_cond=min(sorted_derv[:n+1]) 
		
		if _debug: print('derv_cond',derv_cond) #, sorted_derv[:n+1] )
		
		#######################################################
		# FINAL CLUSTER STOP SEARCHING
		# most important big deriv change OR previous _stop
		# sigm condition less likely
		########################################################
		lastii=min_clust_size-1 #minp # HERE SOMETIMESS MINP is too big
		
		for ii,d in enumerate(derv):
			if ii<lastii: #mmin : #(1+n): 
				continue				
			# FINAL COND either jump over sigma in 1 move or overal > 6 sigma 
			
			if d>=derv_cond or dms_a[ii]>sigm*_sigm_m*_dim_sigma_multip  or (ii>=_stop  ): 
				if _debug: print(f'match cond d>derv_cond or dms_a[ii]>sigm*{_sigm_m}',ii,d>=derv_cond, dms_a[ii], sigm*_sigm_m*_dim_sigma_multip) #, sigm*_sigm_m*_dim_sigma_multip/2)
				lastii=ii +1
				break
				
		############################################################
		# GET CLUSTER POINTS TO REMOVE AND CALC NEW CENTER AND SIGM
		############################################################
		
		sort_to_remove=argsort(dm[bestii])
		to_remove=sort_to_remove[:lastii] 
		
		new_center=np_stack( [np_mean(x[to_remove,0]), np_mean( x[to_remove,1])] )
		sigm_fin=np_stack( [np_std(x[to_remove,0],ddof=1), np_std( x[to_remove,1],ddof=1)] ) 
		
		# special cond for xlen-leftover < ...
		# if point has sigma > max sigma - reject clulster 
		# centers, sigmas)
		if xlen-leftover<4: # exceptional check 
			sigm_fin_tot2=np_sum(sigm_fin**2)
			_reject=False
			for _sig in sigmas:
				sigmas_tot2=np_sum(_sig**2)
				if _debug: print('%%% comparing sigmas',sigm_fin_tot2,sigmas_tot2)
				if sigm_fin_tot2>sigmas_tot2: # reject cluster 
					_reject=True
					if _debug: print('%%% REJECTED sigmas' )
					break
					
			if _reject:				
				if _debug: debug_plot(dm[bestii],dms_a[:lastii],derv,x_orig,x[to_remove],centers,pot_out_x_new,x ) 	
				break
		
		
		# first clear outliers after new match - maybe some drop!
		potential_outliers=np_setdiff1d(potential_outliers, _ij[to_remove])  
		to_estim_err= sort_to_remove[lastii:]
		rty=np_sqrt(np_sum( ( np_subtract(x[to_estim_err], new_center) / sigm_fin )**2, axis=1))
		pot_err=rty<(_sigm_m+0.5)*(_dim_sigma_multip) #*2
		tmp_ij=_ij[to_estim_err]
		potential_outliers=np_append( potential_outliers, tmp_ij[pot_err] )
		
		pot_out_x_new=x[to_estim_err][pot_err]
		if _debug: print(n,'potential_outliers', len(potential_outliers),  potential_outliers, 'from cur iter',pot_out_x_new ) 
		
			
		oo,reassign=checkOverlap(new_center,sigm_fin,clusters,x[to_remove],_dim_sigma_multip)
		
		
		
		if len(oo)==1:  
			clusters[oo[0]]=extendCluster(clusters[oo[0]], x[to_remove] , _ij[to_remove])
			n+=1
		elif len(oo)>1: # pass # split 
			# for each point calculate closest center
			reas_set=set(reassign) 
			for ss in reas_set:  
				kl=to_remove[ reassign==ss ]
				clusters[ss]=extendCluster(clusters[ss], x[kl] , _ij[kl])
			n+=1
			
			# HERE CONSIDER MERGING in while loop after success mergin try again! 
			clusters=mergeClusters(clusters,_dim_sigma_multip)
			
		else: 
			if _debug: print(new_center, sigm_fin )
			clusters[iiter]={'size': len(to_remove)
							, 'features':x[to_remove]
							, 'index':_ij[to_remove]
							, 'center':new_center.copy()
							, 'sigma':sigm_fin #.copy()
							, 'significance':len(to_remove)/sqrt(np_sum(sigm_fin*sigm_fin))
							}
		# update centers and sigmas 
		centers, sigmas=[],[]
		for k,v in clusters.items():
			centers.append(v['center'])
			sigmas.append(v['sigma'])
			
		if _debug: 
			print('centers',centers, sigmas)
			debug_plot(dm[bestii],dms_a[:lastii],derv,x_orig,x[to_remove],centers,pot_out_x_new,x ) 
		
		# REWRITE X and IJ
		if len(to_remove)<xlen:		
			x_new=[]
			new_ij=[]
			for ii,xi in enumerate(x):
				if ii not in to_remove:
					x_new.append(xi)
					new_ij.append(_ij[ii])
			x=np_stack( x_new ) #ndarray(x_new,2)
			_ij=np_stack(new_ij)
			
		# total_removed+=len(to_remove)
		n-=1
		
	# try assign outliers and final test cluster merge 
	clusters, _ij, x=tryFixOutliers(clusters,x,_ij,_sigm_m,_dim_sigma_multip,_enforceAssign)
	clusters=mergeClusters(clusters,_dim_sigma_multip)
	centers=[v['center'] for k,v in clusters.items()]
	
	
	_labels[_ij]=0
	for k,v in clusters.items():
		_labels[v['index']]=k
		
	outliers={'indexes':_ij, 'features':x }
	if _debug: print('final  outlier',outliers)	
	
	return np_stack( centers ), _labels , clusters, outliers





def tryFixOutliers(clusters,x,_ij,_sigm_m,_dim_sigma_multip,_enforceAssign=False):
	_keys=list(clusters.keys())
	x_ij={k:{'xi':[], 'ij':[]} for k in _keys}
	_ij2,x2=[],[]
	for ii, xi in enumerate(x):	
		testv={}
		for k,v in clusters.items(): 
			testv[k]=sqrt( np_sum( ( (xi-v['center'])/(v['sigma']*(_sigm_m+0.5)*_dim_sigma_multip) )**2 ) ) 
		_closest=argsort(list(testv.values()))   
		best_k=list(testv.keys())[_closest[0]]  
		if testv[best_k]<1 or _enforceAssign: #(xi,_ij[ii])
			x_ij[best_k]['xi'].append( xi )
			x_ij[best_k]['ij'].append( _ij[ii] )
		else:
			_ij2.append(_ij[ii])
			x2.append(xi)
	# ASSIGN OUTLIERS:
	
	for k,v in x_ij.items():
		if len(v['xi'])>0: 
			clusters[k]=extendCluster(clusters[k],v['xi'],v['ij'])
	return clusters,_ij2,x2
	







def extendCluster(clusterIn,x,_ij):
	# print('extendCluster',type(clusterIn['features']), type(x) ,len(clusterIn['features']), len(x))
	clusterIn['features']=np_concatenate( (clusterIn['features'], x ) ) 
	clusterIn['index']=np_concatenate( (clusterIn['index'], _ij ) )
	clusterIn['size']=len(clusterIn['features']) 
	clusterIn['center']=np_stack([np_mean(clusterIn['features'][:,0]),np_mean( clusterIn['features'][:,1])])
	clusterIn['sigma']=np_stack([np_std(clusterIn['features'][:,0],ddof=1),np_std( clusterIn['features'][:,1],ddof=1)]) 
	clusterIn['significance']=clusterIn['size']/sqrt(np_sum(clusterIn['sigma']*clusterIn['sigma']))
	
	return clusterIn
			
			



def mergeClusters(clusters,_dim_sigma_multip):
	_mindelta=1.0*_dim_sigma_multip
	_merged=True
	while _merged: 
		_merged=False
		_cs=[ (v['center'],v['sigma'], k) for k,v in clusters.items()]
		_cslen=len(_cs)
		for ii  in range(_cslen):
			k1=_cs[ii][2]
			for jj in range(ii+1,_cslen):
				k2=_cs[jj][2]
				testv=  sqrt( np_sum( ( (_cs[ii][0]-_cs[jj][0])/(_cs[ii][1]+_cs[jj][1]) )**2 ) )
				if testv<_mindelta: 
					clusters[k1]=extendCluster(clusters[k1] , clusters[k2]['features'],clusters[k2]['index'])
					# clusters[k1]['features']=np_concatenate( (clusters[k1]['features'], clusters[k2]['features'] ) ) 
					# clusters[k1]['index']=np_concatenate( (clusters[k1]['index'], clusters[k2]['index'] ) ) 
					# clusters[k1]['size']=len(clusters[k1]['features'])
					# clusters[k1]['center']=np_stack([np_mean(clusters[k1]['features'][:,0]),np_mean( clusters[k1]['features'][:,1])])
					# clusters[k1]['sigma']=np_stack([np_std(clusters[k1]['features'][:,0],ddof=1),np_std( clusters[k1]['features'][:,1],ddof=1)]) 
					# clusters[k1]['significance']=clusters[k1]['size']/sqrt(np_sum(clusters[k1]['sigma']*clusters[k1]['sigma']))
					del clusters[k2]
					_merged=True
					break
			if _merged:
				print('merged!',k1,k2)
				break
	return clusters



# separate reassign ?
# add otpion of empty x_new to not calc reassign ...
 
def checkOverlap(new_c,new_s,clusters, x_new, _dim_sigma_multip): #centers,sigmas,):new_len,
	_mindelta=1.0*_dim_sigma_multip # opt 3 ?
	# if overlap with 1 - merge, if overlap with >1 split points
	overlaps=[]
	
	for k,v in clusters.items():
		delt= sqrt( np_sum( ( (v['center']-new_c)/(new_s+v['sigma']) )**2 ) ) # much better 
		# print('delta',delt,v['center'],v['sigma'],new_c,new_s)
		if delt<_mindelta: 
			overlaps.append(k)
			
	# print('overlaps',overlaps)
	reassign=[]
	if len(overlaps)>1 :
		for ii,xi in enumerate(x_new): # this only to reassign 
			tmpval={}
			for k,v in clusters.items(): 
				tmpval[k]= np_sum( ( (v['center']-xi)/(new_s+v['sigma']) )**2  )   
			_closest=argsort(list(tmpval.values()))   
			reassign.append( list(tmpval.keys())[_closest[0]] )
		reassign=np_stack(reassign)
	return overlaps, reassign
			
			

def debug_plot(dm_bestii,dms_a_lastii,derv,x_orig,x_to_remove,centers,pot_out_x_new,x ):
	
	fig, (ax1,ax2,ax3  ) = plt.subplots(3,1)
	ax1.plot(list(range(len(dm_bestii))), sorted(dm_bestii) )
	ax1.plot(list( range(len(dms_a_lastii))), dms_a_lastii,color='orange' )
	
	ax2.plot(list(range(len(derv))),derv )
	
	ax3.scatter(x_orig[:,0], x_orig[:,1], s=5,color='red')
	ax3.scatter(x_to_remove[:,0], x_to_remove[:,1], s=5,color='orange')
	ttmp=np_stack( centers )
	ax3.scatter(ttmp[:,0], ttmp[:,1], s=15,color='blue')
	ax3.scatter(pot_out_x_new[:,0], pot_out_x_new[:,1], s=10,color='black')
	
	# ax4.scatter(x[:,0], x[:,1], s=5)
	plt.show()
	
	
# FOR TESTING 	

def compare2kmeans(x,cent1,lbls1, cent2,lbls2):
	fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(1,2))
	
	ax1.set_title('Step Clust')
	ax2.set_title('K-Means with init=k-means++') 
		
	ax1.scatter(x[:,0], x[:,1], s=20,c=lbls1)
	ax2.scatter(cent1[:,0], cent1[:,1], s=40,color='white', marker='+')
	ax1.scatter(cent1[:,0], cent1[:,1], s=32,color='black', marker='+')	
		
	ax2.scatter(x[:,0], x[:,1], s=20,c=lbls2)
	ax2.scatter(cent2[:,0], cent2[:,1], s=40,color='white', marker='+')	
	ax2.scatter(cent2[:,0], cent2[:,1], s=32,color='black', marker='+')	
	
	# fig.tight_layout()
	fig.set_size_inches(10, 5)
	# fig.set_dpi(100)
	plt.show()	
	
	

def plot_init_centers(x,cent,lbls): 
	lbls=np_stack( lbls ) 
	lbls=lbls.reshape((len(lbls),1))
	plt.scatter(x[:,0], x[:,1], s=40,c=lbls)
	plt.scatter(cent[:,0], cent[:,1], s=40,color='black')
	plt.show()
	
def do_kmeans(x,n):		
	km = KMeans(init="k-means++", n_clusters=n, n_init='auto' ) 
	km.fit(x)
	# print('kmeans centers',km.cluster_centers_)
	return km.cluster_centers_, km.labels_
	
	

# def close(v,p): # finding nearest point to this one for debuging 
	# s=np_subtract(v, p)
	# r=np_sum(s**2,axis=1) 
	# ii=argsort(r)
	# print(ii[0],v[ii[0]])
		

def gen_blobs(n_samples=[10,20,50,100,500], centers=None, cluster_std=[0.1,0.1,0.2,0.2,0.5] ):
	if type(centers)==type(5):
		return make_blobs(n_samples=700, centers=centers)
		
	if cluster_std==None:
		return make_blobs(n_samples=n_samples ,  centers=centers)
	xy, cluster_orig = make_blobs(n_samples=n_samples ,  centers=centers, cluster_std=cluster_std) #, random_state=0)
	return xy, cluster_orig




if __name__=='__main__':
	
	max_iter=10
	
	# print('\n### FIRST TEST - 5 RANDOM CENTERS WITH DIFFERENT CLUSTER SIZE [10,20,50,100,500] AND DIFF STD [0.1,0.1,0.2,0.2,0.5] ###')
	# for ii in range(max_iter):
		
		# xy, cluster_orig = gen_blobs( ) 
		
		# cent1,lbls1, clust,outl=step_clust(xy, max_clusters=5, min_clust_size=9, _debug=False)
		
		# cent2,lbls2=do_kmeans(xy,5)
		
		# compare2kmeans(xy,cent1,lbls1, cent2,lbls2)
		
		
		
	# print('\n### 2nd TEST - 5 RANDOM CENTERS WITH DIFFERENT CLUSTER SIZE [10,20,50,100,500] AND SAME STD=1 ###')
	
	# for ii in range(max_iter):
		
		# xy, cluster_orig = gen_blobs(cluster_std=None )   
		
		# cent1,lbls1, clust,outl=step_clust(xy, max_clusters=5, min_clust_size=9, _debug=False)
		
		# cent2,lbls2=do_kmeans(xy,5)
		
		# compare2kmeans(xy,cent1,lbls1, cent2,lbls2)
	
	
	
	print('\n### 3rd TEST - 5 RANDOM CENTERS WITH SAME CLUSTER SIZE AND SAME STD=1 ###')
	
	for ii in range(max_iter):
		
		xy, cluster_orig = gen_blobs( centers=5 ) 
		
		cent1,lbls1, clust,outl=step_clust(xy, max_clusters=5, min_clust_size=9, _debug=False)
		
		cent2,lbls2=do_kmeans(xy,5)
		
		compare2kmeans(xy,cent1,lbls1, cent2,lbls2)
		
		
	