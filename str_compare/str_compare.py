
# goal:
# 1. Human error detection: be able to detect string similarity based on combination of damerau_levenshtein_distance
# combined with unidecode + lowercased options
# 2. detect name spoofing (create name versions)

from sys import argv as sys_argv
from  jellyfish import  damerau_levenshtein_distance, jaro_winkler_similarity ,jaro_similarity
from unidecode import unidecode
from string import ascii_letters as s_ascii_letters, digits as s_digits
import homoglyphs_fork  as hg
 
class CompareStr:

	# constructor CompareStr(uniDecode=True,lowerCase=True) - set if discount unicode and lowercase 
	# similarity(self,s1,s2,_r=4) calculate similarity , returns rate + transformed strings 
	# spoof(self,s,_decoded=False ) return list of spoofed strings for name spoof detection 
	# bestSpoof(self,spf_list,ss) having referal ss checks vs spoof list for best match 

	def __init__(self,uniDecode=True,lowerCase=True):
		
		self.unidecode=uniDecode
		self.lowercase=lowerCase
		self.cost={'lower':1/4, 'unidecode':1/4} # only used when detected + no other chages found
		self.ASCIIvsUTF8()
		# self.hmap=self.gethmap() # for spoofing - TODO 
		
	# because unidecode can give few strings - cross similarity is done and most similar taken 	
	def similarity(self,s1,s2,_r=4,_spoof=False,_explain=False): # need to ignore lower case and unidecode for spoofing 
		if s1==s2:
			return 1.0
	
		denom=max(len(s1),len(s2))
		
		s11_list=[s1] 
		s21_list=[s2] 
		
		if self.unidecode:  # this can create multiple values per string ! since some unicodes have multiple similar asciis 
			s11_list,s21_list=self.uni2ascii(s1), self.uni2ascii(s2)
		
		res={} # r:[s1,s2]
		for s11 in s11_list:
			for s21 in s21_list:
		
				s12,s22=s11,s21
				if self.lowercase: 
					s12,s22=s11.lower(), s21.lower()

				if _explain: 
					print(f'Input strings: ({s1},{s2}) after lower case and unidecode:({s12},{s22}) ')			
					print(f'Damerau_levenshtein_distance({s12},{s22}) =',damerau_levenshtein_distance(s12,s22))
				init_cost=damerau_levenshtein_distance(s12,s22)
				sim=1-init_cost/denom  
				if _explain: print(f'Init similarity {round(sim,_r)} from cost {init_cost} of denom {denom}')
				
				# correcting for lower case and unidecode is somehow redundant but at this spot I do not have info which chars are t odel
				# so decided to make correction of small value + ensure rate >= 0
				if not _spoof and (self.unidecode or self.lowercase) : # r==1 and  if no diff detected do small correct for lower + unidecode 
					unicode_cost=0
					if self.unidecode :
						c1u,c2u=self.changeCount( s1,s11) , self.changeCount( s2,s21) # 
						unicode_cost= len(listAminusB(c1u.copy(),c2u.copy())) + len( listAminusB(c2u.copy(),c1u.copy()))
						if _explain: print(f"Unicode_cost before/after weight {unicode_cost}/{self.cost['unidecode']*unicode_cost}")
					
					lower_cost=0
					if self.lowercase :
						c1l,c2l=self.changeCount( s11,s12), self.changeCount( s21,s22)  #print('c1l,c2l',c1l,c2l)
						lower_cost= len(listAminusB(c1l.copy(),c2l.copy())) + len( listAminusB(c2l.copy(),c1l.copy()))
						if _explain: print(f"Lowercase_cost before/after weight {lower_cost}/{self.cost['lower']*lower_cost}")
					
					adj_cost=sum(( self.cost['unidecode']*unicode_cost, self.cost['lower']*lower_cost) )/denom #(len(s1)+len(s2))
					if _explain: print(f"Similarity adjustment {-adj_cost}")
					sim=sim-adj_cost if sim>adj_cost else 0
					if _explain: print(f"Adjusted similarity  {round( sim,_r)} for {s12} and {s22}\n")
				sim=round( sim,_r)
					
				if sim not in res: res[sim]=[[s12,s22]]
				else: res[sim].append([s12,s22])
		# best match in case of multi pairs :
		bestr=max(res.keys())
		sim=res[bestr][0]
		if _explain: print(f"Final similarity  {bestr} of {sim}")
	
		return bestr,sim
	
	
	def changeCount(self,s1,s2): # detect changes :1 for lowered or unidecoded
		# print(s1,s2,len(s1),len(s2))
		found=[]
		for ii, ti in enumerate( (*s2,)  ): 
			if s1[ii]!=ti: 
				found.append(s1[ii])				
		return found #, changed # change needed to not count double !?
		
	
	
	def uni2ascii(self,s):
		s2=[''] # may be combinations so output may be multiple decoded 
		for si in s:
			# print(si,si in self.h_utf2ascii)
			if si in self.h_utf2ascii:
				# if single just add:
				tmp=self.h_utf2ascii[si]
				new_s2=[s2i+t for t in tmp for  s2i in s2 ]
				s2=new_s2
				
			else: # just add chsr					
				for ii,s2i in enumerate(s2):
					s2[ii]=s2i+si
		return s2
			
	# decoding with h_utf2ascii may give few values 
	# in that case calc similarity for all cross ?
	# and find best match 
	def ASCIIvsUTF8(self):
		self.h_asci2utf={} # USE IN SPOOFING
		self.h_utf2ascii={} # includes multiple values USE IN DECOGING
		aa=list(s_ascii_letters+s_digits)
		homoglyphs = hg.Homoglyphs( strategy=hg.STRATEGY_LOAD,
							ascii_strategy=hg.STRATEGY_LOAD,
							categories=hg.Categories.get_all(), #('LATIN', 'COMMON', 'CYRILLIC'),
							languages=hg.Languages.get_all()
							)
		# print(homoglyphs.get_combinations('e'))
		full_alpha=hg.Categories.get_alphabet(hg.Categories.get_all())
		# print('full_alpha',len(full_alpha))	
		# remove after added in combinations to add "final combinations" if unidecode works!
		# with open('a.txt','w', encoding="utf-8") as f :
		if True:
			for a in aa:
				self.h_asci2utf[a]=homoglyphs.get_combinations(a)
				full_alpha-=set(self.h_asci2utf[a])
				self.h_asci2utf[a].remove(a)
				# f.write(str(self.h_asci2utf[a])+'\n')
				for u in self.h_asci2utf[a]:
					if u in aa: continue # do not map ascci to ascii 
					if u not in self.h_utf2ascii:
						self.h_utf2ascii[u]=[a]
					elif a not in self.h_utf2ascii[u]:
						self.h_utf2ascii[u].append(a)
						
			# manual correction for German double ss 'ß','ẞ','ʙ', and Greek ... 
			man_cor={'µ':['u'], 'γ':['y'],'η':['n'],'ν':['v'],'ω':['w'],'φ':['p'],'θ':['0'],'ß':['B'],'ẞ':['B'], 'ʙ':['B']}
			for k,vv in man_cor.items():
				for v in vv:
					self.h_asci2utf[v].append(k)
					if k not in self.h_utf2ascii:
						self.h_utf2ascii[k]=[v]
					elif v not in self.h_utf2ascii[k]:
						self.h_utf2ascii[k].append(v)
			
			# print('h_asci2utf',len(self.h_asci2utf))	
			# print('h_utf2ascii',len(self.h_utf2ascii))	
			# aded=[]	
			for fa in full_alpha: # add unidecode - far from perfect but usefull
				if fa in aa: continue # do not map ascci to ascii 
				try: # homoglyphs do not show diactrics as similar!
					uu=unidecode(fa) 
					self.h_asci2utf[uu].append(fa)
					# aded.append({fa:uu})
					
					if fa not in self.h_utf2ascii:
						self.h_utf2ascii[fa]=[uu]
					elif uu not in self.h_utf2ascii[fa]:
						self.h_utf2ascii[u].append(a)
				except:
					pass
			# print('2 h_asci2utf',len(self.h_asci2utf))	
			# print('2 h_utf2ascii',len(self.h_utf2ascii))	
			# with open('ad.txt','w', encoding="utf-8") as f :
				# for ad in aded:
					# f.write(str(ad)+'\n')
				
					
			# with open('a2.txt','w', encoding="utf-8") as f :
				# f.write(str(self.h_utf2ascii )+'\n')
		# print('full_alpha',len(full_alpha))				
		# with open('a2.txt','w', encoding="utf-8") as f :
			# for k,v in self.h_utf2ascii.items():
				# f.write(str(k)+':'+str(v)+'\n')
	
	
	#
	#	SPOOFING PART REMOVED
	# def gethmap(self):	# extensive list may look differently depending on font
		# grouped=[ [ 'G','@','6']
				# , ['g','p','q','9']
				# , ['B','8','&','ß','ẞ','ʙ']
				# , ['E','3']
				# , ['e','@']
				# , ['R','A','fi']
				# , ['J','U','V']
				# , ['u','v']
				# , ['S','5','$']
				# , ['i','!','j',';']
				# , ['/','|','I','l','1','{','}']
				# , ['Z','2','7']
				# , ['Q','O','0']
				# , ['b','6' ]
				# , ['c','C','(']
				# , ['\'','`' ]
				# , [':',';' ]
				# , ['"',"''","``" ]
				# , ['a',"ci"  ]
				# , ['d',"cl" ]
				# , ['g',"cj" ]
				# , ['m',"rn","nn" ]
				# , ['W',"VV","UU" ]
				# , ['n',"h" ]
				# , ['h','k']
				# , ['A',"/\\" ]
				# , ['B',"|3" ]
				# , ['D',"|)" ]
				# , ['G',"(¬" ]
				# , ['H',"|-|" ]
				# , ['K','|<','|{' ]
				# , ['L','|_' ]
				# , ['M','|v|' ]
				# , ['N','|\|' ]
				# , ['V','\\/' ] 
				# ]
		# hmap={}
		# for g in grouped:
			# for el in g:
				# if el not in hmap:
					# hmap[el]=g.copy()
					# hmap[el].remove(el) # remove self mapping 	
				# else:
					# tmp=g.copy()
					# tmp.remove(el) 
					# hmap[el]+= tmp
		
		# return hmap


	# DO NOT LOWERCASE BUT UNIDECODE EXCLUDING 'ß','ẞ','ʙ'
	# create spoof potential strings for spoof name countering
	# def spoof(self,s,_decoded=False ): 
		# if not _decoded:
			# s=unidecode(s)
			# print(s)
		# _all=[] 
		# tmp=self.hmap[s[0]].copy()
		# tmp.append(s[0])
			
		# vv=['']
		# if len(s)>1:
			# vv=self.spoof(s[1:], True )	 		
		# for v in vv:
			# for t in tmp:
				# _all.append( t+v) 
					 
		# return _all
		
		
		
	# def bestSpoof(self,spf_list,ss):
		# v=-1 #
		# s=''
		# ss=unidecode(ss)
		
		# for spf in spf_list:
			# tmp=self.similarity(spf,ss,_spoof=True)
			# if tmp>v:
				# v=tmp
				# s=spf
		# return s,v 
	
	
	
	
	
	
	
def listAminusB(a,b):
	red=[]
	for ai in a:
		if ai not in b:
			red.append(ai)
		else:
			b.remove(ai)
	return red
	
	

def test():	

	cs=CompareStr(True,lowerCase=True)
	s1='jellyfish'
	s2='smellγfish'
	v1=cs.similarity(s1,s2,_explain=False)
	print(f"Similarity of [{s1}] and [{s2}] = {v1[0]} ")
	s1='jellyfish'
	s2='smellyFish'
	v1=cs.similarity(s1,s2,_explain=False)
	print(f"Similarity of [{s1}] and [{s2}] = {v1[0]} ")
	s1='jellyFish'
	s2='smellyFish'
	v1=cs.similarity(s1,s2,_explain=False)
	print(f"Similarity of [{s1}] and [{s2}] = {v1[0]} ")
	s1='ßusiness'
	s2='Business'
	v1=cs.similarity(s1,s2,_explain=False)
	print(f"Similarity of [{s1}] and [{s2}] = {v1[0]} ")
	s1='ωood'
	s2='wood'
	v1=cs.similarity(s1,s2,_explain=False)
	print(f"Similarity of [{s1}] and [{s2}] = {v1[0]} ")
	
	return
	# print(cs.uni2ascii('A1šždčąę￨ｌ')) # 
	# s1,s2='A1šždčąęｌ', 'A1šždčąę￨'
	# cs.similarity(s1,s2,_explain=True)
	# use below to also cast diactrics:
	# print(hg.Categories.get_alphabet(hg.Categories.get_all()))
	return
	
	# s1,s2='AΑАᎪᗅᴀꓮＡ𐊠𝐀𝐴𝑨', '𝒜 𝓐 𝔄 𝔸 𝕬 𝖠 𝗔 𝘈 𝘼 𝙰 𝚨 𝛢 𝜜 𝝖 𝞐'
	# s1=['µ','ß','ẞ','ʙ'] #['A', 'Ａ', '𝐀', '𝐴', '𝑨', '𝒜', '𝓐', '𝔄', '𝔸', '𝕬', '𝖠', '𝗔', '𝘈', '𝘼', '𝙰', '𝚨', '𝛢', '𝜜', '𝝖', '𝞐']
		
	
if __name__ == "__main__":
	
	test()
	# s1='jellyfish'
	# s2='smellyfish'
	# v1=round(1-damerau_levenshtein_distance(s1,s2)/max(len(s1),len(s2)),4)
	# v2=round(jaro_winkler_similarity(s1,s2),4)
	# print(f"Similarity of [{s1}] and [{s2}]\n\tdamerau_levenshtein = {v1}\n\tjaro_winkler_similarity = {v2}")
	# s1='wood'
	# s2='door'
	# v1=round(1-damerau_levenshtein_distance(s1,s2)/max(len(s1),len(s2)),4)
	# v2=round(jaro_winkler_similarity(s1,s2),4)
	# print(f"Similarity of [{s1}] and [{s2}]\n\tdamerau_levenshtein = {v1}\n\tjaro_winkler_similarity = {v2}")
	# s1='logarithm'
	# s2='algorithm'
	# v1=round(1-damerau_levenshtein_distance(s1,s2)/max(len(s1),len(s2)),4)
	# v2=round(jaro_winkler_similarity(s1,s2),4)
	# print(f"Similarity of [{s1}] and [{s2}]\n\tdamerau_levenshtein = {v1}\n\tjaro_winkler_similarity = {v2}")
	# s1='a'
	# s2='ab'
	# v1=round(1-damerau_levenshtein_distance(s1,s2)/max(len(s1),len(s2)),4)
	# v2=round(jaro_winkler_similarity(s1,s2),4)
	# print(f"Similarity of [{s1}] and [{s2}]\n\tdamerau_levenshtein = {v1}\n\tjaro_winkler_similarity = {v2}")
	# s1='a'
	# s2='ba'
	# v1=round(1-damerau_levenshtein_distance(s1,s2)/max(len(s1),len(s2)),4)
	# v2=round(jaro_winkler_similarity(s1,s2),4)
	# print(f"Similarity of [{s1}] and [{s2}]\n\tdamerau_levenshtein = {v1}\n\tjaro_winkler_similarity = {v2}")
	
	
	
	