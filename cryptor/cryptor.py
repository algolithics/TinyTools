# to compile python -m nuitka cryptor.py

from sys import argv as sys_argv 
from time import perf_counter
from  traceback import  format_exc as traceback_format_exc
from base64 import urlsafe_b64encode, b64encode, b64decode
from os import urandom as os_urandom
from ujson import loads as json_loads, dumps as json_dumps

from os.path import exists as os_path_exists, isdir as os_path_isdir, join as os_path_join, sep as os_path_sep

# additional modules for dealing with files
from string import ascii_letters as s_ascii_letters, digits as s_digits
from random import seed as r_seed, sample as r_sample, choices as r_choices, randint as r_randint, randbytes
# from dirtyjson import loads as dirty_j_loads

# PYCRYPTODOME
# from Crypto.Protocol.KDF import PBKDF2 as dome_PBKDF2, scrypt as dome_scrypt
# from Crypto.Hash import SHA256 as dome_sha256
# from Crypto.Random import get_random_bytes as dome_get_random_bytes
# from Crypto.Cipher import ChaCha20

# FERNET:
# from cryptography.fernet import Fernet
# from cryptography.hazmat.primitives import hashes
# from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# nacl
from nacl import pwhash, secret, utils


class Cryptor:

	# encrypt(self,password,msg) returns json string of dict {'encr_msg':p.encode64(encr_msg) , 'salt':p.encode64(salt) , 'ops':ops,'mem':mem  } #, 'nonce':p.encode64(nonce)
	# decrypt returns message bytes / may need decode(utf-8)
	# encryptFile(self,password,fpath,newFname='',toPath='.') created encrypted file
	# decryptFile(self,password,fpath, toPath='.') creates decrypted file 
	
	# encrpt does:
	# _level=='_INTERACTIVE' for local usage
	# _level=='_MODERATE' 8x longer
	# _SENSITIVE for max protection
	
	def __init__(self,_level='_INTERACTIVE'):
		
		if _level=='_INTERACTIVE':
			self.nacl_ops = pwhash.argon2i.OPSLIMIT_INTERACTIVE
			self.nacl_mem = pwhash.argon2i.MEMLIMIT_INTERACTIVE
			# self.fernet_iterations=128*1024 # number of hashes using fernet
			# self.dome_scrypt_N=2**14 # strength of hashing password in dome min 2**14 max 2**20		
			
		elif _level=='_MODERATE': 
			self.nacl_ops = pwhash.argon2i.OPSLIMIT_MODERATE # 8x longer for nacl
			self.nacl_mem = pwhash.argon2i.MEMLIMIT_MODERATE
			# self.fernet_iterations=512*1024 # x4 
			# self.dome_scrypt_N=2**16 # x4
			
		else: # EXTREME
			self.nacl_ops = pwhash.argon2i.OPSLIMIT_SENSITIVE
			self.nacl_mem = pwhash.argon2i.MEMLIMIT_SENSITIVE
			# self.fernet_iterations=1024*1024  
			# self.dome_scrypt_N=2**20
		
		
		
	def encrypt(self,password,msg):
		return json_dumps( self.encryptNaClxSalsa20( password,msg ) )
		
	def decrypt(self,password,msg): 
		_msg=json_loads(msg) 
		_decr=self.decryptNaClxSalsa20( password,_msg['salt'] ,_msg['encr_msg'], int(_msg['ops']),int(_msg['mem']))
		return _decr['msg']
		
		

	def decryptFile(self,password,fpath, toPath='.'):	 
		dd=self._readEncrFile(fpath) 
		decr_fname=self.decryptNaClxSalsa20( password,dd['encr_fname']['salt'] ,dd['encr_fname']['encr_msg'],dd['encr_fname']['ops'],dd['encr_fname']['mem'] )
		decr_cont=self.decryptNaClxSalsa20( password,dd['encr_cont']['salt'] ,dd['encr_cont']['encr_msg'],dd['encr_cont']['ops'],dd['encr_cont']['mem'] )
		
		newFname=decr_fname['msg'].decode('utf-8')  
		newFnameInit=newFname
		if toPath!='.':
			newFname=os_path_join(toPath,newFname)
		if os_path_exists(newFname):	
			_chars=list(s_ascii_letters +  s_digits) 			
			r_seed( int.from_bytes(utils.random(size=8), byteorder='big', signed=True) )
			_ii_left=7
			while os_path_exists(newFname) and _ii_left>0:
				newFname=''.join(r_choices( _chars, k=8-_ii_left ) )+'_'+newFnameInit
				if toPath!='.':
					newFname=os_path_join(toPath,newFname)
				_ii_left-=1
			if _ii_left==0 and os_path_exists(newFname):
				return f'Cannot create decrypted file of {fpath} -- too many files in directory!'	
				
		
		with open(newFname,mode='wb' ) as f:
			f.write(decr_cont['msg'])
			
		return f'Created decrypted file of {fpath} at {newFname}'		
	

	def _readEncrFile(self,path ):
		dds=''
		with open(path,mode='r' ) as f:
			dds= f.read() 
		return json_loads(dds)		
		
		
	def encryptFile(self,password,fpath,newFname='',toPath='.'):	
		# read file orig name , content
		dd=self._readPlainFile(fpath)
		# encrypt
		encr_fname=self.encryptNaClxSalsa20( password,dd['fname'] )
		encr_cont=self.encryptNaClxSalsa20( password,dd['content'] )
		# generate random name , ext=file
		if newFname=='':
			_chars=list(s_ascii_letters +  s_digits) 			
			r_seed( int.from_bytes(utils.random(size=8), byteorder='big', signed=True) )
			newFname=''.join(r_choices( _chars, k=16 ) )+'.txt'
		if toPath!='.':
			newFname=os_path_join(toPath,newFname)
		if os_path_exists(newFname):
			return f'Cannot create decrypted file of {fpath} at {newFname} -- too many files in directory?'	
			
		with open(newFname,mode='w' ) as f:
			f.write(json_dumps({"encr_fname":encr_fname, "encr_cont":encr_cont}) )
			
		return f'Created encrypted file of {fpath} at {newFname}'
	
	
	def _readPlainFile(self,path,mode='rb'):
		_dd={'fname':'', 'content':''}	
		tmp=path.split(os_path_sep)
		_dd['fname']=tmp[-1] if tmp[-1].strip()!='' else tmp[-2]
		# file content:
		with open(path,mode ) as f:
			_dd['content']= f.read()
		return _dd	
	
	
	
	
	
	# COMMON FUNCTIONS
	def _ensureBytes(self,tt):
		if type(tt)!=bytes:
			return bytes(tt,encoding='utf-8')
		return tt
		
	def encode64(self,te): # return b64 str
		return b64encode(te).decode('utf-8') 
		
	def decode64(self,td): # return byte string utf8
		if type(td)==bytes:
			return b64decode(td)
		return b64decode(td.encode('utf-8')) 
	
	

	def naclKeySalt(self,password,salt=None,ops=None,mem=None):
		if salt==None:
			salt = utils.random(pwhash.argon2i.SALTBYTES)
			ops = self.nacl_ops
			mem = self.nacl_mem
		kdf = pwhash.argon2i.kdf
		base64_key = kdf(secret.SecretBox.KEY_SIZE, password, salt, opslimit=ops, memlimit=mem)
		
		return base64_key, salt, ops, mem
	

	def encryptNaClxSalsa20(self,password,msg ):
		password=self._ensureBytes(password)
		msg=self._ensureBytes(msg)
		
		base64_key, salt, ops, mem=self.naclKeySalt(password)
		_box = secret.SecretBox(base64_key)
		# nonce = utils.random(secret.SecretBox.NONCE_SIZE)
		encr_msg = _box.encrypt(msg) #  ,nonce
			
		return {'encr_msg': self.encode64(encr_msg) , 'salt': self.encode64(salt) , 'ops':ops,'mem':mem  } #, 'nonce':p.encode64(nonce)
		
	
	def decryptNaClxSalsa20(self,password,salt,msg,ops,mem): # {'msg':decr_msg }
		password=self._ensureBytes(password) 
		msg=self.decode64( msg)
		salt=self.decode64(salt)  
			
		base64_key, salt, ops, mem=self.naclKeySalt(password,salt,ops,mem)
		_box = secret.SecretBox(base64_key)
		
		decr_msg = _box.decrypt(msg) 
		
		return {'msg':decr_msg } # decide if needs .decode('utf-8')  
		
	
	
		
	
	# def	encrypt2(self,password1,password2,msg): 
		# dd1=self.encryptNaClxSalsa20( password1,msg,_toSend=True) 
		# dd2=self.encryptDomeChaCha20( password2,json_dumps(dd1),_toSend=True) 
		# return json_dumps(dd2)
		
	# def	decrypt2(self,password1,password2,msg):
		# dd2=json_loads(msg) 
		# dd1= self.decryptDomeChaCha20( password2,dd2['salt'],dd2['nonce'] ,dd2['encr_msg'])  
		# dd1=json_loads(dd1['msg'])
		# dd0=self.decryptNaClxSalsa20( password1,dd1['salt'] ,dd1['encr_msg'],dd1['ops'],dd1['mem'])
		# return dd0['msg']
		
	# def toStr(self,tt):
		# if type(tt)==str:
			# return tt
		# return tt.decode('utf-8')	
		
	#DOME
	# def domeKeySalt(self,password,salt=None):
		# if salt==None:
			# salt = dome_get_random_bytes(16)
		# b64pwd = b64encode(dome_sha256.new(password).digest()) 
		# base64_key = dome_scrypt(b64pwd, salt, 32, N=self.dome_scrypt_N, r=8, p=1) # 17->20 
		# return base64_key, salt
		
	# def encryptDomeChaCha20(self,password,msg,_toSend=False):
		# password=self._ensureBytes(password)
		# msg=self._ensureBytes(msg) 
		# base64_key, salt=self.domeKeySalt(password) 
		# cipher = ChaCha20.new(key=base64_key)
		# ciphertext = cipher.encrypt(msg) 
		# nonce = cipher.nonce 
		# ct = ciphertext 
		# if _toSend:			
			# return {'encr_msg': encode64(ct) , 'salt': encode64(salt) , 'nonce': encode64(nonce) }
		# else:
			# return {'encr_msg':ct, 'salt':salt, 'nonce': nonce}
		
	
	# def decryptDomeChaCha20(self,password, salt,nonce,msg):
		# password=self._ensureBytes(password)
		# _decoded=False
		# if type(salt)==str: # asume decoding needed:
			# msg=self.decode64( msg)
			# nonce=self.decode64(nonce)
			# salt=self.decode64(salt)
			# _decoded=True  
		# base64_key, salt=self.domeKeySalt(password,salt) 
		# cipher = ChaCha20.new(key=base64_key, nonce=nonce) 
		# ciphertext = msg 
		# msg = cipher.decrypt(ciphertext)
		# if _decoded:
			# msg=self.toStr(msg)  
		# return {'msg':msg }
	
	
	
	
	
	# FERNET	
	# def fernetKeySalt(self,password,salt=None):
		# if salt==None:
			# salt = os_urandom(16)
		# kdf_password = PBKDF2HMAC( algorithm=hashes.SHA256(), length=32, salt=salt, iterations=self.fernet_iterations)
		# base64_key = b64encode(kdf_password.derive(password))
		# return base64_key, salt
		
	# def encryptFernetAES(self,password,msg,_toSend=False):
		# password=self._ensureBytes(password)
		# msg=self._ensureBytes(msg)
		# base64_key, salt=self.fernetKeySalt(password)
		# f = Fernet(base64_key)
		# encr_msg = f.encrypt(msg) # base 64 ! 
		# if _toSend:			
			# return {'encr_msg':encr_msg.decode('utf-8') , 'salt':p.encode64(salt)   }
		# else:
			# return {'encr_msg':encr_msg, 'salt':salt}
		
	# def decryptFernetAES(self,password,salt,msg):
		# password=self._ensureBytes(password)
		# _decoded=False
		# if type(salt)==str: # asume decoding needed:
			# msg=  msg.encode('utf-8')
			# salt=self.decode64(salt)
			# _decoded=True 
		# base64_key, salt=self.fernetKeySalt( password,salt)
		# f = Fernet(base64_key)
		# decr_msg = f.decrypt(msg)
		# if _decoded:
			# decr_msg=self.toStr(decr_msg)  
		# return {'msg':decr_msg }
			
		
		
def jsonMissingQuotations(json_str):	# return fixed string 	
	if len(json_str)>2:
		if '"' not in json_str:
		
			def clear_j(jstr):
				bl_ii=jstr.find('{')
				br_ii=jstr.rfind('}')
				substr=jstr[bl_ii+1:br_ii]
				
				spl_coma=substr.split(',')
				new_str=''
				for v in spl_coma:
					spl_colo=v.split(':')
					kkey='"'+spl_colo[0].strip()+'":'
					vval='"'+spl_colo[1].strip()+'" '
					if new_str=='':
						new_str+=kkey+vval
					else :
						new_str+=', '+kkey+vval 
				return '{'+new_str+'}' 
			
			return clear_j(json_str)				
	
	return  json_str		
		
		
	
# encrypt or decrypt depending on args
#sys_argv= [ this script name, action: one of [encrypt, decrypt], password, file or message
def main(aargs):
	if len(aargs)<3:
		print('3 parameters required: action, password, file path or message text')
		print('Action can be "e" for encrypt, "d" for decrypt')
		print('Example: e asdf1234#@# "this is my message"')
		return
	elif aargs[0] not in ['e','d']:
		print(f'Action can be "e" for encrypt, "d" for decrypt, not {aargs[0]}')
		return

	try:	 
		cry=Cryptor()
		if aargs[0]=='e':
			if os_path_exists(aargs[2]):
				print( cry.encryptFile( aargs[1],aargs[2] ) )
			else:
				print( cry.encrypt( aargs[1],aargs[2] )  )
		else:
			if os_path_exists(aargs[2]):
				print( cry.decryptFile( aargs[1],aargs[2] ) )
			else: 
				print( cry.decrypt( aargs[1], jsonMissingQuotations(aargs[2]) ).decode('utf-8') )
			
	except:
		print(traceback_format_exc() )
		exit()
	
	
	
	
if __name__ == '__main__':
	main(sys_argv[1:])