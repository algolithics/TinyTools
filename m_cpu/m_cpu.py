#### DESCRIPTION ######################################################################
# GOAL is to have control over used CPUs  
# ans  split work (ndarray or list of lists) among selected cores
# evenly 
# for optimal core usage and faster calculations
# user may define how many cores to use; they are sorted least to most busy

#### CLASSES:
#
# class CPU_Load to probe cpu load and get list of cpus
#	CPU_Load(markbusy=30,n=9,_do_print=False)
#	analyze(self,markbusy ,n,_do_print=False) # auto run on init, can be rerun if needed more
#	cpu_list=getCpuList(self,opt): int or allowed_list=['all','ab1','best','notbusy']
#
# class WorkManager(print_performance=False)
# 	startWorkers(self,cpu_list, work_to_split, other_args, worker_fun)
#	>> returns: results={cpuii:worker_fun(worker_mem,other_args) }, workers_time={cpuii:}
#		[optional] work_to_split=ndarray or list of lists 
#		[optional] other_args
# 		worker_fun(work_ndarray, other_args) or worker_fun(other_args)

from psutil import cpu_percent, cpu_count as multip_cpu_count, Process
from concurrent.futures import ProcessPoolExecutor, as_completed #, wait
from multiprocessing import  Manager as multi_manager # manager actually use more mem 
from gc import enable as gc_enable, disable as gc_disable, collect as gc_collect
from time import perf_counter, time as time_time
from tracemalloc import get_traced_memory, start as tracemalloc_start



		

class WorkManager:

	# main method:
	# startWorkers(self,cpu_list, work_to_split, other_args, worker_fun)

	def __init__(self,print_performance=False,print_fun=print ):	
	
		self.work_split_local=[]
		self.print_fun=print_fun
		self.print_performance=print_performance
		
		if self.print_performance:
			tracemalloc_start()# Check memory usage
			current, peak = get_traced_memory()
			self.print_fun(f"Init memory usage {current/1e6}MB; Peak: {peak/1e6}MB")
		
	
		
	# returns: results (cpuii, worker_fun(worker_mem,other_args) ), workers_time
	def startWorkers(self,cpu_list, worker_fun, work_to_split=[], other_args={}):
		self.work_split_local=[]
		cpucc=len(cpu_list)
		
		if len(work_to_split)==0:
			self.print_fun(f'Nothing to process- empty work list: {work_to_split}')
			return None,None
		
		if type(work_to_split)==type({}): # EXCEPTION FOR DICT - no need to split work 
			self.work_split_local=[v for k,v in work_to_split.items()]
			# print('not SPLITING???')
		else:
			# print('SPLITING???')
			self._splitWork( cpucc, work_to_split) # split work 
		
		cpucc=len(self.work_split_local)
		
		future_res=[]
		results={}
		workers_time={}
			
		t1=perf_counter()
		
		with ProcessPoolExecutor() as exe: #cpucc
			if self.print_performance:
				self.print_fun(f' $ Executor init in {round(perf_counter()-t1,6)} sec.')

			t1=perf_counter()
			for ii in range(cpucc):	# start jobs 
				future_res.append( exe.submit( _start_direct_worker,  worker_fun  , cpu_list[ii], self.work_split_local[ii], other_args )  )
				
			if self.print_performance:
				self.print_fun(f' $  {cpucc} workers started in {round(perf_counter()-t1,6)} sec.')	
			
			t1=perf_counter()			
			
			# collect results # wait(future_res)
			for _ in as_completed(future_res): #.as_completed(fs, timeout=None) 
				tmp_r=_.result()
				workers_time[tmp_r[0]]=round(perf_counter()-t1,6)
				results[tmp_r[0]]=tmp_r[1] 	
				
			if self.print_performance:
				self.print_fun(f' $  Workers finished in {round(perf_counter()-t1,6)}')
				current, peak = get_traced_memory()
				self.print_fun(f"After result collection memory usage {current/1e6}MB; Peak: {peak/1e6}MB")
			
		if self.print_performance:
			self.print_fun(f' $ Executor closed in {round(perf_counter()-t1,6)} sec.')
				
		return results, workers_time
		
		
	
	# work_to_split = list of lists or ndarray 
	# test both 
	def _splitWork(self, cpu_count, work_to_split):	
		t1=perf_counter()			
		
		if cpu_count>1:
			self.work_split_local=[] # reset 	
			ranges=self._calc_ranges(len(work_to_split), cpu_count) # print(ranges)
			
			for ii in range(len(ranges)): 
				self.work_split_local.append(   work_to_split[ranges[ii]['start'] : ranges[ii]['end']]     )
		else:
			self.work_split_local=[work_to_split]
			
		if self.print_performance:
			self.print_fun(f' $ Data sharing time {round(perf_counter()-t1,6)} sec.')
			current, peak = get_traced_memory()
			self.print_fun(f"After work split memory usage {current/1e6}MB; Peak: {peak/1e6}MB")
				
			
		
		
	def _calc_ranges(self,list_len, mcpu): 
		
		ranges={}
		
		xx=int(  list_len//mcpu )  # values per cpu
		additional=list_len-xx*mcpu
		
		signii=1
		if additional<0:
			signii=-1
			
		if list_len<mcpu: # if list_len<=mcpu:
			self.print_fun(f'Selected CPU count {mcpu} > work object count {list_len} - will use only {list_len} cores.')
			xx=1
			mcpu=list_len
			additional=0
		elif xx<2:
			xx=xx+1 # small list len 
			additional=0 
		
		end=0
		for iii in range(mcpu):
		
			start=end 
			if start>list_len:
				break
			
			end1=	start+xx			
			if additional*signii>0:
				end1+=signii
				additional-=signii
				
			end=min(end1, list_len)
			
			ranges[iii]={'start':start, 'end':end}
			
		return ranges		
		
	
	



def _start_direct_worker(worker_fun,cpuii,worker_mem, other_args):
	proc = Process() 
	proc.cpu_affinity([cpuii])
	if other_args=={}:
		return (cpuii, worker_fun(worker_mem ) )
	other_args['cpuii']=cpuii
	return (cpuii, worker_fun(worker_mem,other_args) )
	
		


# test physical if works better ?
class CPU_Load:

	# self.cpus_load contains result for all logical cpus 
	# self.busycpus list of busy cpus
	# self.sortedAll
	# self.sortedNotBusy
	
	# n=9 about 1 sec 
	def __init__(self,markbusy=30,n=9,_do_print=False): # if avg core is 30% busy mark as busy 
	
		self.sortedAll=[] # for all 
		self.sortedNotBusy=[] # for opti
		self.cpus_load=[]
		self.busycpus=[]
		self.cpu_count=multip_cpu_count( ) #logical=False
		self.cpu_count_phisical=multip_cpu_count( logical=False) #
		
		self.analyze( markbusy ,n,_do_print )
	
	
	def analyze(self,markbusy ,n,_do_print=False):
		mmm=self.cpu_count//self.cpu_count_phisical # threads per cpu 
		
		if n<=0: 
			self.sortedAll=[ii for ii in range(self.cpu_count-1,-1,-1) if (ii+mmm-1)%mmm==0 ]
			self.sortedNotBusy=self.sortedAll
			return
		
		cpuc=cpu_percent(interval=0.1,percpu=True)		
		for ii in range(n):
			x=cpu_percent(interval=0.1,percpu=True)
			cpuc=[sum(a) for a in zip(cpuc, x)]
			
		self.cpus_load=[ round(c/(n+1),2)  for c in cpuc] 
		if _do_print: print('CPU load per logical core:',self.cpus_load)
		self.cpus_load_phisical=[]
		self.cpu_phisical_ii=[]
		for ii in range(self.cpu_count,0,-mmm): # range(0,self.cpu_count,mmm):
			tmpmin=min( self.cpus_load[ ii-mmm:ii] ) #min( self.cpus_load[ii:ii+mmm] ) 
			minii=self.cpus_load.index(tmpmin,ii-mmm,ii) #self.cpus_load.index(tmpmin,ii,ii+mmm)
			self.cpus_load_phisical.append(tmpmin) # min value per physical core
			self.cpu_phisical_ii.append(minii) # index of the min val 
		
		tmp_to_sort=[[ii,vv] for ii,vv in enumerate(self.cpus_load_phisical)] 
		tmp_sorted=self.sortLoO(tmp_to_sort,1,False)  
		
		for ii,cc in tmp_sorted:
			toapp=self.cpu_phisical_ii[ii]
			self.sortedAll.append(toapp)
			if cc>markbusy: self.busycpus.append(toapp)
			else: self.sortedNotBusy.append(toapp)
			
		if _do_print: 
			print('CPUs list sorted:', self.sortedAll )
			print('CPUs busy:',self.busycpus )
			print('CPUs not busy:',self.sortedNotBusy )

	
	
	# opt all, ab1=all but 1, best [1], not busy = all not busy
	# OR, N<max - number of cpus 
	def getCpuList(self,opt):
	
		allowed_list=['all','ab1','best','notbusy','ab1m1']
		
		if type(opt) not in (str,int): 
			printNotif(f'\t!!! ERROR opt={opt}\n\t\tvalue must be integer between 1 and {self.cpu_count_phisical} \n\t\tor string in {allowed_list}, not type {type(opt)}.') 
			return []
			
			
		if type(opt)==int:
			if opt>self.cpu_count_phisical or opt<1: 
				printNotif(f'\tNOTE opt={opt} is not in the correct range between 1 and max for this device {self.cpu_count_phisical}\n\tSetting to {self.cpu_count_phisical-1}')
				opt=self.cpu_count_phisical-1
			return self.sortedAll[:opt]
			
		elif type(opt)==str: #except:
				
			if opt=='all': return self.sortedAll 
			elif opt=='ab1': return self.sortedAll[:-1]
			elif opt=='best': return [self.sortedAll[0]]
			elif opt=='notbusy': return self.sortedNotBusy
			# all but one min 1
			elif opt=='ab1m1': 
				if len(self.sortedAll[:-1])>0:
					return self.sortedAll[:-1]
				return self.sortedAll[0]
		  
			printNotif(f"\t!!! ERROR, opt='{opt}', string value must one of {allowed_list}.")
			
			return []


	# CPU_Load.sortLoO(self,loo,col_or_key,reverse=True)
	# works lists of lists or list of dict [pass key instead col num]
	def sortLoO(self,loo,col_or_key,reverse=True):
		def sortBy(kv):
			return kv[col_or_key]
		return sorted(loo, key=sortBy, reverse=reverse) # bytes 		
		








def printNotif(txt):
	tmp='-'.join('' for ii in range(len(txt)+16))
	print(f'\n{tmp}')
	print(txt)
	print(f'{tmp}\n')
	








# worker fun should be outside
def worker_fun_example(arg1,arg2):
	print(arg1,arg2)
	

if __name__ == '__main__': 

	# worker fun should be outside
	def bad_worker_fun_example(arg1,arg2):
		print('this wont work',arg1,arg2)

	x=CPU_Load(n=5,_do_print=True)
	cpu_list=x.getCpuList('ab1')
	print(f"Selected CPUs {cpu_list}")
	
	work=[[ii] for ii in range(4)]
	print('Work:',work)
	
	wm=WorkManager()	 
	results, workers_time=wm.startWorkers(cpu_list, worker_fun_example,  work )
	print('Printing results:')
	for k,v in results.items():
		print(f"Result for cpu {k} is {v}, took {workers_time[k]} seconds")
		
		
	# EXAMPLE HOW IT WONT WORK / STRANGELY ...	
	print('\n==== This wont work / ERROR ====')
	print('UNCOMMENT TO TEST\n')
	
	# results, workers_time=wm.startWorkers(cpu_list, bad_worker_fun_example,  work )
		
		
	