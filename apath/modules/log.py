# todo:
# finish AppMem meintenance last n days n sessions 

# import os 
from modules.helpers import os_path_exists
from modules.dateTimeOp import now_str_datetime_format, Timer, date_time_from_stamp, format_datetime, date_from_str
from modules.fileOp import jsonFromFile, jsonToFile


# todo set if limit historical mem  - how many last sessions or last days etc ... 
class AppMem:
	
	# if sess older then N days or more then 10 sess in mem - delete 
	def __init__(self,appname,session_params_dict,_keep={'lastNsess':10, 'lastNdays':30}): # append or 'new'
	
		self.appname=appname 
		self.log={}
		if os_path_exists(appname):
			self.log=jsonFromFile( appname) #self.load()
			
			
			
			
		sess_dt=sorted([date_from_str(dd,'%Y-%m-%d %H:%M:%S') for dd in list(self.log.keys())	],reverse=True)
		print(sess_dt)	
			
			
		# check sess sort for delete 
		if len(	self.log)>	_keep['lastNsess']:
			print('toclear')
			
			
			
			
			
		self.timer=Timer() #.te
		self.cur_sess_start=format_datetime(  date_time_from_stamp(self.timer.t0))
		
		self.sess_start=self.timer.t0
		self.log[self.cur_sess_start]={'finished_at':'', 'session_lifespan':'' ,'params':str(session_params_dict), 'result':''}
		
	# on close 
	def saveResult(self,res):
		self.log[self.cur_sess_start]['result']=res
		te=self.timer.end()
		self.log[self.cur_sess_start]['finished_at']=format_datetime(  date_time_from_stamp(te))
		self.log[self.cur_sess_start]['session_lifespan']=str(te-self.sess_start)
		jsonToFile(self.log,self.appname,overwrite=True)
		
		
	# log: session=start, session end, session time , parameters string , result string  ?
	
	# def log(self,):
		
		
	# def load(self):
		# return jsonFromFile(self.appname)
		# self.logname='z_log.txt' # if append continue in the same file 
		# if mode=='new':
			# self.logname='z_log_'+now_str_datetime_format('%Y_%m_%d_%H_%M_%S')+'.txt'
		


class Log:
	
	def __init__(self,mode='append'): # append or 'new'
		self.lines=[]
		self.logname='z_log.txt' # if append continue in the same file 
		if mode=='new':
			self.logname='z_log_'+now_str_datetime_format('%Y_%m_%d_%H_%M_%S')+'.txt'
		
		# self.write_file=open
		# self._open=open;
		
	def log(self,*txt):
		print(*txt)
		newlinereplace='\n                       '
		txt=' '.join([str(el).replace('\n',newlinereplace) for el in txt])
		dt=now_str_datetime_format('%Y-%m-%d-%H:%M:%S')
		self.lines.append({dt:txt})
		self.saveLine(dt,txt)
	
	def print(self,*txt): # for easy replacement 
		self.log( txt)
		
		
	def saveLine(self,dt,txt):	
		with open(self.logname,'a') as f:
			f.write(str(dt)+' : '+str(txt)+'\n')
			
			
			
	# old version better use log()	
	def addLine(self,txt,displ=True):
	
		if displ:
			if type(txt)==type( (1,) ):
				print(*txt)
			else:
				print(txt)
					
		newlinereplace='\n                       '
		if type(txt)==type( (1,) ):
			txt=' '.join([str(el).replace('\n',newlinereplace) for el in txt])
	
			
		dt=now_str_datetime_format('%Y-%m-%d-%H:%M:%S')
		self.lines.append({dt:txt})
		
		self.saveLine(dt,txt)
		
	
			
	
					