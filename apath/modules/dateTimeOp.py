#datetiem manage 
from datetime import datetime
from datetime import timedelta
# import datetime 
from time import perf_counter as time_perf_counter
from copy import copy as copy_copy

# global printlog


# datetime from timestamp 
def date_time_from_stamp(timestmp):
	return datetime.fromtimestamp(timestmp)

def path_time(timestmp):
	# timestmp=os.path.getmtime(p)
	return datetime.fromtimestamp(timestmp)


def format_datetime(dt,ff='%Y-%m-%d %H:%M:%S'):
	return str(dt.strftime(ff))

# formats examples:
# file extension str_datetime_format('-%Y-%m-%d-%H-%M-%S') '-%Y-%m-%d-%H-%M-%S'
def now_str_datetime_format(ff): # for unique files names
	return format_datetime(datetime.now(),ff)
	# dt=datetime.now()
	# print('dt',dt,ff)
	# print('dt2',dt.strftime(ff))
	# return str(dt.strftime(ff))



def date_now():
	return datetime.now()



def date_from_str(str1,ff='%Y-%m-%d'):
	try:
		return {'valid':True, 'value': datetime.strptime(str1,ff) }
	except:
		return {'valid':False, 'value':f'not proper date format {str1}, should be like {now_str_datetime_format(ff)}'}



class Timer:
	def __init__(self):
		self.start()
		self.t_init= copy_copy(self.t0)
	
	def start(self):
		self.t0=time_perf_counter()
		# return self.t0

	def end(self):
		self.te=time_perf_counter()
		return self.te
	
	# def tick2print_since_init(self,txt,units='s',prec=4):
		# return txt+f'[{units}] '+str(self.tick(units ,prec,since_init=True) )
		
		
		
	def tick2print(self,txt,units='s',prec=4,since_init=False):
		return txt+f'[{units}] '+str(self.tick(units ,prec,reset=True,since_init=since_init) )
	
	
	
	def tick(self,units='s',prec=4,reset=False,since_init=False): # units: s, m, h,d
		compare_to=self.t0 
		if since_init:
			compare_to=self.t_init 
			
		tmp= time_perf_counter()-compare_to
		if units=='m':
			tmp=round(tmp/60.0,prec)
		elif units=='h':
			tmp=round(tmp/3600.0,prec)
		elif units=='d':
			tmp=round(tmp/86400.0,prec)
		else:
			tmp=round(tmp ,prec)
			
		if reset: self.start()
		
		return tmp # print(custStr,tmp)