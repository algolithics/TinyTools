#fileOp
# MOVE save_df( )
# DEL saveDictList(self,fname,dictList)
# MOVE xlsx_read(filename )
# MOVE printDataFrameAnalysis(df,rows=3)
# MOVE csv_read(filename, cur_cfg)
# getFilePaths(mainDir,file_pattern='')
# FilePath class 
# zip/tar class
# jsonFromStr(json_str)
# clearComments(txt,comment_char='#')
# trimJsonStr
# fixJson
# jsonFromFile
# jsonToStr
# jsonToFile

# import tarfile # py built in 
# import subprocess 
#, os
from traceback import format_exc as traceback_format_exc
# import traceback
import modules.dateTimeOp as dateTimeOp
import modules.helpers as helpers # os_path_exists

# from pandas import DataFrame, read_csv
# from itertools import islice 
# from openpyxl import load_workbook
try:
	import ujson as json #
except:
	import json

global printlog


def writeTxtFile(path,towrite,mode='w'):

	encoding='UTF-8'
	if mode=='wb':
		encoding=None
		
	with open(path,mode,encoding=encoding) as f:
		f.write(towrite)
		
		

def readTxtFile(path,mode='r'):
	rr=''
	# print('ok?')
	encoding='UTF-8'
	if mode=='rb':
		encoding=None
		
	with open(path,mode,encoding=encoding) as f:
		rr= f.read()
		
	return rr

#
# JSON PROCESSING
#


	




# exception when too much coma or coma missing between values 
# if json structure is " {schema:asdf, table:qwer , from_df: zxcv } "
def jsonFromStr(json_str,fixjson=False):
	try:
		if fixjson:
			json_str=fixJson(trimJsonStr(json_str))
		# if json structure is " {schema:asdf, table:qwer , from_df: zxcv } "
		# add "" where necessary 
		if len(json_str)>2:
			if '"' not in json_str:
			
				def clear_j(jstr):
					bl_ii=jstr.find('{')
					br_ii=jstr.rfind('}')
					substr=jstr[bl_ii+1:br_ii]
					
					spl_coma=substr.split(',')
					new_str=''
					for v in spl_coma:
						spl_colo=v.split(':')
						kkey='"'+spl_colo[0].strip()+'":'
						vval='"'+spl_colo[1].strip()+'" '
						if new_str=='':
							new_str+=kkey+vval
						else :
							new_str+=', '+kkey+vval
						# if '{' in spl_colo[1]: 
							# vval=recur_split( spl_colo[1])
					return '{'+new_str+'}' 
				
				json_str= clear_j(json_str)	
				# print('json_str',json_str)
				# spl_bra_left=json_str.split('{')
				# spl_bra_right=json_str.split('}')
				
	
		return json.loads(json_str)
	except:
		err=traceback_format_exc()
		printlog.log(err )
		printlog.log(json_str )
		return {'err':err}
		


def strToDates(ddict,str_type=type('asd'), d_type=type({}), l_type=type([]) ):
	
	# def trydate(v):
		# if '__DATE__'==v[:8] :
			# print('v[8:]',v[8:])
			# return dateTimeOp.date_from_str(v[8:],'%Y-%m-%d %H:%M:%S')
		# return v
	
	# if parent elem is list of dict do recur
	if type(ddict)==l_type: # assume each element is dict in a list 
		for ii,d in enumerate(ddict):
			ddict[ii]=strToDates(d,str_type,d_type,l_type)
			
	elif type(ddict)==str_type and '__DATE__'==ddict[:8] :
		tmp=dateTimeOp.date_from_str(v[8:],'%Y-%m-%d %H:%M:%S')
		# print(tmp)
		ddict[k]=tmp['value']
	elif type(ddict)==d_type: # if is dict - analize values 
		# print('dict',ddict)
		for k,v in ddict.items():
		
			# if type(v)==str_type:
				# tmp=trydate(v)
				# ddict[k]=tmp['value']
			if type(v)==str_type and '__DATE__'==v[:8] :
				# print('v[8:]',v[8:])
				tmp=dateTimeOp.date_from_str(v[8:],'%Y-%m-%d %H:%M:%S')
				# print(tmp)
				ddict[k]=tmp['value'] #'__DATE__'+dateTimeOp.format_datetime(v) # parse dates
			elif type(v) in [d_type,l_type]:
				ddict[k]=strToDates(v,str_type,d_type,l_type) # recur if dict or list 
		
	return ddict	
	
		
# del all before first / lasst [{ }]
def trimJsonStr(jstr):

	start=len(jstr)-1
	for ss in ['{','[']:
		tmps=jstr.find(ss)
		if tmps>-1 and tmps<start:
			start=tmps
	 
	end=0
	for ss in ['}',']']:
		tmps=jstr.rfind(ss)
		if tmps>-1 and tmps>end:
			end=tmps 
	
	return jstr[start:end+1]
	
	
	
def fixJson(jstr):
	return jstr.replace("'",'"').replace("True",'true').replace("False",'false')
	
	
	
#actually uset to read dict in .py as json 		
# recognize dates '__DATE__'+dateTimeOp.format_datetime(v)
def jsonFromFile(fpath):

	if not helpers.os_path_exists(fpath): #os.path.exists(fpath):
		printlog.log('Cannot read json - path does not exist:'+fpath )
		return
		
	json_str=''
	with open(fpath, "rb") as ff:
		json_str = ff.read().decode("utf-8")
		
	json_str=fixJson(trimJsonStr(json_str))
	json_str=helpers.clearComments(json_str)
	json_str=jsonFromStr(json_str)
	if 'err' in json_str:
		return json_str
	json_str=strToDates(json_str)
	# print(json_str)
	return  json_str
	
	
# does  recur on dict to parse datetime 
def datesToStr(ddict,dt_type=type(dateTimeOp.date_now()), d_type=type({}), l_type=type([]) ):
	
	# if parent elem is list of dict do recur
	if type(ddict)==l_type: # assume each element is dict in a list 
		for ii,d in enumerate(ddict):
			ddict[ii]=datesToStr(d,dt_type,d_type,l_type)
	else: # if is dict - analize values 
		for k,v in ddict.items():
			if type(v)==dt_type:
				ddict[k]='__DATE__'+dateTimeOp.format_datetime(v) # parse dates
			elif type(v) in [d_type,l_type]:
				ddict[k]=datesToStr(v,dt_type,d_type,l_type) # recur if dict or list 
		
	return ddict
		
		
def jsonToStr(jdata,parseDates=False):
	try:
		if parseDates:
			jdata=datesToStr(jdata)
			# print(jdata)
		return json.dumps(jdata) 
	except:
		err=traceback_format_exc()
		printlog.log(err)
		return err
		
		
		

def jsonToFile(jdata,fpath,overwrite=False):
	try:
		if not overwrite and helpers.os_path_exists(fpath): #os.path.exists(fpath):
			printlog.log('Path '+fpath+' exists - not writing!')
			return
		
		json_str= jsonToStr(jdata,True)
		json_bytes = json_str.encode('utf-8')
		with open(fpath, "wb") as fout: 
			fout.write(json_bytes)
		return True
	except:
		err=traceback_format_exc()
		printlog.log(err) 
		return False







		
# if is dir file name ='' and dir name is last val in [] dir_path 
# split file name from dir path
# split dir path to list of dir names 
# os.path.exists os.path.abspath os.path.isdir
class FilePath:
		# self.dir_path=[]
		# self.file_name=''
		# self.path_exists 
		# self.is_file 
		# abspath
	def split_ext(self,tt ):
		splext=tt.split('.')
		# print(splext)
		if len(splext)>=2:
			return ('.'.join(splext[1:])).lower() # standardize lower 
		else:
			if self.is_file: return 'UNKNOWN'
			return  '__DIR__'
		
	def __init__(self,filePath):
		self.path_separator='' # needed to split file 
		self.dir_path=[]
		self.file_name=''
		self.file_ext=''
		self.is_file=False
		self.abspath=''
		self.path_exists=True if helpers.os_path_exists(filePath) else False
		
		if self.path_exists:
			self.abspath=helpers.os_path_abspath(filePath ) #"mydir/myfile.txt"
			self.is_file=False if helpers.os_path_isdir(filePath) else True
		else:
			# splext=filePath.split('.')
			self.file_ext=self.split_ext(filePath)
 
		if '\\' in filePath:
			self.path_separator='\\'

		elif '/' in filePath: 
			self.path_separator='/'
			
			
		if self.path_separator=='':
			printlog.addLine( ('\n*** Be aware of Windows path separator - should be either "/" or "\\\\", not single "\\" \n',) )
			# self.file_name=filePath
			filePath=filePath.split('/')
		else:
			filePath=filePath.split(self.path_separator)
			
		if self.is_file:
			self.dir_path=filePath[:-1]
			self.file_name=filePath[-1]
			# splext=self.file_name.split('.')
			self.file_ext=self.split_ext(self.file_name)
			# if len(splext)==2:
				# self.file_ext=splext[1].lower() # standardize lower 
			# else:
				# self.file_ext='UNKNOWN'
		else:
			self.dir_path=filePath
			
	
	
	

# def printDataFrameAnalysis(df,rows=3):
	# printlog.addLine(('\n======================File analysis:',) )
	# printlog.addLine(('==top 3 rows',) )
	# printlog.addLine((df.head(rows),'\n') )
	# printlog.addLine(('==column types',) )
	# printlog.addLine((df.dtypes,'\n') )




# def csv_read(filename, cur_cfg):
	
	# do_infer_datetime_format=False
	# if len(cur_cfg["csv_dates_columns_to_parse"])>0:
		# do_infer_datetime_format=True
	# try:
		# df=read_csv(filename, parse_dates=cur_cfg["csv_dates_columns_to_parse"],dayfirst=cur_cfg["csv_us_format_day_first"], delimiter=cur_cfg["csv_delimiter"],   dtype=cur_cfg["csv_dtype"], infer_datetime_format=do_infer_datetime_format)
		# return df
	# except:
		# printlog.addLine(('Could not read ',file_type,'file',filename) )
		# tmp=traceback_format_exc()
		
		# if 'ValueError:' in (tmp):
			# tmp2=tmp.split('ValueError:')
			# printlog.addLine(('Wrong column name to parse date? or dtype?',tmp2[1]) )
		# else:
			# printlog.addLine((tmp,) )
		
		# return None



# def xlsx_read(filename ):
 
	# wb = load_workbook(filename )
	# ws=wb[wb.sheetnames[0]] 
	
	# data = ws.values
	# cols = next(data)[0:]  
	# data = list(data) 
	# data = (islice(r, 0, None) for r in data) 
	 
	# df = DataFrame(data,index=None, columns=cols)  # df2 = df[xls_col_sel]
			
	# return df


#
# FUN TO SAVE CSV ;
# fname - name of file, csv_fdir - dir containing file 
# def save_df(csv_fname, csv_fdir, df_save, save_col_names,zipObj, only_save_zip=True):

	# if not df_save.empty:
		# fname=csv_fname
		# if not save_col_names: # then add to file name 
		
			# for cc in df_save.columns: #
				# if  len(fname)+4+1+len(cc)<128:
					# fname+='-'+cc 
				# else:
					# break
				 
		# fpath=os.path.join(csv_fdir, fname+'.csv' )
		# if save_col_names:
			# df_save.to_csv( fpath, sep=';', encoding='utf-8', index=False, header=df_save.columns)
		# else:
			# df_save.to_csv( fpath, sep=';', encoding='utf-8', index=False, header=None)
			
			
		# if zipObj.zipOn and only_save_zip:
			# zip_file=os.path.join(csv_fdir, fname+'.zip' ) #csv_fname+'.zip'
			# zipObj.saveZipDelOrig(zip_file, fpath)
			# return zip_file
			
		# return fpath
	# else:
		# printlog.addLine(('empty df',) )
		# return ''
		
		

#
#  SAVE dict list [{},{}]
# 	
# def saveDictList(self,fname,dictList): 
		
	# with open(fname,'w') as f:
		# for dd in dictList:
			# for k,v in dd.items(): 
				# f.write(str(k)+' : '+str(v)+'\n')
				
				
	
