#helper functions

from string import  ascii_lowercase as string_ascii_lowercase, digits as string_digits
from psutil import virtual_memory as psutil_virtual_memory
from os.path import exists as os_path_exists, split as os_path_split, join as os_path_join,abspath as os_path_abspath,  isdir as os_path_isdir
from numpy import argsort as np_argsort


def sortedIndexes(llist,reverse=True):
	aa=np_argsort(llist)
	if reverse:
		aa=aa[::-1]
	return aa 


# also works for list of dict col i key 
def sortLoO(loo,col_or_key,reverse=True):

	def sortBy(kv):
		return kv[col_or_key]
			
	return sorted(loo, key=sortBy, reverse=reverse) # bytes 




def alphabetList():
	return list(string_ascii_lowercase)

# os.path.exists os.path.abspath os.path.isdir
def listRemoveEachVal(llist,val):		
	return list(filter(lambda a: a != val, llist))

def list_diff(first, second):
	# second = set(second)
	return [item for item in first if item not in second]

# return generator and count of iterations 
def df_chunker(df_or_seq, size): # from http://stackoverflow.com/a/434328
	cc=1.0* len(df_or_seq) / size
	if int(cc)!=cc:
		cc=int(cc)+1
		
	return (df_or_seq[pos:pos + size] for pos in range(0, len(df_or_seq), size)), cc

def dataframe_mem(df):
	col_mem=df.memory_usage(index=True, deep=True)
	gb=1024*1024*1024
	mb= 1024*1024
	tmp=col_mem.sum()
	return {'bytes':tmp, 'mb':round(1.0*tmp/mb,1), 'gb':round(1.0*tmp/gb,1),  }


# if faster needed : https://stackoverflow.com/questions/44634972/how-to-access-a-field-of-a-namedtuple-using-a-variable-for-the-field-name
def mem_status():
	mem=psutil_virtual_memory()
	# svmem(total=17179451392, available=14597283840, percent=15.0, used=2582167552, free=14597283840)
	gb=1024*1024*1024
	return {'total_gb':round(1.0*getattr(mem, 'total')/gb,1), 'available_gb':round(1.0*getattr(mem, 'available')/gb,1)  }
	

def chunkify(strdata,n):
	return [strdata[ii:ii+n] for ii in range(0, len(strdata), n)]
	
	

# num format 1'234'567'890.098
def strFormatNum(n,sep="'"):
	s=str(n)
	spl=s.split('.')
	# format int part:
	ilen=len(spl[0])
	m=ilen%3
	res=''
	if m>0:
		res=spl[0][:m] 
		
	for ii in range(m,ilen,3):
		res+=sep+spl[0][ii:ii+3]
		
	if len(spl)>1:
		res+='.'+spl[1]
		
	if res[0]==sep:
		res=res[1:]
		
	return res
	
	
#nice dict to str format visual presentation
def strFormatDict(descr,tt=''):
		
	ttshift='    '
	if type(descr)!=type({}):
		return str(descr)
	kk=descr.keys()
	
	tmpstr='{\n'
	for k in kk:
		v2=strFormatDict(descr[k],tt+ttshift) #+ '  '
		tmpstr+=ttshift+tt+'"'+k+'":'+v2+',\n'
		
	tmpstr+=tt+'}'
	return tmpstr
	
# clean comments
def clearComments(txt,comment_str='#'):
	newtxt=''
	lines=txt.split('\n') 
	comment_str_len=len(comment_str)
	for ll in lines:
		lls=ll.strip() # remove whitespace 
		lls_len=len(lls)
		if lls_len==0: 
			continue
		elif lls_len>=comment_str_len and lls[:comment_str_len]==comment_str:# first char is comment char - remove whole line 
			continue
		else:
			ii=lls.find(comment_str)
			if ii>-1:
				lls=lls[0:ii] 
		newtxt+=lls+'\n' 
	return newtxt
	

def main():
	# print(alphabetList())
	# aa=alphabetList()
	pass
	# print(mem_left())
		
if __name__ == "__main__":
	main()