
# return whole summary
#  def summary(self,ddict=None,tt='',maxlvl=0,fromKey='',topN=0): # nice dict string 
# return file ext summary 
#  def extSummary(self,measure='bytes'):
# return files summary 
#  def fileIndexSummary(self,mode='alike')
# new analize: self.dirTree=analizepath(self,ddir,_iter=0 )
#  init __init__(self,root,byteUnit=1 )
# setUnits(self,byteUnit):
#  setSorting(self,dire='desc',measure='bytes')
# search(self,cond_dict) return list of ff 



# add dir op rename files folders delete 
	
# DOCS:
# class AnalizePath(root,byteUnit=1 )
# 	extSummary( ,measure='bytes')
#   setUnits( ,byteUnit):
#   setSorting( dire='desc',measure='bytes'):
# file and folder :   summary( ,ddict=None,tt='',maxlvl=0,fromKey='',topN=0)
# raw result: niceDict(self,ddict,tt='' )
# fileIndexSummary(self,mode='alike')
#
# standaloe fun getFilePaths(mainDir,file_pattern='',case_sens=False,only_main=False)




from os import listdir as os_listdir, walk as os_walk
from os.path import exists as os_path_exists, getmtime as os_path_getmtime, isdir as os_path_isdir, getsize as os_path_getsize, sep as  os_path_sep, join as os_path_join, abspath as os_path_abspath
# import os
from copy import copy as copy_copy
import modules.dateTimeOp as dateTimeOp
from modules.helpers import strFormatNum
# import  dateTimeOp as dateTimeOp
# from  helpers import strFormatNum



# return longest common root for paths ready to cut for simplifying archive paths 
def paths_common_root(paths):
	
	tmp=paths[0].split(os_path_sep)
	longest_p_split=tmp[:-1]
	longest_p=os_path_sep.join(longest_p_split)
	best_len=len(longest_p)
	
	for p in paths[1:]:
		
		if longest_p==p[:best_len]: # same
			continue
			
		tmp=p.split(os_path_sep) # if not same - cut while 
		tmpsplit=[]
		ii=0
		while ii<len(longest_p_split) and longest_p_split[ii]==tmp[ii]:
			tmpsplit.append(longest_p_split[ii])
			ii+=1
			
		longest_p_split=tmpsplit
		longest_p= os_path_sep.join(longest_p_split)
		best_len=len(longest_p)	 # print('new best',longest_p_split,longest_p,best_len)
		
	return longest_p+os_path_sep
	
	
	
	
	
def getFilePathsByExt(mainDir,extList=['.py','.sql'] ):
	
	# def sameExt(s1,s2):
		# if s1.lower()==s2.lower():
			# return True
		# return False
		
	eelen=[len(ee) for ee in extList]
	extList=[ee.lower() for ee in extList]
	paths=[]
		
	for root, dirs, files in os_walk(mainDir): 
		for file in files:
			f=file.lower()
			for ii,ee in enumerate(extList):
				if ee==f[-eelen[ii]:]: #sameExt(ee,file[-eelen[ii]:]):
					paths.append( os_path_abspath(os_path_join(root,file)))
					break # if matched by 1 rule enough 
	return paths
	
	
	
	
######################################## simple file search in all subdirs 
# example asdf*.txt * is wild card 
# if empty pattern return all files 
def getFilePaths(mainDir,file_pattern='',case_sens=False,only_main=False):

	def isEqual(s1,s2):
		if not case_sens:
			if s1.lower()==s2.lower():
				return True
		elif s1 ==s2 :
			return True
		return False

	pat_split=file_pattern.split('*')
	l1,l2=0,0 #l2=0
	if file_pattern!='':
		l1=len(pat_split[0])
		if len(pat_split)>1:
			l2=len(pat_split[1])
	
	paths=[]		
	for root, dirs, files in os_walk(mainDir): 
		if only_main and root!=mainDir:
			continue
		
		for file in files: # tmpflen=len(file) 
			if file_pattern!='' :
				if l1>0 and l2>0 :
					if isEqual(pat_split[0],file[:l1]) and isEqual(pat_split[1],file[-l2:]) : #pat_split[0]==file[:l1] and pat_split[1]==file[tmpflen-l2:]:
						paths.append( os_path_abspath(os_path_join(root,file))	)	
				elif l1>0:
					if isEqual(pat_split[0],file[:l1]): #pat_split[0]==file[:l1]:
						paths.append(os_path_abspath(os_path_join(root,file)) )
				elif l2>0 :
					if isEqual(pat_split[1],file[-l2:]):
						paths.append(os_path_abspath(os_path_join(root,file)) )
			else:
				paths.append(os_path_abspath(os_path_join(root,file)))
		
	return paths
	
	
	

	
	
	
# add file indexing for similar files recognition:
# fname:[{path size updated }]

class AnalizePath: # (self,root,byteUnit=1 )

	def setUnits(self,byteUnit):
		allowedUnits=[1, 1024, 1024*1024, 1024*1024*1024] # use after analyzis done ! 
		unitsList=[' bytes',' kB',' MB',' GB']
		self.byteUnit=1 #byteUnit
		self.units='bytes'
		if byteUnit in allowedUnits:
			self.byteUnit=byteUnit 
			ii=allowedUnits.index(byteUnit)
			self.units=unitsList[ii]
			
	def setSorting(self,measure='bytes',dire='desc'):
		self.sortingDirection='desc'
		if dire=='asc':
			self.sortingDirection='asc'
		self.sortingMeasure='bytes'
		
		if measure in ['updated','count'] :
			self.sortingMeasure=measure
	

	
	def __init__(self,root,byteUnit=1 ,measure='bytes',dire='desc'): 
		self.setUnits( byteUnit)
		self.setSorting(measure ,dire)
		
		self.files_index={} # 'list':[], '#':0 fname:[ {fpath, size, updated} ]
		self.all_keys=['root', 'folders', 'files', 'file-ext', 'files-bytes', 'folders-bytes', 'ord', 'files-b', 'files-u','folders-b', 'folders-c']
		# self.root=root
		self.files_full_paths=[]
		
		self.dirTree=self.analizepath(root )
		
	

	def testLike(self,txt,cond): 
		# print(cond)
		if not cond['case_sens'] :
			txt=txt.lower() 
			
		def _compare(tx1,tx2):
			if not cond['reversed']: #like_unlike=='like' : 
				if tx1==tx2:
					return True
				return False
			else: # 'unlike' in cond: 
				if tx1==tx2:
					return False
				return True
				
		for ii,spl in enumerate(cond['search_split'] ):
		
			# print(ii,spl,cond['search_case'],'\n\n')
			# if reverse cond must match all not in 
			# if normal can return after getting true 
			if cond['search_case'][ii]=='equal':
				if _compare(spl[0],txt) and not cond['reversed']:
					return True
				elif not _compare(spl[0],txt) and cond['reversed']:
					return False
			elif cond['search_case'][ii]=='starts':
				if _compare(spl[0], txt[:cond['search_split_lengths'][0]] ) and not cond['reversed']:
					return True
				elif not _compare(spl[0], txt[:cond['search_split_lengths'][0]] ) and cond['reversed']:
					return False	
			elif cond['search_case'][ii]=='ends':
				if _compare(spl[1], txt[-cond['search_split_lengths'][1]:] ) and not cond['reversed']:
					return True
				elif not _compare(spl[1], txt[-cond['search_split_lengths'][1]:] ) and cond['reversed']:
					return False			
			else: # 'starts*ends'
					
				v1=_compare(spl[0], txt[:cond['search_split_lengths'][0]] )
				v2=_compare(spl[1], txt[-cond['search_split_lengths'][1]:] ) 
				if v1*v2  and not cond['reversed']:
					return True
				elif not v1*v2  and  cond['reversed']:
					return False
					
		if not cond['reversed']:
			return False
		elif cond['reversed']:
			return True
		
		
		
	def search(self,cond_dict):
	
	
		self.search_cond=copy_copy(cond_dict)
		# preaalize conditions and add some variables:
		for kk,vv in self.search_cond.items():
			if type(vv)==type({}):
				# if kk=='parrent_dir_pattern':
				# if 'unlike' in vv: 
					# self.search_cond[kk]['search']=vv['unlike']
					# self.search_cond[kk]['reversed']=True
					
				# else:
					# self.search_cond[kk]['search']=vv['like']
					# self.search_cond[kk]['reversed']=False
					
				# if vv['case_sens']=='yes':
					# vv['case_sens']=True
				# else:
					# vv['case_sens']=False
				if not vv['case_sens']:
					self.search_cond[kk]['like']=[el.lower() for el in self.search_cond[kk]['like']]
					# search may have ist of conditions 
					# for each needed parsms:
					
				self.search_cond[kk]['search_split']=[]
				self.search_cond[kk]['search_split_lengths']=[]
				self.search_cond[kk]['search_case']=[]
				for el in self.search_cond[kk]['like']:
					tmp=el.split('*')
					if kk=='ext':
						if tmp[0][0]=='.':
							tmp[0]=tmp[0][1:]
					self.search_cond[kk]['search_split'].append(tmp)
					self.search_cond[kk]['search_split_lengths'].append([len(ss) for ss in tmp ])
					
				
					spllen=len(tmp)
					if spllen==1: self.search_cond[kk]['search_case'].append('equal')
					else:
						if len(self.search_cond[kk]['search_split'][1])==0: self.search_cond[kk]['search_case'].append('starts')
						elif len(self.search_cond[kk]['search_split'][0])==0: self.search_cond[kk]['search_case'].append('ends' )
						else: self.search_cond[kk]['search_case'].append('starts*ends'		)	 
					 		 
		# exit() 
		#first analize:
		if self.root!=self.search_cond['search_path']:
			# print('self.analizepath')
			# print('testing - diff PATH    ',self.root,self.search_cond['search_path'])
			self.dirTree=self.analizepath( self.search_cond['search_path'] )
		# else:	
			# print('testing - saME PATH OK ',self.root,self.search_cond['search_path'])
			
		for dd in ['search_path','end_path','__allowed_values__']:
			if dd in self.search_cond:
				del self.search_cond[dd]
		
		# print(self.search_cond)	
		# self.files_full_paths.append({'path':tmpp, 'bytes':tmps, 'time':dt, 'fname':ii, 'parent_dir_name':tmpparent[-1]})	
		self.found=[]
		# print('self.search_cond',self.search_cond)
		for ff in self.files_full_paths:
			# if ff['ext']=='py':
				# print('test',ff)
			match=True
			# print('\ntesting',ff)
			for cd,vv in self.search_cond.items():
				# print('cd,vv',cd,vv)
				if cd=='parrent_dir_pattern':
					if self.testLike(ff['parent_dir_name'],vv) :
						continue #match*=self.testLike(ff['parent_dir_name'],vv) 
					# else:
						# print('failed at',cd)
				elif cd=='ext':
					if self.testLike(ff['ext'],vv):
						continue #match*=self.testLike(ff['ext'],vv)
					# else:
						# print('failed at',cd)
					 
				elif cd=='file_pattern':
					if self.testLike(ff['fname'],vv):
						continue #match*=
					# else:
						# print('failed at',cd)
				elif cd=='created_after':
					if ff['time']>vv: #match*=True
						continue
					# else:
						# print('failed at',cd)
				elif cd=='created_before':
					if ff['time']<vv: #match*=True
						continue
					# else:
						# print('failed at',cd)
				elif cd=='min_bytes':
					if ff['bytes']>=vv: #match*=True
						continue
					# else:
						# print('failed at',cd)
				elif cd=='max_bytes':
					if ff['bytes']<=vv: #match*=True
						continue
					# else:
						# print('failed at',cd)
				match=False
				break
				
			if match:
				self.found.append(ff)
			
		return self.found
		
	
	 
	# os_path_exists os_path_getmtime os_listdir os_path_isdir os_path_getsize os_path_sep os_path_sep
	
	def analizepath(self,ddir,_iter=0 ):
		if _iter==0:
			if not os_path_exists(ddir):
				# print('no such path',ddir)
				return {'err':'no such path '+ddir}
			else: 
				self.root=ddir
				self.root_dir_list=ddir.split(os_path_sep)
				# print(self.root_dir_list)
		

		cur_dict={'root':ddir, 
				'folders':{},
				'files':{}, 
				'file-ext':{}, 
				'files-bytes':0, 
				'folders-bytes':0, 
				'ord':{'files-b':[], 'files-u':[],'folders-b':[], 'folders-c':[], 'folders-u':[] },
				'updated_at':dateTimeOp.path_time(os_path_getmtime(ddir)) #dtdict['value']
				}
		items=os_listdir(ddir)
		
		for ii in items:
		
			tmpp=os_path_join(ddir,ii)
			
			if os_path_isdir(tmpp): #recur 
			
				cur_dict['folders'][ii]=self.analizepath(os_path_join(ddir,ii),_iter=_iter+1) #increase cur folder bytes by subfolder bytes and sub file bytes 
				cur_dict['folders-bytes']+=cur_dict['folders'][ii]['files-bytes']+cur_dict['folders'][ii]['folders-bytes']
				
				# update updated at of folder to max file updated at :
				# for cur_dict['folders'][ii]
				cur_dict['updated_at']=max(cur_dict['updated_at'],cur_dict['folders'][ii]['updated_at'])
				
				if cur_dict['file-ext']=={}: # if cur empty create - rewrite 
					cur_dict['file-ext']=cur_dict['folders'][ii]['file-ext'].copy() 
				else: # else add
					for ee,vv in cur_dict['folders'][ii]['file-ext'].items():
						if ee not in cur_dict['file-ext']:
							cur_dict['file-ext'][ee]=vv.copy()  
						else:
							cur_dict['file-ext'][ee]['count']+=vv['count']
							cur_dict['file-ext'][ee]['bytes']+=vv['bytes']
					
			else: #file
				tmps=os_path_getsize(tmpp)
				
				dt=dateTimeOp.path_time(os_path_getmtime(tmpp))
				
				tmpparent=ddir.split(os_path_sep)
				# print(tmpparent)
				# exit()
				
				
				if ii not in self.files_index:
					# {'list':[], '#':0}
					self.files_index[ii]={'list':[{'path':tmpp,'size':tmps,'updated':dt}], '#':1} # 'name':ii, alredy in key
				else:
					self.files_index[ii]['list'].append({'path':tmpp,'size':tmps,'updated':dt})
					self.files_index[ii]['#']+=1
				
				cur_dict['updated_at']=max(cur_dict['updated_at'],dt) # update folder age based on files 
				
				fext_split=ii.split('.') #[-1].lower()
				fext='unknown'
				if len(fext_split)>1:
					fext=fext_split[-1].lower()
					
				if fext not in cur_dict['file-ext']:
					cur_dict['file-ext'][fext]={'count':1,'bytes':tmps}
				else:
					cur_dict['file-ext'][fext]['count']+=1
					cur_dict['file-ext'][fext]['bytes']+=tmps
					
				# parent dir name only if not root dir:
				dir_branch=os_path_sep.join(tmpparent[len(self.root_dir_list):]) # dirs after root 
				# self.root_dir_list
				self.files_full_paths.append({'path':tmpp, 'bytes':tmps, 'time':dt, 'fname':ii, 'ext':fext,'parent_dir_name':tmpparent[-1], 'dir_branch':dir_branch})
				
				cur_dict['files'][ii]={'bytes':tmps,'updated_at':dt}
				cur_dict['files-bytes']+=tmps
		
		self.calcOrders( cur_dict)
		
		return 	cur_dict.copy()
		
		
		
		
	def calcOrders(self,cur_dict):	
		
		def sortBy(kv):
			return kv[1]
		
		
		files_dicts=[] # 'files-b':[], 'files-u'
		files_dicts_u=[]
		for k,v in cur_dict['files'].items():
			files_dicts.append([k,v['bytes']])
			files_dicts_u.append([k,v['updated_at']])
		
		files_dicts=sorted(files_dicts, key=sortBy, reverse=True) # bytes 
		cur_dict['ord']['files-b']=[fb[0] for fb in files_dicts]
		
		files_dicts_u=sorted(files_dicts_u, key=sortBy, reverse=True) # updated at 
		cur_dict['ord']['files-u']=[fb[0] for fb in files_dicts_u]
		
		
		folder_dicts=[]
		folder_dicts_c=[]
		folder_dicts_u=[]
		for k,v in cur_dict['folders'].items():
			folder_dicts.append([k,v['files-bytes']+v['folders-bytes']])  
			folder_dicts_c.append([k,len(v['files'])])  
			folder_dicts_u.append([k,v['updated_at']])
		
		folder_dicts=sorted(folder_dicts, key=sortBy, reverse=True)
		cur_dict['ord']['folders-b']=[fb[0] for fb in folder_dicts]
		
		folder_dicts_c=sorted(folder_dicts_c, key=sortBy, reverse=True)
		cur_dict['ord']['folders-c']=[fb[0] for fb in folder_dicts_c]
		
		folder_dicts_u=sorted(folder_dicts_u, key=sortBy, reverse=True)
		cur_dict['ord']['folders-u']=[fb[0] for fb in folder_dicts_u]
		
		
		
	# return whole summary
	# def summary(self,ddict=None,tt='',maxlvl=0,fromKey='',topN=0): # nice dict string 
	# return file ext summary 
	# def extSummary(self,measure='bytes'):
	# return files summary 
	# def fileIndexSummary(self,mode='alike')
		
	# self.files_index[ii]={'list':[{'path':tmpp,'size':tmps,'updated':dt}], '#':1}	
	# mode in alike, biggest, newest
	def fileIndexSummary(self,mode='alike'):	
	
		if mode not in ['alike','biggest','newest','oldest']:
			mode='alike'
	
		def sortBy(kv):
			return kv[1] 
		
		totalsize=self.dirTree['files-bytes']+self.dirTree['folders-bytes']
		totalsizestr=strFormatNum( int( round(totalsize/self.byteUnit,0) ))
		tmpstr='*** File index summary / '+mode+' for root dir '+self.root+', size: '+totalsizestr+self.units
		
		if mode=='alike':
			
			sortable=[ [k,  v['#']  ] for k,v in self.files_index.items() ]
			for ii,vv in enumerate( sorted(sortable, key=sortBy, reverse=True) ):
			
				if mode=='alike' and vv[1]<2: # count 1 break 
					break
					
				tmpstr+='\n\t'+str(ii+1)+'. "'+vv[0]+'", count:'+str(vv[1])
				tmpl=[ [ll['path'],  ll['updated'] ,  ll['size']  ]  for ll in self.files_index[vv[0]]['list'] ]
				
				for jj,ww in enumerate(sorted(tmpl, key=sortBy, reverse=True)):
					ww2str=str(ww[2])
					# if self.byteUnit>1:
					ww2str=strFormatNum( int( round(ww[2]/self.byteUnit,0) ))
					tmpstr+='\n\t\t'+str(jj+1)+'. "'+ww[0].replace(self.root,'.')+'", size: '+ww2str+self.units+', updated: '+dateTimeOp.format_datetime(ww[1],ff='%Y-%m-%d %H:%M:%S') 
					
		else: #if mode=='biggest':
			tmpord=['size','updated','path']
			_rev=True
			if mode in ['newest','oldest']:
				tmpord=['updated','size','path']
				if mode =='oldest':
					_rev=False
				
				
			sortable=[ [k,  ll[tmpord[0]],  ll[tmpord[1]], ll[tmpord[2]]   ] for k,v in self.files_index.items() for ll in v['list']  ]
			for ii,vv in enumerate( sorted(sortable, key=sortBy, reverse=_rev) ):
				# vv1=vv[1]
				# vv2=vv[2]
				vv1,vv2=vv[1],vv[2]
				if mode in ['newest','oldest'] :
					vv1,vv2=vv[2],vv[1]
					
				size_prct=' ['+str(int( round(100*vv1/totalsize,0) ))+'%] '
				
				vv1str=str(vv1)
				# if self.byteUnit>1: 
				vv1str=strFormatNum( int( round(vv1/self.byteUnit,0) )) 
				tmpstr+='\n\t'+str(ii+1)+'. "'+vv[0]+'", size: '+vv1str+self.units +size_prct+', updated: '+dateTimeOp.format_datetime( vv2)+', path: '+vv[3].replace(self.root,'.')
					
		return tmpstr
	
	
	
	# return whole summary
	# def summary(self,ddict=None,tt='',maxlvl=0,fromKey='',topN=0): # nice dict string 
	# return file ext summary 
	def extSummary(self,measure='bytes'):
		
		tmpm='bytes'
		if measure=='count':
			tmpm='count'
		
		def sortBy(kv):
			return kv[1]
			
		sortable=[ [k,v[tmpm], v['bytes'], v['count'] ] for k,v in self.dirTree['file-ext'].items() ]
		
		totalbytes=sum([x[2] for x in sortable ])
		totalcount=sum([x[3] for x in sortable ])
		
		total_bytes_str=str(totalbytes)
		# if self.byteUnit>1:
		total_bytes_str=strFormatNum( int( round(totalbytes/self.byteUnit,0) ))+self.units
				
		tmpstr='*** File extensions summary, total '+str(totalcount)+' files '+str(total_bytes_str)
		
		for ii,vv in enumerate( sorted(sortable, key=sortBy, reverse=True) ):
			
			tmpsize=self.dirTree['file-ext'][vv[0]]['bytes']
			# if self.byteUnit>1:
			tmpsize=strFormatNum( int( round(tmpsize/self.byteUnit,0) ))
			
			bytes_prct=str( int( round(100*self.dirTree['file-ext'][vv[0]]['bytes']/totalbytes,0) ))+'%'
			count_prct=str( int( round(100*self.dirTree['file-ext'][vv[0]]['count']/totalcount,0) ))+'%'
			
			tmpstr+='\n\t"'+vv[0]+'": count '+str(self.dirTree['file-ext'][vv[0]]['count'])+' ['+count_prct+'], size '+str(tmpsize)+self.units+' ['+bytes_prct+']'
		
		return tmpstr
		
		 
		 
		 
		 
		 
	# return whole summary
	def summary(self,ddict=None,tt='',maxlvl=0,fromKey='',topN=0): # nice dict string 
	
		if ddict==None:
			ddict=self.dirTree
			
		ttshift='    ' #'\t'
		if type(ddict)!=type({}): 
			return str(ddict)
			
		kk=ddict.keys() 
		inst_c=1+int(len(tt)/len(ttshift)) # mark stars 
		
		if maxlvl>0 and inst_c>maxlvl:
			return ''
		
		lvl='[LVL '+str(inst_c)+'] ' 
		
		if 'ord' in kk:
		
			folders_bl=len(ddict['ord']['folders-b'])
			files_bk=len(ddict['ord']['files-b'])
			if folders_bl+files_bk<1:
				return ''
			
			tmpstr=''			
			if inst_c==1:
				tmpstr='*** View sorted by '+self.sortingMeasure+' '+self.sortingDirection
				if topN>0: tmpstr+=', top '+str(topN)
				tmpstr+='\n'
				
			tmpstr+='{\n\n'
			
			if fromKey!='':
				fromKey='in "'+fromKey+'"'
				
			### FOLDERS on that LVL
			tot_bytes=ddict['folders-bytes']+ddict['files-bytes']
			tot_bytes=strFormatNum( int(round(tot_bytes/self.byteUnit,0)))
			
			tmpstr+=ttshift+tt+lvl+' Folders '+fromKey+' #'+str(folders_bl)+' totl. size: '+tot_bytes+self.units+':,\n'
			
				
			_folder_sorting='folders-b'
			if self.sortingMeasure=='updated': _folder_sorting='folders-u'
			elif self.sortingMeasure=='count': _folder_sorting='folders-c'
			
			ii_list=[ (ii,ff) for ii,ff in enumerate(ddict['ord'][_folder_sorting])]
			
			if  self.sortingDirection=='asc':
				ii_list=[ii_list[jj] for jj in range(len(ii_list)-1,-1,-1)]
			
			cc=1
			for ii,ff in ii_list: # for ii,ff in enumerate(ddict['ord']['folders-b']):
				if topN>0 and cc>topN:
					tmpstr+=ttshift+tt+str(cc)+'. "..." ,\n'
					break
				
				files_size= ddict['folders'][ff]['files-bytes']
				subf_size=ddict['folders'][ff]['folders-bytes']
				total_size=files_size+subf_size
				# if self.byteUnit>1:
				files_size=strFormatNum( int( round(files_size/self.byteUnit,0) ))
				subf_size=strFormatNum( int(round(subf_size/self.byteUnit,0)))
				total_size=strFormatNum( int(round(total_size/self.byteUnit,0)))
				# else:
					# total_size=str(total_size)
				
				v2=self.summary(ddict['folders'][ff],tt+ttshift, maxlvl, ff,topN ) #
				# str_size=' total'+': ' + total_size+self.units+ ', files: '+ files_size+self.units+ ', subfolders: '+ subf_size+self.units
				str_size=f" total: {total_size}{self.units}, files: {files_size}{self.units}, subfolders:{subf_size}{self.units}"
				ff='"'+ff+'" updated '+ dateTimeOp.format_datetime(ddict['folders'][ff]['updated_at']) #  str(   ddict['folders'][ff]['updated_at']   )
				tmpstr+=ttshift+tt+str(cc)+'. '+ff+str_size+' '+v2+',\n'
				cc+=1
				
				
			### FILES on that LVL ddict['files-bytes']
			tmpstr+='\n'+ttshift+tt+lvl+' Files '+fromKey+' #'+str(files_bk)+' totl. size: '+strFormatNum( int(round(ddict['files-bytes'] /self.byteUnit,0)))+self.units+':,\n'
			# tmpstr+=ttshift+tt+lvl+' Folders '+fromKey+' #'+str(folders_bl)+' totl. size: '+tot_bytes+self.units+':,\n'
			
			_files_sorting='files-b'
			if self.sortingMeasure=='updated': _files_sorting='files-u' 
			
			ii_list=[ (ii,ff) for ii,ff in enumerate(ddict['ord'][_files_sorting])] 
			if  self.sortingDirection=='asc':
				ii_list=[ii_list[jj] for jj in range(len(ii_list)-1,-1,-1)]
			
			# cur_dict={'root':ddir, 'folders':{},'files':{}, 'file-ext':{}, 'files-bytes':0, 'folders-bytes':0, 'ord':{'files-b':[], 'files-u':[],'folders-b':[], 'folders-c':[] }}
			# files= {'bytes':tmps,'updated_at':dt}
			cc=1
			for ii,ff in ii_list: #enumerate(ddict['ord']['files-b']):
			
				if topN>0 and cc>topN:
					tmpstr+=ttshift+tt+str(cc)+'. "..." ,\n'
					break
					
				files_size= ddict['files'][ff]['bytes'] 
				# if self.byteUnit>1:
				files_size=strFormatNum( int( round(files_size/self.byteUnit,0) )) 
				
				str_size=', size: '+ str(files_size)+self.units+', updated '+ dateTimeOp.format_datetime(ddict['files'][ff]['updated_at'])
				
				tmpstr+=ttshift+tt+str(cc)+'. "'+ff+'"'+str_size+',\n'
				cc+=1
				
			tmpstr+=tt+'}'
			return tmpstr	
		return ''
	
		
		
		
		
		
		
	# ddict keys: root, folders, files, 'file-ext', 'files-bytes' 'folders-bytes' 'ord':{'files-b':[], 'files-u':[],'folders-b':[], 'folders-c':[] }}
	def niceDict(self,ddict,tt='' ): # nice dict string 
	
		ttshift='    ' #'\t'
		if type(ddict)!=type({}): 
			return str(ddict)
			
		kk=ddict.keys() 
		tmpstr='{\n'
		
		for k in kk:
			v2=self.niceDict(ddict[k],tt+ttshift  ) 
			tmpstr+=ttshift+tt+'"'+k+'":'+v2+',\n' 
			
		tmpstr+=tt+'}'
		return tmpstr	
		
	
	
	
	
	
	
	
	
	
	
		
	
	
		
	

if __name__ == "__main__": 
	
	print(getFilePathsByExt('.\\..',extList=['.PY','.pyd'] ))