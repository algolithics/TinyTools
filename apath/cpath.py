# copy from to using ext
# Example: python copyExt.py modules new_modules py
# to compile python -m nuitka cryptor.py


from traceback import format_exc as traceback_format_exc
from os import makedirs as os_makedirs, walk as os_walk
from os.path import split as os_path_split, sep as os_path_sep, join as os_path_join
from sys import argv as sys_argv
from modules.helpers import os_path_exists,os_path_abspath,listRemoveEachVal
from modules.log import Log #, now_str_datetime_format

# import modules.fileOp as fop
from shutil import copyfile as shutil_copyfile, copymode as shutil_copymode, copystat  as shutil_copystat

global printlog



def clearDirPath(ppath): # ensure no ending \\ or / 
	tmp=ppath.split(os_path_sep)
	if '' in tmp: tmp=listRemoveEachVal(tmp,'') 
	return tmp
	
	


# test against bad path of dest like ending with \\ 
def copyPaths(rootDir,pathList,dest='',overwrite=False):

	# if dest=='': # gen default backup folder 
		# dest=now_str_datetime_format('backup_%Y%m%d_%H_%M_%S_%f')
	# else:
		# dest=os_path_sep.join(clearDirPath(dest))
		
	if os_path_exists(dest):
		if not overwrite:
			printlog.log(f'\nDestination {dest} already exist and overwrite is off.' )
			return
	else:	
		os_makedirs(dest)
		 
	dest=os_path_abspath(dest)
	rootDir_abs=os_path_abspath(rootDir)
	# print(rootDir,rootDir_abs,dest)
	# return
	
	# if provided path == abs path ... ?
	# new paths should be = dest path + 
	
	for dd in pathList:
	
		head,tail=os_path_split(dd ) 
		add_dir=head.replace(rootDir_abs,dest)

		dir_list=clearDirPath(add_dir) 
		add_dir=os_path_sep.join(dir_list) 
		 
		testdir=os_path_join(dest,add_dir) 
		# return
		if not os_path_exists(testdir):  
			if len(dir_list)>0: #add_dir!='': # first create dir tree for file 
				dirtodo= os_path_join(dest,add_dir)
				if not os_path_exists(dirtodo): os_makedirs(dirtodo)
				 	
		copy_to=os_path_join(dest,add_dir,tail) 
		
		try:
			shutil_copyfile(dd , copy_to)
			shutil_copymode(dd , copy_to)
			shutil_copystat(dd , copy_to)
		except:
			err=traceback_format_exc()
			printlog.log(err )
			return
			
		if not os_path_exists(copy_to):
			printlog.log(f"Error when copying {dd['path']} to {copy_to}" )
			return
			
	printlog.log(f"\nCopy finished [{len(pathList)} files] and verified, see {dest}" )
	# printlog.log(f"\nCopy {dd} to {copy_to}" )
	
	


# add arc option ? opening arc / tar and adding files ?? 
# overwriting existing? ore create with new date ?
	
def getFilePathsByExt(mainDir,extList=['.py','.sql'] ):
	 
	eelen=[len(ee) for ee in extList]
	extList=[ee.lower() for ee in extList]
	extListLen=len(extList)
	paths=[]
		 
	for root, dirs, files in os_walk(mainDir):  
		for file in files:
			f=file.lower()
			if extListLen>0:
				for ii,ee in enumerate(extList):
					if ee==f[-eelen[ii]:]: #sameExt(ee,file[-eelen[ii]:]):
						paths.append( os_path_abspath(os_path_join(root,file)))
						break # if matched by 1 rule enough 
			else: # all files
				paths.append( os_path_abspath(os_path_join(root,file)))
	return paths
	
	
			
def main1(sys_argv):
	global printlog
	printlog=Log()
	# fop.printlog=printlog
	
	if len(sys_argv)<3:
		printlog.log('Need at least 2 arguments: dir to copy and name of new dir. Optional arguments - list of extensions')
		printlog.log('Example: python copyExt.py modules new_modules py')
		printlog.log('\t will backup modules to new_modules only copying .py files')
		return
	
	from_dir=sys_argv[1]
	to_dir=sys_argv[2]
	
	extList=[]
	if len(sys_argv)>3:
		extList=[ el if el[0]=='.' else '.'+el for el in sys_argv[3:] ]
		
	pathList=getFilePathsByExt(from_dir,extList )
	
	copyPaths(from_dir,pathList,to_dir)
	print(extList if len(extList)>0 else 'All' ,'files copied from',from_dir,'to',to_dir)
	# print('Check z_logs.txt for details')
	

# args: search file, destination folder 
if __name__ == "__main__":
	
	main1(sys_argv)