# tar directory or selected files using multiple CPUs
# untar is much faster so using single core
# PROS: 
# 1. should be much faster then single core when having >10 big files or hundreds/thousnds of files
# 2. is compatible with other tar tools (eg 7 zip) since it is not breaking apart files (like the march.py does)
# CONS:
# 1. not spliting files as march.py does so single files or small number of files will be slower
# 2. to untar using other tools more manual work (eg 6-10 times to untar all of it)
# INTERESTING:
# used in-memory tart.gz using byteIO, then final file write to .tar

# python -m nuitka --follow-imports tartar.py

from sys import argv as sys_argv #, getsizeof
from os.path import exists as os_path_exists, isdir as os_path_isdir, join as os_path_join,  getsize as os_path_getsize, sep as os_path_sep, abspath as os_path_abspath
from os.path import split as os_path_split
from time import perf_counter


from modules.helpers import  listRemoveEachVal
from modules.extras import strFormatNum, sortLoO, listPaths, readFile, getOpt,optiByteUnits,make_dir,paths_common_root

import modules.m_cpu as m_cpu

from io import BytesIO
from tarfile import open as tarfile_open, TarInfo
from  traceback import  format_exc as traceback_format_exc


global arch_ext




# return sorted files and common root for arch name 
def _initPaths(input_paths,extList  ):
	eelen=[len(ee) for ee in extList]
	extList=[ee.lower() for ee in extList]
	extListLen=len(extList)
	
	def _isext(ff,extList,extListLen,eelen):
		if extListLen>0:
			for ii,ee in enumerate(extList):
				if ee==ff[-eelen[ii]:]: 
					return True
			return False
		else: 
			return True
		
	
	files=[]
	paths=[] # for root finding 
	for ip in input_paths:
		if os_path_isdir(ip):
			
			files_list, roots=listPaths(ip,_sorted=False, _abs_path=True ) #getFilePaths(ip ) # get all files:
			
			for f in files_list: 
				if _isext(f[0].lower(),extList,extListLen,eelen):
					files.append(f) #[f , os_path_getsize(f)])
					paths.append(f[0])
					
		else:
			if _isext(ip.lower(),extList,extListLen,eelen):					
				files.append( ( os_path_abspath(ip)  , os_path_getsize(ip)) ) #[ip , os_path_getsize(ip)])
				paths.append(os_path_abspath(ip))
	
	files=sortLoO(files,1,reverse=True)	
	
	common_root=paths_common_root(paths) # must be found per whole package to keep consistency 
	
	return files, common_root 
	
def assignFilesPerCPU(input_paths,extList ):
	sorted_files, common_root = _initPaths(input_paths,extList )


	total_size=sum([p[1] for p in sorted_files ])
	
	n, opt=getOpt(total_size)
	
	cpu_load=m_cpu.CPU_Load( 30,n,_do_print=True)
	cpu_list=cpu_load.getCpuList(opt) 
	cpucc=len(cpu_list)
	if len(sorted_files)<cpucc: # if low number of files -limit cpu usage
		cpucc=len(sorted_files)
		cpu_list=cpu_list[:cpucc]
	print(f'Preparing {len(sorted_files)} files.')	
	print(f'Using {cpucc} CPUs')
	
	files_split={} # cpu:file list 
	work_per_cpu=list(0 for ii in cpu_list)
	for cpuii in cpu_list:
		files_split[cpuii]=[]
		 
	ii=0
	for f in sorted_files:
		files_split[cpu_list[ii]].append(f[0])
		work_per_cpu[ii]+=f[1] 
		ii+=1
		if ii>=cpucc:
			ii=0 
	return files_split, common_root, work_per_cpu





# tar in mem, return BytesIO()	
def writetarIO(paths,other_args):

	cut_root=other_args['cut_root']
	
	torename=paths.copy() 
	if cut_root!='':
		torename=[p.replace(cut_root,'') for p in paths] 
			
	ztype='w:gz'
	
	io_mem_tar = BytesIO()  
	try:
	
		with tarfile_open(fileobj=io_mem_tar , mode=ztype, bufsize=1024*1024) as tfile: #
			
			for ii,pp in enumerate(paths): #Adding files to the tar file
				f_tmp = open(pp, 'rb')
				f_tmp.seek(0,2) # go to the end
				source_len = f_tmp.tell()
				f_tmp.seek(0)
				
				info =  TarInfo(torename[ii])
				info.size = source_len
				tfile.addfile(info, f_tmp)
				f_tmp.close()
	except:
		return f"Error at writetar: {traceback_format_exc()}"	
				
	return  io_mem_tar




# final tar list of BytesIO()
def writeTarFromIO_list_dict(io_dict=[(0,'io_mem_tar'), ] , other_args={'archname':'name.tar'}):

	archname=other_args['archname']
	ztype='w'
	
	try: 
		with tarfile_open(archname , mode=ztype) as tfile: #, bufsize=1024*1024
			
			for ii,pp in enumerate(io_dict): 
				info =  TarInfo('p_'+str(pp[0])+'.tar.gz') # name tared bytesio				
				pp[1].seek(0,2) # go to the end
				info.size = pp[1].tell()
				
				pp[1].seek(0)
				tfile.addfile(info, pp[1]) #
				pp[1].close()
				
	except:
		return f"Error at writetar: {traceback_format_exc()}"	
		
	return archname



	
	
	
	



# files split by cpu ii
def tar(cpu_path_dict, final_name, common_root,work_per_cpu ):

	final_tar_file_list=[]
	
	wm=m_cpu.WorkManager(print_performance=True)
	
	# use tar in mem / IO
	results, workers_time=wm.startWorkers( list(cpu_path_dict.keys()), writetarIO  , work_to_split=cpu_path_dict, other_args={ 'cut_root':common_root } )
	
	for k,v in results.items():
		final_tar_file_list.append((k,v))
	
	retv=writeTarFromIO_list_dict( final_tar_file_list, other_args={'archname':final_name,  'cut_root':'' }  )
	
	
	s1=	sum(work_per_cpu) #total_size
	s2=os_path_getsize(retv)
	r=round(s1/s2,1)
	s1,arr,b_unit=optiByteUnits(s1,[s2])
	s2=arr[0]
	
	print(f"Created zip file: {retv}. Compressed {strFormatNum(round(s1,2))} {b_unit} to {strFormatNum(round(s2,2))} {b_unit}, ratio={r} ")
	
		
	return retv 
	
	


# extracting is superfast - not using cpus 
def untar(arcpath,todir='.'):
	topath=''
	if todir[0]=='.':
		head,tail=os_path_split(arcpath ) 
		todir=tail.split('.')
		todir=todir[0]
	# print('todir',todir)
	# exit(1)
		
	with open(arcpath, 'rb') as f:
		# print (f)
		with tarfile_open(fileobj=f, mode='r') as tar: # Unpack tar
			# print(tar.getnames())
			cc=len(tar.getnames())
			topath='.'
			if cc>1:
				arcpath_spl=arcpath.split('.')
				topath=arcpath_spl[0]
				
			if todir!='.':
				if not os_path_exists(todir):
					make_dir(todir)
				if todir!=topath:
					topath=os_path_join(todir,topath)
				
			for itar in tar: 
				with tar.extractfile(itar) as inner_tar:
					with tarfile_open(fileobj=inner_tar, mode='r:gz') as it:
						it.extractall(path=topath)
						
	print(f"Extracted archive at: {topath}"	)		
	return topath

	
	
def tartar(args ): # modified - accepting only 1 path / DIR 
	
	global arch_ext
	arch_ext='.tar'
	
	input_paths=[]
	extlist=[]
	
	# init: cover exceptions
	# if len(args)<1:
		# return ''
		
	# input COVER EXCEPTION no input
	if len(args)==0:
		print(f'\n***Need at least one more argument - at least 1 path to tar or untar! EXIT!')
		return
	else:
		input_paths=[args[0]]
		if len(args)>1:
			extlist=args[1:]
	
	# DECIDE TAR OR UNTAR 
	if len(input_paths[0])>len(arch_ext) and input_paths[0][-len(arch_ext):]==arch_ext:
		untar(input_paths[0])
	else:
		# IF IF MORE FILES TAR
		for a in input_paths:
			if not os_path_exists(a):
				print(f'\n*** Path {a} do not exist! EXIT!\n All paths must exist to tar!')
				return
		
		# ANALIZE PATHS AND DECIDE ARCH NAME 
		files_split, common_root, work_per_cpu=assignFilesPerCPU(input_paths,extlist )
		arch_name_tmp=common_root.split(os_path_sep)
		arch_name_tmp=listRemoveEachVal(arch_name_tmp,'') 
		if len(arch_name_tmp)>0:
			arch_name=arch_name_tmp[-1]+'.tar'
		else:
			arch_name='asdfzxcv.tar'
			
		# arch_name=input_paths[0]
		# print('arch_name',arch_name,'.' == arch_name[0],common_root,arch_name_tmp)
		# if '.' == arch_name[0]: # and len(input_paths)==1: # SINGLE FILE TAR
			# arch_name=arch_name+'.tar'
			# print('arch_name',arch_name,'.' == arch_name[0])
		# elif len(arch_name_tmp)>1:
			# arch_name=arch_name_tmp[-2]+'.tar' # MULTIPLE FILES OR DIRECTORIES
		
		# elif ':' in arch_name: # case windows C:\\
			# arch_name=arch_name.split(':')
			# arch_name=arch_name[0]+'_new_arch.tar'
			
		# COVER EXCEPTION ARCHIVE FILE EXISTS:
		if os_path_exists(arch_name):
			print(f"Error. Path: {arch_name} already exist... Delete it to create new tar."	)
			return
		
		# final tar 		
		rr=tar(files_split,arch_name, common_root,work_per_cpu) 
		
	
		
		
		
		
		

def main():
	try:	 
		tartar(sys_argv[1:]) # all arguments will be archived 
	except:
		print(traceback_format_exc() )
		exit()
	
	
	
	
if __name__ == '__main__':
	t1=perf_counter()
	main()
	print(f' Finished in {round(perf_counter()-t1,6)} sec.')
		