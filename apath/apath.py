# example usage of analize dir
# creates 4 summary files 


import os , sys
import modules.log
import modules.dirOp as dop
from modules.fileOp import writeTxtFile

global printlog

def path_to_name(pp):
	return pp.replace('\\','_').replace('/','_').replace(':','_')
	

def main(sys_argv):

	printlog=modules.log.Log()

	rdir='.'
	if len(sys_argv)<2:
		printlog.log('No root dir passed - taking current root as argument to anlize')
	else:				
		rdir=sys_argv[1]
		
	if os.path.exists(rdir):
		rdir=os.path.abspath(rdir)
	else:
		printlog.print('No such path:',rdir)
		printlog.print('Use \\\\ on Windows or / on Linux as path separator.')
		return
		
	printlog.log('\n*** Analizing path [',rdir,']\n' )
	
	ap=dop.AnalizePath(rdir,byteUnit=1 ) # 1024
	
	tt=path_to_name(rdir)
	
	extsum=ap.extSummary(measure='count') 
	writeTxtFile(f'a_analized_extensions_{tt}.txt',extsum)
	
	top7sum=ap.summary( topN=7)
	writeTxtFile(f'b_summary_top7_{tt}.txt',top7sum)
	
	totsum=ap.summary()
	writeTxtFile(f'c_summary_{tt}.txt',totsum)
	
	fileind=ap.fileIndexSummary('biggest')
	writeTxtFile(f'd_files_starting_biggest_{tt}.txt',fileind)
	
	fileind=ap.fileIndexSummary('newest')
	writeTxtFile(f'e_files_starting_newest_{tt}.txt',fileind)
	
	fileind=ap.fileIndexSummary('oldest')
	writeTxtFile(f'f_files_starting_oldest_{tt}.txt',fileind)
	

	printlog.log('\n*** done\n' )







if __name__ == "__main__":
	main(sys.argv)