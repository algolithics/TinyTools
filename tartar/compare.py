# compare files

from sys import argv

from os.path import exists as os_path_exists, isdir as os_path_isdir, abspath as os_path_abspath, join as os_path_join,  getsize as os_path_getsize
from os import walk as os_walk

def readFile(path,mode='rb'):
	rr=''
	with open(path,mode ) as f:
		rr= f.read()
	return rr
	
	

def listPaths(ddir ):
	
	paths={}
	
	for root, dirs, files in os_walk(ddir): 
		for file in files:
			
			tmp= os_path_join(root,file)
				
			paths[tmp.replace(ddir,'')]=os_path_join(root,file)
		
	return paths
	
	
	
def compareDirsTrees(d1,d2):
	p1=listPaths(d1 )
	p2=listPaths(d2 )
	diff=[]
	for p,v in p1.items():
		if p in p2:
			# f1=readFile(v)
			# f2=readFile(p2[p]) #print('comparing files',v,p2[p],'same' if f1==f2 else 'diff')
			# if f1!=f2:
			if areFilesDiff(v,p2[p]):
				diff.append(f"{p} diff between {d1} and {d2}")
		else:
			diff.append(f"{p} missing in {d2}")
		
	for p,v in p2.items():
		if p not in p1:
			diff.append(f"{p} missing in {d1}")
			
	return diff
	
	
def areFilesDiff(p1,p2):
	f1=readFile(p1)
	f2=readFile(p2)
	return f1!=f2
	
	
	
	
if __name__ == '__main__':

	if os_path_isdir(argv[1]) and os_path_isdir(argv[2]):
		diff=compareDirsTrees(argv[1],argv[2])
		if len(diff)==0:
			print(f'Trees {argv[1]} and {argv[2]} are exact copies.')
		else:
			print(f'Trees {argv[1]} and {argv[2]} are different.')
			for d in diff:
				print(d)
		# p1=listPaths(argv[1] )
		# p2=listPaths(argv[2] )
		# diff=[]
		# for p,v in p1.items():
			# if p in p2:
				# f1=readFile(v)
				# f2=readFile(p2[p])
				# print('comparing files',v,p2[p],'same' if f1==f2 else 'diff')
				# if f1!=f2:
					# diff.append(f"{p} diff between {argv[1]} and {argv[2]}")
			# else:
				# diff.append(f"{p} missing in {argv[2]}")
			
		# for p,v in p2.items():
			# if p not in p1:
				# diff.append(f"{p} missing in {argv[1]}")
				
		# print('\n***diff summary:\n')
		# if len(diff)==0: print('\tall same')
		# for d in diff:
			# print(d)
		
	else:
		if not areFilesDiff(argv[1],argv[2]):
			print(f'Files {argv[1]} and {argv[2]} are exact copies.')
		else:
			print(f'Files {argv[1]} and {argv[2]} are different.')
			
		# makre compare per dir walks and check 
		# f1=readFile(argv[1])
		# f2=readFile(argv[2])
		# print('comparing files',argv[1],argv[2],len(f1),len(f2),f1==f2)