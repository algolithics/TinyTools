# zpckl zipped pickle: pickle->gzip using multi cpu->final pickle
# and reverse - read file and unpickle + unzip 
# use mutiple cores if byte size > 1 MB


# EXAMPLE using this script: 
# - python zpckl.py qqq # will  create example zipped pickle file
# - python zpckl.py qqq.zpckl # will read data from zipped pickle file 

# AS MODULE:
# - dataToFile(ddata,fileName) - ddata - to pickle, fileName - path to save .zpckl file
# - fileToData(fileName) - will read .zpckl file and return the data 
