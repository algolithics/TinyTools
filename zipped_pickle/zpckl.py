#zpckl zipped pickle: pickle->gzip using mcpu->final pickle
# use mutiple cores if byte size > 1 MB


# EXAMPLE using this script: 
# - python zpckl.py qqq # will  create example zipped pickle file
# - python zpckl.py qqq.zpckl # will read data from zipped pickle file 

# AS MODULE:
# - dataToFile(ddata,fileName) - ddata - to pickle, fileName - path to save .zpckl file
# - fileToData(fileName) - will read .zpckl file and return the data 

from os.path import exists as os_path_exists,    getsize as os_path_getsize 
from gzip import compress as gzip_compress, decompress as gzip_decompress  #open as gzip_open
from  traceback import  format_exc as traceback_format_exc
from pickle import dumps as pickle_dumps, loads as pickle_loads, dump as pickle_dump, load as pickle_load


import modules.m_cpu as m_cpu
from modules.extras import strFormatNum,optiByteUnits ,getOpt

global arch_ext


	
# unzip using multiple cpus 
def unzip1file(infile):
	
	tounzip='' # unpickle
	with open(infile ,'rb' ) as f:
		tounzip=pickle_load(f)
		
	tounzip_list=[ [ci, tounzip[ci]]  for ci in tounzip['_ord'] ]
			
	# decide cpu count
	fsize=os_path_getsize(infile)
	n, opt=getOpt(fsize)
	# n, opt=1,1
	
	cpul=m_cpu.CPU_Load( 30,n,_do_print=False)
	cpu_list=cpul.getCpuList(opt)
					
	wm=m_cpu.WorkManager(print_performance=False)	
	
	unz_b=b''
	if len(tounzip_list)>0:	
			
		if -1 in tounzip['_ord']:
			unz_b=tounzip[-1]
		else:
			tosave=[]
			results, workers_time=wm.startWorkers( cpu_list,  unzipFile , work_to_split=tounzip_list )
			for ci in cpu_list: 
				if ci not in results:
					continue
				for k,v in results[ci].items(): 	
					tosave.append(v)
					
			unz_b=b''.join( tosave)
			
	return pickle_loads(unz_b)
	
	


def unzipFile(llist):
	res={}
	for ll in llist: 
		res[ll[0]]=gzip_decompress(ll[1]) 
	return res




def fileToData(fileName):
	
	if not os_path_exists(fileName):
		print(f'File does not exist: {fileName} - exit!')
		return {}
	
	try:	 
		return unzip1file(fileName)
	except:
		print(traceback_format_exc() )
		return {}


	
	
	
# split file to parts, zip, pickle
def zip1file(bytes_data,newfname):
	
	if os_path_exists(newfname):
		print(f'Cannot create {newfname} - file already exists!')
		return ''
		
	fsize=len(bytes_data)
	
	tosave={}
	tosave['_type']='file'
	if fsize<=64:
		tosave['_ord']=[-1] #exception
		tosave[-1]=bytes_data 
		
	else:
		n, opt=getOpt(fsize)
			
		cpul=m_cpu.CPU_Load( 30,n,_do_print=False)
		cpu_list=cpul.getCpuList(opt) 
		
		wm=m_cpu.WorkManager(print_performance=False)
		results, workers_time=wm.startWorkers( cpu_list, gzip_compress  , work_to_split=bytes_data)
		tosave['_ord']=[]
		if results!=None:
			for ci in cpu_list:
				if ci not in results:
					continue
				tosave[ci]=results[ci]
				tosave['_ord'].append(ci)
			
	with open(newfname ,'wb' ) as f:
		pickle_dump(tosave, f)
		
	s1=	fsize
	s2=os_path_getsize(newfname)
	r=round(s1/s2,1)
	s1,arr,b_unit=optiByteUnits(s1,[s2])
	s2=arr[0]
	
	print(f"Created file: [{newfname}] . Compressed {strFormatNum(round(s1,2))} {b_unit} to {strFormatNum(round(s2,2))} {b_unit}, ratio={r} ")
	return newfname
	
	
def dataToFile(ddata,fileName):
	global arch_ext
	arch_ext='.zpckl'
	
	if '.' not in fileName:
		fileName+=arch_ext # add ext
	
	try:	 
		
		bbytes=pickle_dumps(ddata)
		
		zip1file(bbytes,fileName)
	except:
		print(traceback_format_exc() )
		
	
	
# EXAMPLE: python zpckl.py qqq # will  create example zipped pickle file
# python zpckl.py qqq.zpckl # will read data from zipped pickle file 

if __name__ == '__main__':
	from sys import argv as sys_argv #, getsizeof
	from time import perf_counter, time as t_time
	from numpy import random as np_random, int32 as np_int32
	
	rng = np_random.default_rng(int(round(t_time() ,0)))
	ddict={'big_arr':rng.integers(0, high=15, size=(1000000),dtype=np_int32)
			, 'descr':'data example'}
			
			
	t1=perf_counter()
	if len(sys_argv)>1 and os_path_exists(sys_argv[1]): # if file exists - try unpickle
		dd=fileToData(sys_argv[1])
		print('data keys:',dd.keys())
		
	else:
		dataToFile(ddict,sys_argv[1])
		
	print(f' Finished in {round(perf_counter()-t1,6)} sec.')
		