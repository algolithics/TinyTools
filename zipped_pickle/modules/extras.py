
from os.path import exists as os_path_exists, isdir as os_path_isdir, abspath as os_path_abspath, join as os_path_join,  getsize as os_path_getsize,sep as os_path_sep
from os import walk as os_walk, mkdir as os_mkdir # os_mkdir('test\\c\\f')
	
# strFormatNum, sortLoO, listPaths, readFile, getOpt,optiByteUnits

def make_dir(dd):
	if not os_path_exists(dd):
		os_mkdir(dd)

# use s1 as scale value, arr as other values to scale
def optiByteUnits(s1,arr):
	unit=['B','kB','MB','GB']
	ui=0
	while s1>1024 and ui<3:
		s1=s1/1024
		arr=[a/1024 for a in arr]
		ui=ui+1
	return s1,arr,unit[ui]




def getOpt(fsize,_scale=1): # overwrite setup from extras 
	n=1
	opt='ab1m1'
	if fsize>1024*1024*16*_scale: n=9
	elif fsize>1024*1024*4*_scale: n=5
	elif fsize>1024*1024*1*_scale:  n=2  # quick probing
	else:  opt='best' # for small file single best core
	return n, opt



def readFile(path,mode='rb'):
	rr=''
	with open(path,mode ) as f:
		rr= f.read()
	return rr


# shortest common root for paths:
def paths_common_root(paths):
	# print(paths)
	tmp=paths[0].split(os_path_sep)
	longest_p_split=tmp[:-1]
	longest_p=os_path_sep.join(longest_p_split)
	best_len=len(longest_p)
	
	for p in paths[1:]:
		
		if longest_p==p[:best_len]: # same
			continue
			
		tmp=p.split(os_path_sep) # if not same - cut while 
		tmpsplit=[]
		ii=0
		while ii<len(longest_p_split) and longest_p_split[ii]==tmp[ii]:
			tmpsplit.append(longest_p_split[ii])
			ii+=1
			
		longest_p_split=tmpsplit
		longest_p= os_path_sep.join(longest_p_split)
		best_len=len(longest_p)	 # print('new best',longest_p_split,longest_p,best_len)
		
	return longest_p+os_path_sep
	
	
	

def listPaths(ddir,_sorted=True, _abs_path=False ):
	
	paths=[]	
	roots=[]
	for root, dirs, files in os_walk(ddir): 
		roots.append(root)
		for file in files:
			# tmp=os_path_abspath(os_path_join(root,file))
			tmp= os_path_join(root,file)
			if _abs_path:
				tmp=os_path_abspath(tmp)
			# paths.append( os_path_abspath(os_path_join(root,file))	)
			paths.append( (tmp,os_path_getsize(tmp)) )
	if _sorted:		
		paths=sortLoO(paths,1,False) # ASC order 
		
	return paths, roots
	
	
	
def sortLoO( loo,col_or_key,reverse=True):
	def sortBy(kv):
		return kv[col_or_key]
	return sorted(loo, key=sortBy, reverse=reverse) # bytes 		
			


def strFormatNum(n,sep="'"):
	s=str(n)
	spl=s.split('.')
	# format int part:
	ilen=len(spl[0])
	m=ilen%3
	res=''
	if m>0:
		res=spl[0][:m] 
		
	for ii in range(m,ilen,3):
		res+=sep+spl[0][ii:ii+3]
		
	if len(spl)>1:
		res+='.'+spl[1]
		
	if res[0]==sep:
		res=res[1:]
		
	return res	