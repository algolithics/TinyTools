
cfg={'delimiter':','
	, 'infer_datetime_format':True 
	, 'header':0 
	}
		
		
# all options with default values 
# csv_read_cfg={ 'date_columns_to_parse':[]
						# , 'dayfirst':False #DD/MM format dates, international and European format.
						# , 'infer_datetime_format':False
						# , 'delimiter':';'
						# , 'index_col':False #  index_col=False force exclude index 
						# , 'read_columns_list':None #usecols can be also numbers 
						# , 'header':None # header row number - if no col names None 
						# , 'dtype':{} # single value or E.g. {‘a’: np.float64, ‘b’: np.int32, ‘c’: ‘Int64’} 
						# , 'true_values':['True','true']
						# , 'false_values':['False','false']
						# , 'skiprows':[] # int or list 
						# , 'nrows': None # int 
						# , 'quotechar':None # to be able to use delimiter inside values 
						# , 'doublequote':False
						# , 'comment':None #will exclude the line or anything after in the line
						# , 'na_check':True   #na_filter - if sure no NA in the file may read faster using False
						# ,'encoding':'utf-8'
						# }
