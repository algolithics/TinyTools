# csv match
# python -m nuitka --follow-imports csv_match.py
# certUtil -hashfile csv_match.exe sha256

from sys import argv as sys_argv # commands: analize, join opt on k1 k2, unique - save unique value per file 
from modules import pandas_table
from config import cfg
from os.path import exists as os_path_exists



def save_df(csv_fname,df_save,cfg):
	if not df_save.empty:
		fname=csv_fname+'.csv'
		df_save.to_csv( fname, sep=cfg["delimiter"], encoding='utf-8', index=False, header=True)
		print('Saved',fname)
	else:
		print('empty df - not saved')
		
		
		

def analize(fpath):
	print('\n *** Analize',fpath)			
	t=pandas_table.Table()
	t.fromCsvFile( fpath, cur_cfg=cfg)
	t.prints()
	if t.isEmpty():
		print(' ??? Is this correct csv file?',fpath)



def inferKeys(args,cols1,cols2):
	if len(args)==0:
		print('Inferred columns used as join keys:',cols1[0],cols1[0])
		return cols1[0],cols2[0]
		
	elif len(args)==1:
		tmp=args[0].lower()
		print(tmp)
		if 'key=' in tmp or 'keys=' in tmp:
			s=tmp.split('=')
			tmp=s[-1].strip()
			if tmp in cols1 and tmp in cols2:
				return tmp,tmp
			else:
				print(tmp,'key does not exist in both files!')
				
				return None,None
	else:
		print(f'Analizing {args} to find left and right keys')
		left_key=None
		right_key=None
		for aa in args:
			if 'left_key=' in aa:
				left_key=aa.replace('left_key=','')
			elif 'right_key=' in aa:
				right_key=aa.replace('right_key=','')
		if not left_key in cols1:
			if left_key==None:
				print("Didn't found [left_key=] in {args}")
			else:
				print(left_key,'key does not exist in first file! These are available:',cols1)
			return None,None
		if not right_key in cols2:
			if right_key==None:
				print("Didn't found [right_key=] in {args}")
			else:
				print(right_key,'key does not exist in second file! These are available:',cols2)
			return None,None
		return left_key,right_key
	
	
	

def join_tables(fpath1,fpath2,*args):
	
	print('\n *** Join',fpath1,fpath2,args)			
	t1=pandas_table.Table()
	t1.fromCsvFile( fpath1, cur_cfg=cfg)
	if t1.isEmpty():
		print(' ??? Is this correct csv file?',fpath1)
		
	t2=pandas_table.Table()
	t2.fromCsvFile( fpath2, cur_cfg=cfg)
	if t2.isEmpty():
		print(' ??? Is this correct csv file?',fpath2)
	
	k1,k2=inferKeys(args,t1.df.columns,t2.df.columns)
	if k1!=None:
		t1.join(t2,join_type='outer', on_columns={k1:k2}, _suffixes=('', '_joined'), _sql_nulls=True )
		
		save2='joined_'+t1._r.getName( 8, 8 )
		while os_path_exists(save2):
			save2='joined_'+t1._r.getName( 8, 8 )
			
		save_df(save2,t1.df,cfg)
	
	
# find diff values or records not existing in other files 
def find_diff(fpath1,fpath2,*args):	
	print('\n *** Find distinct',fpath1,fpath2,args)			
	t1=pandas_table.Table()
	t1.fromCsvFile( fpath1, cur_cfg=cfg)
	if t1.isEmpty():
		print(' ??? Is this correct csv file?',fpath1)
	# add rand col with values 	- new 1 100% full column to distinguish not matched !
	new_col1=t1._r.getName( 8, 8 )
	t1.addColumns( cols={new_col1:'missing right'})
		
	t2=pandas_table.Table()
	t2.fromCsvFile( fpath2, cur_cfg=cfg)
	if t2.isEmpty():
		print(' ??? Is this correct csv file?',fpath2)
	# add rand col with values 	- new 1 100% full column to distinguish not matched !
	new_col2=t2._r.getName( 8, 8 )
	t2.addColumns( cols={new_col2:'missing left'})
	
	value_key=None
	args=list(args)
	for aa in args:
		if 'value=' in aa:
			value_key=aa.replace('value=','') 
			args.remove(aa)
			break
			
	k1,k2=inferKeys(args,t1.df.columns,t2.df.columns)
	if k1!=None:
		t1.join(t2,join_type='outer', on_columns={k1:k2}, _suffixes=(None, '_joined'), _sql_nulls=True )
		if new_col2+'_joined' in t1.df.columns: new_col2+='_joined'
		t3=t1.filteredNA( columns=[new_col1, new_col2 ]) # not existing/matching records
		
		# now find diff values:
		
		if value_key!=None:
			if value_key not in t1.df.columns:
				print(f'Bad value column name to compare: {value_key}, existing available: {t1.df.columns}.')
				return
			t4=t1.filtered( cond=[{'column':value_key,'operator':'!=','value':f'df_column_{value_key}_joined'} ])
			t4.df[new_col1]='diff val'
			t4.df[new_col2]='diff val'
			t3.append( t4 )
			t3.removeDupli(_subset=[k1, k2])
		
		t3.renameColumns( ddict={new_col1:'missing_right', new_col2:'missing_left'}, _print=False )
		save2='diff_'+t3._r.getName( 8, 8 )
		while os_path_exists(save2):
			save2='diff_'+t3._r.getName( 8, 8 )
			
		save_df(save2,t3.df,cfg)
	
	
	

if __name__ == '__main__':
	if len(sys_argv)==1:
		print('\n *** Usage:\n\t 1. python csv_match.py analize file.csv ')
		print('\t 2. python csv_match.py diff file1.csv file2.csv # find records not existing in the other file ')
		print('\t 3. python csv_match.py join file1.csv file2.csv left_key=id right_key=id2 #create files with joined values based on provided keys')
		print('\t 4. python csv_match.py join file1.csv file2.csv key=id #create file with joined value, same key in both table')
		print('\t 5. python csv_match.py diff file1.csv file2.csv key=id value=id # find records with different values in column=value or not existing in the other file ')
	else:
		commands=('analize','join','diff')
		if sys_argv[1] not in commands:
			print(f'Bad command {sys_argv[1]} only one of {commands} allowed')
		elif len(sys_argv)<3:
			print(f'Not enough arguments? File name is needed after command {sys_argv[1]}!')
		else:
			if not os_path_exists(sys_argv[2]):
				print('*** ERROR: no such file!',sys_argv[2])
				
			elif sys_argv[1]==commands[0]:
				analize(sys_argv[2])
			elif sys_argv[1]==commands[1]:
				join_tables(*sys_argv[2:])
			else:
				find_diff(*sys_argv[2:])
	