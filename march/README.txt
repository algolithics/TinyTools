This module can gzip file or directory using multiple CPUs.
Created archive can be unzipped using only this app (not compatible with 7zip etc...)
BEWARE: do your own testing if you plan to use it.
I've tested it for tousands of random runs, but it was not extensively used so may be there is some edge case bug hidden.
That's why I also attach the testing tool: compare.py

It works by:
1. splitting files/bytes into groups per CPU
	- if needed split also file bytes between CPUs 
2. gzip each file/bytes
3. pickle gzipped files

Will use multiple CPUs only for big files bytes > 1024*1024*16
Or directories of total file size above 16 MB
This can be changed in modules.extras.py

Example:
# zip
python march.py README.txt # creates README.txt.march
python march.py modules # creates modules.march
# unzip:
python march.py modules.march # unzips to modules / error if files already exist
python march.py modules.march some\folders # unzips modules to path some\folders
python march.py README.txt.march some\folders # unzips to path some\folders
python march.py README.txt.march unzip.txt # unzips content of file to unzip.txt