

from sys import argv as sys_argv #, getsizeof
from os.path import exists as os_path_exists, isdir as os_path_isdir, abspath as os_path_abspath, join as os_path_join,  getsize as os_path_getsize, sep as os_path_sep
from os import walk as os_walk, mkdir as os_mkdir # os_mkdir('test\\c\\f')
from gzip import compress as gzip_compress, decompress as gzip_decompress  #open as gzip_open
from  traceback import  format_exc as traceback_format_exc
from pickle import dump as pickle_dump, load as pickle_load
from time import perf_counter

import modules.m_cpu as m_cpu
from modules.extras import strFormatNum, sortLoO, listPaths, readFile, getOpt,optiByteUnits,make_dir

global arch_ext


def splitFiles2( list_file_size, mcpu): # sinpler algo ?

	list_len=len(list_file_size)
	if list_len<mcpu:
		print(f'Selected CPU count {mcpu} > work object count {list_len} - will use only {list_len} cores.') 
		mcpu=list_len 
		
	_ffact=(1+0.25/mcpu)
	total_bytes=sum((lfs[1]  for lfs in list_file_size ))
		
	bytes_per_cpu=int( round(total_bytes/mcpu,0) )+1
	bytes_per_cpu_ffact=int( round( bytes_per_cpu*_ffact,0) )+1
	work_sum_per_cpu=[0]
	work_split={0:[]}
	next_file_ii=0
	cpuii=0
	
	def _incr_cpu(cpuii): # each step iterate file and cpu to assgined: for sorted more or less equal distrib 
		cpuii=cpuii+1
		if cpuii>=mcpu:
			cpuii=0	
		if cpuii not in work_split:
			work_sum_per_cpu.append(0) # INIT
			work_split[cpuii]=[] # INIT		
		return cpuii
		
				
	while next_file_ii<list_len:
	
		if work_sum_per_cpu[cpuii]+list_file_size[next_file_ii][1] < bytes_per_cpu_ffact:
		
			work_sum_per_cpu[cpuii]+=list_file_size[next_file_ii][1]
			work_split[cpuii].append( {'start':'start','end':'end','file':list_file_size[next_file_ii][0],'size':list_file_size[next_file_ii][1]} )
			
		else: # SPLIT FILE
			to_fill=bytes_per_cpu-work_sum_per_cpu[cpuii] # MAY BE NEGATIVE THEN SWITCH TO NEXT CPU
			while to_fill<=0:
				cpuii=_incr_cpu(cpuii)	
				to_fill=bytes_per_cpu-work_sum_per_cpu[cpuii]
				
			to_fill=bytes_per_cpu_ffact-work_sum_per_cpu[cpuii]
				
			b_tosplit=list_file_size[next_file_ii][1] #split this file
			start=0
			while start<b_tosplit: 
				eend=start+to_fill
				if eend>=b_tosplit:
					eend='end' 
					to_fill=b_tosplit-start
					if start==0:
						start='start'
					
				work_split[cpuii].append( {'start':start,'end':eend,'file':list_file_size[next_file_ii][0],'size':to_fill} )
				work_sum_per_cpu[cpuii]+=to_fill
				
				if eend=='end':
					break
					
				cpuii=_incr_cpu(cpuii)					
				start=eend 	
				to_fill=bytes_per_cpu_ffact-work_sum_per_cpu[cpuii]
				
		if work_sum_per_cpu[cpuii]>bytes_per_cpu_ffact: # obly increment if work_sum_per_cpu[cpuii] signif bigger then avg bytes 	
			cpuii=_incr_cpu(cpuii)						
						
		next_file_ii+=1	
		
	return work_split, work_sum_per_cpu
	

# split files equally aong cores
# last files are partitioned 

# to zip multi files
# need zip file name - upper dir ?
# 

def zipDir(ddir):

	zipfilename=ddir+arch_ext
	if os_path_exists(zipfilename):
		print(f'\nCannot create archive of directory [{ddir}], file [{zipfilename}] already exist!')
		return ''
	
	list_file_size, roots=listPaths(ddir ) # recog files by size and sort 
	# list_file_size=sortLoO(list_file_size,1,False) # ASC order 
	total_size=sum([p[1] for p in list_file_size ])
	
	n, opt=getOpt(total_size)
	cpul=m_cpu.CPU_Load( 30,n,_do_print=True)
	cpu_list=cpul.getCpuList(opt) 
	
	wm=m_cpu.WorkManager(print_performance=True)
	
	results, workers_time=None,None
	
	tosave={}
	tosave['_type']='dir'
	tosave['_ord']=[]
	if len(list_file_size)==0:
		pass # no files only ir struct
	else:
		tosave['_type']='dir'
		to_split_work,work_sum_per_cpu=splitFiles2( list_file_size, len(cpu_list)) # WORK SPLIT
		
		work={k:[] for k in to_split_work}
		binc_files={}
		for k,arr in to_split_work.items():
			for v in arr:
				if v['start']=='start': # work = file 
					work[k].append( {'file':v['file'], 'start':'all'})
				else: #read 
					if v['file'] not in binc_files:
						binc_files[v['file']]=readFile(v['file'],mode='rb')
						
					start=v['start']
					end=v['end']
					if end=='end':
						end=len(binc_files[v['file']])
						
					work[k].append( {'file':v['file'], 'bin':binc_files[v['file']][start:end], 'start':start})
		
		results, workers_time=wm.startWorkers( cpu_list, gzipListOfFiles  , work_to_split=work )
		
	if results!=None:
		tosave['_ord']=[]
		for ci in cpu_list:
			if ci not in results:
				continue
			tosave[ci]=results[ci]
			tosave['_ord'].append(ci) # tosave['_ord']=cpu_list
	else:
		print(f'***Will only store directory tree - no files in the dir {ddir}')
	tosave['roots']=roots
			
	
	with open(zipfilename ,'wb' ) as f:
		pickle_dump(tosave, f)
		
	s1=	total_size
	s2=os_path_getsize(zipfilename)
	r=round(s1/s2,1)
	s1,arr,b_unit=optiByteUnits(s1,[s2])
	s2=arr[0]
	
	print(f"Created zip file: {zipfilename}. Compressed {strFormatNum(round(s1,2))} {b_unit} to {strFormatNum(round(s2,2))} {b_unit}, ratio={r} ")
	return zipfilename
	
	
	
	
	
def gzipListOfFiles(llist):
	res={}
	for ll in llist:
		todo=''
		start=ll['start']
		if 'bin' in ll: todo=ll['bin'] # case part file 
		else: todo=readFile(ll['file'],mode='rb') # case full file 
		
		if start=='all' and len(todo)<=64:
			res[ll['file']]={'bin':todo, 'start':start, 'nozip':True} 
		else: 
			res[ll['file']]={'bin':gzip_compress(todo), 'start':start}
		
	return res
	
	
	
	
	
	
# unzip using multiple cpus 
def unzip(infile, unzip_to):
	newfname=infile.replace(arch_ext,'')
	print('unzip_to',unzip_to)
	if unzip_to=='' and os_path_exists(newfname):
		print('Cannot unzip - file already exists',newfname)
		return ''
	
	tounzip='' # unpickle
	with open(infile ,'rb' ) as f:
		tounzip=pickle_load(f)
		
	tounzip_list=[ [ci, tounzip[ci]]  for ci in tounzip['_ord'] ]
			
	# decide cpu count
	fsize=os_path_getsize(infile)
	n, opt=getOpt(fsize)
	# n, opt=1,1
	
	cpul=m_cpu.CPU_Load( 30,n,_do_print=False)
	cpu_list=cpul.getCpuList(opt)
	
	main_root=''
	if 'roots' in tounzip:
		if unzip_to!='':
			main_root=tounzip['roots'][0].replace('\\','').replace('/','').strip()
			if tounzip['_type']=='dir':
				newfname= unzip_to
				
		for r in tounzip['roots']:
			if main_root!='': 
				tmp=r.replace(main_root,unzip_to)
				if os_path_exists(tmp):
					continue
					# print('Cannot unzip - file already exists',tmp)
					# return ''				
				os_mkdir(tmp)
			else: 
				os_mkdir(r)
				
	wm=m_cpu.WorkManager(print_performance=False)	
	
	tosave=[]
	if len(tounzip_list)>0:		
		if tounzip['_type']=='dir' : 
			results, workers_time=wm.startWorkers( cpu_list,  unzipListOfFiles , work_to_split=tounzip_list )
			tosort={} # for partitioned files 
			for ci in cpu_list: 
				if ci not in results:
					continue
					
				for k1,v1 in results[ci].items(): # per orig cpu 
					for k,v in v1.items() :  
						if v['start']=='all': 
							tosave.append({k:v['bin']})
						else:
							if k not in tosort:
								tosort[k]=[]
							tosort[k].append(v)
							
			for k,v in tosort.items():
				ss=sortLoO( v,col_or_key='start',reverse=False)	 
				
				tosave.append({k: b''.join([s['bin'] for s in ss]) }) 
				
			for aa in tosave:
				for k,v in aa.items():
					if main_root!='': k=k.replace(main_root,unzip_to)
					with open(k ,'wb' ) as f:
						f.write(v)
			
		else:
		
			unz_b=b''
			if -1 in tounzip['_ord']:
				unz_b=tounzip[-1]
			else:
				results, workers_time=wm.startWorkers( cpu_list,  unzipFile , work_to_split=tounzip_list )
				for ci in cpu_list: 
					if ci not in results:
						continue
					for k,v in results[ci].items(): 	
						tosave.append(v)
						
				unz_b=b''.join( tosave)
			
			if unzip_to!='': 
				todo_dir=[]
				name_with_ext=''
				if os_path_sep in unzip_to:
					unzip_to_split=unzip_to.split(os_path_sep)
					
					if len(unzip_to_split)>1:
						tmp=''
						todo_dir.append( unzip_to_split[0] )
						make_dir(todo_dir[-1])
						tobuild=unzip_to_split[1:]
						
						
						if '.' in unzip_to_split[-1]:
							tobuild=unzip_to_split[1:-1]
							name_with_ext=unzip_to_split[-1]	
							
						for u in tobuild:
							if u!='':
								todo_dir.append(os_path_join(todo_dir[-1],u) )
								make_dir(todo_dir[-1])
								
						if name_with_ext!='':
							newfname=os_path_join(todo_dir[-1],name_with_ext) 
						else:
							tmp=newfname.split(os_path_sep)
							newfname=os_path_join(todo_dir[-1],tmp[-1]) 
				else:
					
					if '.' not in unzip_to and '.' in newfname:
						fname_split=newfname.split('.')
						newfname=unzip_to+'.'+fname_split[-1]
					else:
						newfname=unzip_to
						
			if os_path_exists(newfname):
				print('Cannot unzip - file already exists',newfname)
				return ''
		
			with open(newfname ,'wb' ) as f:
				f.write(unz_b)
		
	print(f"Unzipped {tounzip['_type']}: {newfname}")
	return newfname




def unzipListOfFiles(llist):
	res={} #ll[0]:{} for ll in llist}
	for ll in llist: # ll[0] cpuii still important otherwise may overwrite !!!
		tmp={}
		for k,v in ll[1].items(): 
			if 'nozip' in v:
				tmp[k]={'bin': v['bin'], 'start':v['start']} # 
			else:
				tmp[k]={'bin':gzip_decompress(v['bin']), 'start':v['start']}  
		res[ll[0]]=tmp
	return res
	
	
	

def unzipFile(llist):
	res={}
	for ll in llist: 
		res[ll[0]]=gzip_decompress(ll[1]) 
		
	return res




# split file to parts, zip, pickle
def zip1file(infile):
	
	newfname=infile+arch_ext
	if os_path_exists(newfname):
		print('Cannot zip - file already exists:',newfname)
		return ''
		
	binc=readFile(infile,mode='rb')
	fsize=len(binc)
	
	tosave={}
	tosave['_type']='file'
	if fsize<=64:
		tosave['_ord']=[-1] #exception
		tosave[-1]=binc 
		
	else:
		n, opt=getOpt(fsize)
			
		cpul=m_cpu.CPU_Load( 30,n,_do_print=False)
		cpu_list=cpul.getCpuList(opt) 
		
		wm=m_cpu.WorkManager(print_performance=False)
		results, workers_time=wm.startWorkers( cpu_list, gzip_compress  , work_to_split=binc) #, other_args=9
		tosave['_ord']=[]
		if results!=None:
			for ci in cpu_list:
				if ci not in results:
					continue
				tosave[ci]=results[ci]
				tosave['_ord'].append(ci)
		# tosave['_ord']=cpu_list
			
	with open(newfname ,'wb' ) as f:
		pickle_dump(tosave, f)
		
	s1=	fsize
	s2=os_path_getsize(newfname)
	r=round(s1/s2,1)
	s1,arr,b_unit=optiByteUnits(s1,[s2])
	s2=arr[0]
	
	print(f"Created zip file: [{newfname}] . Compressed {strFormatNum(round(s1,2))} {b_unit} to {strFormatNum(round(s2,2))} {b_unit}, ratio={r} ")
	return newfname





def march(aa ):
	global arch_ext
	arch_ext='.march'
	# init: cover exceptions
	if len(aa)<1:
		return ''
	_path=aa[0]
	if not os_path_exists(_path):
		print(f'\nNo such path [{_path}], exit!')
		return ''
		
	# Case dir - zip files inside dir 
	if os_path_isdir(_path):
		return zipDir(_path)
		
	else: #case file - zip file or unzip archive
		
		if _path[-len(arch_ext):]==arch_ext: # case unzip			
			_unzip_to=''
			if len(aa)>1:
				_unzip_to=aa[1]
				
			return unzip(_path,_unzip_to) # common unzip both for dir archive and single file
			
			
		# case single file zip
		return zip1file(_path)
	
	
	
	
def main():
	try:	 
		march(sys_argv[1:])
	except:
		print(traceback_format_exc() )
		
	
	
	
if __name__ == '__main__':
	t1=perf_counter()
	main()
	print(f' Finished in {round(perf_counter()-t1,6)} sec.')
		