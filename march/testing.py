# testing zip/unzip for files and folders - 
# *** see if __name__ == '__main__':

from random import seed as r_seed, sample as r_sample, choices as r_choices, randint as r_randint, randbytes
from string import ascii_letters as s_ascii_letters, digits as s_digits
from shutil import rmtree

from os.path import exists as os_path_exists,   join as os_path_join,    sep as os_path_sep
from os import  mkdir as os_mkdir , remove as os_remove 

import march
import compare

class RandName:

	def __init__(self): #,minL=1,maxL=9):
		r_seed()
		self._chars=list(s_ascii_letters +  s_digits)  
		
	def getInt(self,mmin=0,mmax=9):
		return r_randint(mmin,mmax)
		
	def getDirName(self,maxNameLen=12 ):
		return ''.join(r_choices(self._chars, k=r_randint(1,maxNameLen) ) ) 
		
	def getFileName(self,maxNameLen=12,maxExtLen=3):
		return ''.join(r_choices(self._chars, k=r_randint(1,maxNameLen) ) )+'.'+''.join(r_choices(self._chars, k=r_randint(1,maxExtLen) ) )
		
	def randomDirTree(self,maxDepth=5,maxFiles=9,maxDirs=9,_init=True):
		tree={'files':[], 'dirs':[]}
		if _init:
			tree['dirs'].append( { self.getDirName():self.randomDirTree( maxDepth,maxFiles ,maxDirs,_init=False ) } )
		else:
			for ii in range(self.getInt(0,maxFiles)):
				tree['files'].append( self.getFileName() )
				
			if maxDepth>0:
				for ii in range(self.getInt(0,maxDirs)):
					tree['dirs'].append( { self.getDirName():self.randomDirTree( maxDepth-1,maxFiles ,maxDirs,_init=False ) } )
			
		return tree
		
		
	def createDir(self,ppath):
		if not os_path_exists(ppath):
			os_mkdir(ppath)
		
	
	def createFile(self,ppath,maxSize=1024*1024*16):
		if not os_path_exists(ppath):
			with open(ppath ,'wb' ) as f:
				f.write(randbytes(self.getInt(0,maxSize)))
		
		
		
# create random tree structure with files 

def makeTree(rn,tt,maxFileSize=1024,inside=''):

	for d in tt['dirs']:
		for k,v in d.items():
			# print(k,v,inside)
			tmp=os_path_join(inside, k)
			rn.createDir( tmp )
			makeTree(rn,v,maxFileSize, tmp)

	for f in tt['files']:
		rn.createFile(os_path_join(inside, f), maxFileSize)
		
# rmtree
def remove_tree(pp):
	rmtree(pp)

def printTree(tt,_=''):
	for f in tt['files']:
		print(f"{_} file: {f}")
	for d in tt['dirs']:
		for k,v in d.items():
			print(f"{_} dir: {k}")
			printTree(v,_=_+'\t')
		
		
		
def main(maxFileSize,numOfTests):
	rn=RandName()
	
	for test_ii in range(numOfTests):
		print('\n\n test',test_ii,'of',numOfTests)
		tt=rn.randomDirTree(3,4,4)   #printTree(tt ) 
		mainDir= tuple(tt['dirs'][0].keys())[0]
		if os_path_exists(mainDir):
			continue # if by accident dir exist
		
		makeTree(rn,tt,maxFileSize)
		# print(mainDir)
		 
		march_file=march.march( (mainDir,) ) # zip dir 
		# print('created zip,)
		if march_file=='':
			print('some error')
			exit()
			
		unzdir=march.march( (march_file, 'test_unzip') ) # unzip dir 
		
		# test 
		print('*** comparing',mainDir,unzdir)
		diff=compare.compareDirsTrees(mainDir,unzdir)
		if len(diff)==0:
			print(f'=== Trees {mainDir} and {unzdir} are exact copies.')
		else:
			print(f'Trees {mainDir} and {unzdir} are different.')
			for d in diff:
				print(d)
			exit()
				
		# print('deleting',mainDir,unzdir,march_file)
		remove_tree(mainDir)# remove dir
		remove_tree(unzdir)
		
		os_remove(march_file)# remove file




def main2(maxFileSize,numOfTests): # 1 file 
	rn=RandName()
	
	for test_ii in range(numOfTests):
		print('\n\n test',test_ii,'of',numOfTests)
		newfile=rn.getFileName()
		# print(newfile)
		
		if os_path_exists(newfile):
			continue # if by accident dir exist
		rn.createFile(newfile,maxSize=maxFileSize)
		
		march_file=march.march( (newfile,) ) 
		
		if march_file=='':
			print('some error')
			exit()
			
		unzfile=march.march( (march_file,'unziped.file') ) # unzip 
		
		# test 
		print('*** comparing',newfile,unzfile)
		# diff=compare.compareDirsTrees(mainDir,unzdir)
		if compare.areFilesDiff(newfile,unzfile):
			print(f'Files {newfile} and {unzfile} are different.')
			exit()			
		else:
			print(f'===Files {newfile} and {unzfile} are exact copies.')
				
		# print('deleting',newfile,unzfile)
		
		os_remove(newfile)# remove file
		os_remove(unzfile)# remove file
		os_remove(march_file)# remove file
		
		

if __name__ == '__main__':
	# FOLDERS POTENTIAL EDGE CASES
	main(maxFileSize=0, numOfTests=1)
	main(maxFileSize=1, numOfTests=8)
	# main(maxFileSize=8, numOfTests=32)
	# main(maxFileSize=32, numOfTests=128)
	
	# COMMON CASES
	# main(maxFileSize=1024, numOfTests=64)
	# main(maxFileSize=1024*1024, numOfTests=32)
	# main(maxFileSize=1024*1024*16, numOfTests=32)
	
	
	# FILES POTENTIAL EDGE CASES
	# main2(maxFileSize=0, numOfTests=1)
	# main2(maxFileSize=1, numOfTests=1)
	# main2(maxFileSize=8, numOfTests=8)
	# main2(maxFileSize=64, numOfTests=64)
	
	# COMMON CASES
	# main2(maxFileSize=128, numOfTests=128)
	# main2(maxFileSize=1024, numOfTests=64)
	# main2(maxFileSize=1024*1024, numOfTests=32)
	# main2(maxFileSize=1024*1024*16, numOfTests=32)
	
		