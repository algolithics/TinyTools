# TABS - ONLY UI DEFINITION EXAMPLE
# NO ACTIONS FOR CODE CLARITY

import tk_base.tk_gui as tkg  

		

if __name__ == '__main__': 

	tkg=tkg.TK_GUI('Title 1',['Tab 1','Tab 2'], ['rec1', 'rec2'])
	
	
	###################### TAB1
	
	grid_lol=[]
	
	# ROW 1
	tmpdict={'row1': [ {'T':'Label', 'X':'Try the button'}
					, {'T':'Button', 'X':'Click 1' , 'uid':'button1',   'tooltip': 'Will also create a task for queue to process using process button'} 
					, {'T':'Label', 'X':'default txt' , 'visible':True, 'uid':'label variable1' }
					]
			}
	grid_lol.append(tmpdict )	 
	# ROW 2
	tmpdict={'row2':[ {'T':'Label', 'X':'Try the button 2'}
					, {'T':'Button', 'X':'Click 2' , 'uid':'button2',   'tooltip': 'Will do some other action'} 
					, {'T':'InputInt',   'visible':True, 'uid':'input1' }
					] }
	grid_lol.append(tmpdict )					
	# ROW 3			
	tmpdict={'row3': [{'T':'Label', 'X':'Rounding'}
					, {'T':'Combox', 'X':'Click' , 'uid':'combobox1',   'tooltip': 'Select rounding', 'V':[0,1,2,3,4]} 
					, {'T':'Label', 'X':3.141592535 , 'visible':True, 'uid':'label3' }
					] }			
	grid_lol.append(tmpdict)
	# ROW 4			
	tmpdict={'row4': [{'T':'Label', 'X':'Receiver 2'}
					, {'T':'Button', 'X':'Process task' , 'uid':'button4',   'tooltip': 'Will process task for rec2'} 
					, {'T':'Label', 'X':'' , 'visible':True, 'uid':'label4' }
					] }
			
	grid_lol.append(tmpdict)
	tkg.addTabFrame(tabId=0,gridListOfLists=grid_lol,frameName='Frame Name 1' )
	
	
	###################### TAB2
	
	grid_lol2=[ 
					{'row1': [ {'T':'Text', 'width':32 } ] } 
					, {'row2': [ {'T':'Label', 'X':'...' , 'visible':True, 'uid':'label_time' } ] } 
				]
				
	tkg.addTabFrame(tabId=1,gridListOfLists=grid_lol2,frameName='Frame Name 2' )
	
	
	##################### DISPLAY
	
	tkg.root.mainloop()
	