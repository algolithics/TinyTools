# THIS EXAMPLE SHOWS:
# 1. HOW EASY IT IS TO CREATE A FRAME WITH WIDGETS USING TK_GUI
# It is more intuitive/html-like description of frame using grid layout 
# Each frame_grid list element is dict with key = row id
# Each row consists of multiple columns with defined elements eg. Label, Input, Button ... 
# Gui elements are defined using short keys like T:Type, X:Value/Text , uid: unique id to access element, span: extends cell for multiple columns 
	

import tk_base.tk_gui as tkg

if __name__ == '__main__': 

	# 1. INITIALIZE
	gui=tkg.TK_GUI('Title 1')
	
	
	
	# 2. DEFINE GUI ELEMENTS 
	frame_grid=[ 
					{'row1': [ {'T':'Label' # WIDGET TYPE
								,'X':'Some constant text. For this uid is needed only if you need styling.' # WIDGET LABEL/TEXT
								, 'span':2 # WIDGET will take 2 columns 
								, 'tooltip':'This is consatant label - does not have uid'   # information displayed when hovering mouse over this wdiget
								, 'style':{'bgc':'yellow','fgc':'red','fontsize':14} # optional styling  ,
								, 'uid':'222' # style only works for uid and for buttons and labels 
								, 'pads':[5,5]
								, 'width':36
								}  ] } # this row has only 1 column spanning into 2 columns as defined in 'span':2 
								
					, {'row2': [ {'T':'Input', 'X':'Hello'  , 'uid':'entry1', 'tooltip':'Inputs need uid to be able to identify and add functionality', 'width':32  }
									, {'T':'Input', 'X':'World'  , 'uid':'entry2', 'width':32 } ] } 
					, {'row3': [ {'T':'Label', 'X':'This is a variable label - has uid and can be updated.\nInputs, buttons and variable labels need uids.'  , 'uid':'label1','span':2 } ] } 
					, {'row4': [ {'T':'Button', 'X':'Change'  , 'uid':'button1','span':2 , 'tooltip':"This button functionality connects to data source (entries) using relevnt uids."} ] } # spann allows to take 2 columns, expand the element 
				]
			
	gui.addFrame( gridListOfLists=frame_grid,frameId='1' ) # APPLY gui elements to GUI	
	
	
	
	# 3. ADD BUTTON ACTION	
	# 3. a) define action function: take value from 1 element and assign to another 
	# arguments will be :  gui.frames['1'], 'entry1', 'entry2','label1'
	# frame is the object containing elements accessed by uids defined above : 'entry1', 'entry2','label1'
	def action1(frame , elem1, elem2, labelOutput):  
		tmp=frame.get_value(elem1)  + frame.get_value(elem2) 
		frame.set_textvariable( uid=labelOutput, x_val=tmp)  # often update_frame is better instead of set_textvariable - see other examples 
		
	# 3. b) ASSIGN TO ELEMENT: FRAME->BUTTON 
	gui.frames['1'].setButtonAction( 'button1', [ gui.frames['1'], 'entry1', 'entry2','label1'  ], action1 ) # action fun default arg is uid of element of action 
	
	
	
	# 4. DISPLAY
	gui.root.mainloop()
	