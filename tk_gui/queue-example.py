# THIS EXAMPLE SHOWS:
# 1. HOW TO USE QUEUES - SIMPLE EXAMPLE WHERE TASKS ARE CREATED BY 1 BUTTON AND PROCESSED BY ANOTHER BUTTON
# 2. PROPER FRAME UPDATING - WHEN LABEL CHANGES THE FRAME NEEDS TO BE UPDATED SO THAT THE WIDTH OF THE FRAME IS ADJUSTED
#    OTHERWISE PART OF THE TEXT MAY BE CUT


import tk_base.tk_gui as tkg
from tk_base.tk_dialogs import  get_path_dialog, showinfo

if __name__ == '__main__': 

	# 1. INITIALIZE
	gui=tkg.TK_GUI('Task queue',data_receivers=['button_2'])
	
	
	
	# 2. DEFINE GUI ELEMENTS 
	frame_grid=[ 
					{'row1': [ {'T':'Button', 'X':'Create new task'  , 'uid':'button1'  } ] }
					, {'row2': [ {'T':'Button', 'X':'Process next task'  , 'uid':'button2'  } ] }
					, {'row3': [ {'T':'Label', 'X':'Tasks awaiting: 0'  , 'uid':'label1' } ] }   
					, {'row4': [ {'T':'Label', 'X':''  , 'uid':'label2' } ] }   
				]
			
	gui.addFrame( gridListOfLists=frame_grid,frameId='1' ) # APPLY gui elements to GUI	
	
	
	
	# 3. ADD BUTTON ACTIONs	
	# 3. a) task creation and update frame with awaiting task count 
	tmpval=0
	def action1( *args): # selected path will be accessed via pvar 
		global tmpval
		gui.addTask(receiver='button_2',task=tmpval)
		tmpval+=1
		_x=gui.countTasks('button_2')
		gui.frames['1'].set_textvariable( 'label1', f"Tasks awaiting: {_x}")
		# alternative way/old
		# frame_grid[2]['row3'][0]['X']=f"Tasks awaiting: {_x}"
		# gui.frames['1'].update_frame( frame_grid,  only_update=True) # adjust widths - on global level checking all rows in columns 
	
	gui.frames['1'].setButtonAction( 'button1', [ ], action1 ) 
	
		
	# 3. b) Task processing
	def action2( *args): # selected path will be accessed via pvar 
		_task=gui.nextTask( 'button_2' )
		if _task==None: return 
		gui.frames['1'].set_textvariable( 'label1', f"Tasks awaiting: {gui.countTasks('button_2')}" )
		gui.frames['1'].set_textvariable( 'label2', f'Result x^x for {_task} = {_task**_task}' )
		# alternative way/old
		# frame_grid[3]['row4'][0]['X']=f'Result x^x for {_task} = {_task**_task}'
		# frame_grid[2]['row3'][0]['X']=f"Tasks awaiting: {gui.countTasks('button_2')}"
		# gui.frames['1'].update_frame( frame_grid,  only_update=True) # adjust widths - on global level checking all rows in columns 
		
	gui.frames['1'].setButtonAction( 'button2', [ ], action2 ) 
	


	# 4. DISPLAY
	gui.root.mainloop()
	