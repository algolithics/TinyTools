# Dialogs - functions and classes 
# Estimated usefulness min 1 max 3 
# Functions:
# - [3] get_path_dialog
# - [3] showinfo
# - [2] combox_opt_dialog
# - [2] ask_password
# - [2] get_path_to_elem - write the path to element of ui eg label
# - [1] display_value_to_copy
# classes:
# - [3] AskConfirmAction
# - [1] ResultOptions

import tkinter as tk
from tkinter import filedialog, StringVar, ttk, messagebox, Toplevel,Scrollbar
import os
from functools import partial


# messagebox.askyesno(a, b)
	
		
# arbitrary number of buttons with different actions fun
# combox_opt_dialog may be better for multiple actions even when doing different fun - is more comples and fun can be if/elsed by selected option 
def custom_dialog( str_title, strlabel, buttons=[{'name':'OK', 'fun':print}], main_loop_exists=True):
	
	newwin=orTopLevel(main_loop_exists) #Toplevel() if main_loop_exists else tk.Tk()	
	newwin.title(str_title)
	newwin.focus_force()
	
	ttk.Label( newwin, text=strlabel).pack(side="top") #lbl.pack(side="top")
	
	
	def click(f):		
		f() #action['fun'](cmboxv.get())
		newwin.destroy()
		
	for b in buttons:
		ttk.Button( newwin,text=b['name'],command=lambda f=b['fun'] : click(f ) ).pack(side='left' )

	if not main_loop_exists:
		newwin.mainloop()

		

def combox_opt_dialog( str_title,options,action={'name':'OK', 'fun':print}, main_loop_exists=True):
	
	newwin=orTopLevel(main_loop_exists) #Toplevel() if main_loop_exists else tk.Tk()	
	newwin.title(str_title)
	newwin.focus_force()
		
	cmboxv=StringVar()
	
	cmbox=ttk.Combobox( newwin,textvariable=cmboxv,values=options, state="readonly")
	cmbox.pack(side = 'left')
	cmboxv.set(options[0])
	
	def okcmd(*args):		
		action['fun'](cmboxv.get())
		newwin.destroy()
		
	okb=ttk.Button( newwin,text=action['name'],command=okcmd)
	okb.pack(side = 'right')
	okb.bind('<Return>',  okcmd )
	okb.focus()

	if not main_loop_exists:
		newwin.focus_force()
		okb.focus_force()
		newwin.mainloop()
		
	




def ask_password(save_pswd_here=[],title='Enter password',lbl='Enter password to decrypt file',fun_run_after=None, main_loop_exists=True):
	
	rootframe=orTopLevel(main_loop_exists) #Toplevel() if main_loop_exists else tk.Tk()	
	rootframe.title(title)
	
	# dpi =  rootframe.winfo_fpixels('1i') 
	# scaling_f=round(dpi/72,4)
	# rootframe.tk.call('tk', 'scaling', scaling_f) #
	
	
	ttk.Label(rootframe,text=lbl).pack(fill='x')
	tmpvar=StringVar()
	
	entr=ttk.Entry(rootframe,textvariable=tmpvar, show='*')
	entr.pack(fill='x') #.bind('<Return>',  get_pass ).focus()
	
	def get_pass(*args): 
		_t=tmpvar.get().strip()
		if len(_t)>0:
			if len(save_pswd_here)==0: save_pswd_here.append(_t)
			else: save_pswd_here[0]=_t
			fun_run_after(save_pswd_here)			
			rootframe.destroy()
	
	ttk.Button(rootframe,text='Enter',command= get_pass  ).pack(fill='x')
	
	entr.bind('<Return>',  get_pass )
	entr.focus()
			
	# def on_closing():
		# if len(tmpval)==0:
			# tmpval.append(' ')
	# rootframe.protocol("WM_DELETE_WINDOW", on_closing)
	if not main_loop_exists:
		rootframe.focus_force()
		entr.focus_force()
		rootframe.mainloop()






		


def get_path_dialog(init_dir=os.getcwd(),str_title="Select path",ptype='file', main_loop_exists=True):
	if not main_loop_exists:
		tmproot=tk.Tk()
		tmproot.call('tk', 'scaling', round( tmproot.winfo_fpixels('1i') /72,4))
		tmproot.overrideredirect(1)
		tmproot.withdraw()
	path=''
	if ptype=='file': path= filedialog.askopenfilename(initialdir =init_dir, title=str_title)
	else: path=filedialog.askdirectory(initialdir=init_dir, title=str_title)
	
	if not main_loop_exists:
		tmproot.destroy()
		
	return path
	




# show info before loading the main root window
def showinfo(fr_title,fr_content, main_loop_exists=True ): 
	if not main_loop_exists:
		tmproot=tk.Tk() 
		tmproot.call('tk', 'scaling', round( tmproot.winfo_fpixels('1i') /72,4))
		tmproot.overrideredirect(1)
		tmproot.withdraw()
	messagebox.showinfo(fr_title,fr_content)
	if not main_loop_exists:
		tmproot.destroy()
	
	
	

# display dialog to allow user to easy copy value calculated by the app
# eg password created 
def display_value_to_copy(strtitle,stroutput, main_loop_exists=True):
	newwin=orTopLevel(main_loop_exists) #Toplevel() if main_loop_exists else tk.Tk()	
	# newwin=Toplevel()
	newwin.title(strtitle)	
	
	entr=ttk.Label( newwin,text=strtitle+'\n\n'+stroutput) #
	entr.pack()

	okb=ttk.Button( newwin,text='Copy',command=partial(copy, entr,stroutput,newwin ))
	okb.pack()
	def _fun(*args):
		# print('aaaa')
		copy(entr,stroutput,newwin)
		# partial(copy, entr,stroutput,newwin )
	okb.bind('<Return>', _fun  )
	okb.focus()
	
	if not main_loop_exists:
		newwin.focus_force()
		okb.focus_force()
		newwin.mainloop()
	
				
						
# copy to clipboard 
def copy(elem,vv, root):
	elem.clipboard_clear() 
	elem.clipboard_append(vv)
	elem.update()
	messagebox.showinfo("Value copied to clipboard", "Value \n "+vv+" \n copied to clipboard. Use it before closing this window." )
	root.destroy()
	


		

def orTopLevel(main_loop_exists=True):
	if main_loop_exists: 
		return Toplevel()
	else: 
		r=tk.Tk()		 
		r.tk.call('tk', 'scaling', round( r.winfo_fpixels('1i') /72,4))
		return r	




# TOPLEVEL requires first to have root window with elements 
# def get_path_to_elem(elem ,ptype='file',init_dir=os.getcwd(),validation_fun=None ):
	# print(elem,ptype,init_dir,validation_fun)
	# while True:		
		# path=''
		# if ptype=='file': path=filedialog.askopenfilename(initialdir=init_dir, title="Select relevant file")
		# elif ptype=='dir':  path=filedialog.askdirectory(initialdir=init_dir, title="Select directory")
		# else: return 
		
		# if path==None: return  
			
		# elif validation_fun==None or validation_fun(path):
			# print('path',path)
			# if type(elem)==type({}): elem['str_val_obj'].set(path) 
			# else: elem.set(str(path))
			# break
		# else:
			# messagebox.showinfo("Path is not correct!", "Select relevant path!" )		
		
		
		
		
	

# TOPLEVEL requires first to have root window 
class AskConfirmAction: # AskConfirmAction(strlabel,cmd, args)

	def __init__(self,strlabel,cmd, args):
		self.confirmed=False
		self.txt=strlabel
		self.newwin=orTopLevel(True) #Toplevel()
		self.newwin.title('Are you sure?')

		lbl=ttk.Label(self.newwin, text=strlabel)
		lbl.pack(side="top")
		
		confirm=ttk.Button(self.newwin,text='YES',command=lambda:self.set_confirm(cmd, args) )
		cancell=ttk.Button(self.newwin,text='NO/CANCELL',command=lambda:self.newwin.destroy())
		
		confirm.pack(side='left' )
		cancell.pack(side='right' )
		
	def set_confirm(self,cmd, args):
	
		if len(args)>0: cmd(*args)
		else: cmd()
		
		self.confirmed=True # this can also be used to run action based on answer
	
		self.newwin.destroy()
	

# PROCESS LIST OF ELEMENTS ACCORDING TO ACTIONS SPECIFIED IN BUTTON
# LIST VIEW ONLY FOR CLARITY
# argument is list of tuples = multiple arguments possible for every button 
# buttons show options what to do with the results (whole list)
# selection of list element does not work with button 
class ActionsOnList: #ResultOptions(strtitle,str_list=[],button_names=[],functions=[], args=[], main_loop_exists=True)

	def __init__(self,strtitle,str_list=[],button_names=[],functions=[], args=[], main_loop_exists=True):
		
		self.newwin=orTopLevel(main_loop_exists) #Toplevel()  if main_loop_exists else tk.Tk()	
		self.newwin.title(strtitle)
		self.newwin.focus_force()

		scrollbar = Scrollbar(self.newwin)
		scrollbar.pack(side = 'right', fill = 'y') 
		
		max_width=max([len(s) for s in str_list])
		
		ylist = tk.Listbox(self.newwin,yscrollcommand = scrollbar.set , width=min(100,max_width+1) , height=min(30,len(str_list))) # 
		def except_binary(tt_str):
			try: ylist.insert('end', tt_str )
			except: ylist.insert('end', tt_str.encode("utf-8"))   
		
		for tt in str_list:
			if type(tt)==type([]): except_binary(str(tt[0] ))  
			else: except_binary(str(tt  )) 
			
		ylist.pack(side = 'top', fill = 'x') 
  
		scrollbar.config( command = ylist.yview )
		
		btns=[]
		
		for ii,btn in enumerate(button_names):
		
			tmpargs=(str_list,)
			if len(args)>0 :
				if args[ii]!=None:
					if type(args[ii])!=tuple:
						args[ii]=tuple(args[ii]) 
					tmpargs=tmpargs + args[ii]  				
			# print(ii,btn,tmpargs)
			btns.append(ttk.Button(self.newwin,text=btn,command=partial(self.run_opt,functions[ii],tmpargs) ) )
			btns[-1].pack(side='top', fill = 'x')
			
		
		if not main_loop_exists:
			self.newwin.mainloop()
			
	def run_opt(self,opt_fun,args):
	
		opt_fun(*args)
	
		self.newwin.destroy()
	