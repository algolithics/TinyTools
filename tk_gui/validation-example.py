# This example show how to create int/float entries with input validations
# Each list element is dict with key = row id
# Each row consists of multiple columns with defined elements eg. Label, Input, Button ... 
# Gui elements are defined using short keys like T:Type, X:Value/Text , uid: unique id to access element, span: extends cell for multiple columns 
	

import tk_base.tk_gui as tkg

if __name__ == '__main__': 

	# 1. INITIALIZE
	gui=tkg.TK_GUI('Title 1')
	
	
	
	# 2. DEFINE GUI ELEMENTS 
	
	frame_grid=[ 
					{'row1': [ {'T':'Label','X':'This example show how to create int/float entries with input validations', 'span':2 }  ] } 
					, {'row2': [ {'T':'Label', 'X':'Int'   }, {'T':'Label', 'X':'Float'   } ] } 
					, {'row3': [ {'T':'InputInt', 'X':''  , 'uid':'entry1'  }, {'T':'InputFloat', 'X':''  , 'uid':'entry2' } ] } 
					, {'row4': [ {'T':'Label', 'X':'Try to enter wrong values and see what happens '  , 'uid':'label1','span':2 } ] } 
					, {'row5': [ {'T':'Button', 'X':'Add'  , 'uid':'button1','span':2 } ] } # spann allows to take 2 columns, expand the element 
				]
			
	gui.addFrame( gridListOfLists=frame_grid,frameId='1' ) # APPLY gui elements to GUI	
	
	# 3. ADD BUTTON ACTION	
	# 3. a) define action function: take value from 1 element and assign to another 
	# arguments will be :  gui.frames['1'], 'entry1', 'entry2','label1'
	# frame is the object containing elements accessed by uids defined above : 'entry1', 'entry2','label1'
	def action1(frame , elem1, elem2, labelOutput):  
		e1=frame.get_value(elem1)
		e2=frame.get_value(elem2) 
		tmp=  round(e1+ e2,max( (len(str(e1)), len(str(e2)) ) ) )
		frame.set_textvariable( uid=labelOutput, x_val=f'Sum is {tmp}')  
		
	# 3. b) ASSIGN TO ELEMENT: FRAME->BUTTON 
	gui.frames['1'].setButtonAction( 'button1', [ gui.frames['1'], 'entry1', 'entry2','label1'  ], action1 ) # action fun default arg is uid of element of action 
	
	
	
	# 4. DISPLAY
	gui.root.mainloop()
	
	