# THIS EXAMPLE SHOW HOW TO USE STANDALONE DIALOGS
# SOEMTIMES THIS MAY BE ENOUGH EG WHEN YOU ASK USER A PATH FILE TO CLICK

# DIALOGS CAN ALSO BE PLUGGED TO BUTTONS OF MAIN WINDOW - IN THAT CASE THESE ARE NOT STANDALONE: MAIN_LOOP_EXISTS=TRUE
# SEE OTHER EXAMPLES 

import tk_base.tk_dialogs as dialogs


def main():
	
	
	# 1. SIMPLE INFO
	dialogs.showinfo('Here is a sequence of dialogs','Sometimes simple dialogs can be enough for the app \nand no need for complicated gui',main_loop_exists=False )
	
	
	# 2. ASK PASSWORD
	pswd=[] # fun = lambda x: dialogs.showinfo('Your password is ... ',x ) will take care of the work with provided password 
	dialogs.ask_password(pswd, 'Enter password', 'Enter password ',lambda x: dialogs.showinfo('Your password is ... ',str(x) ), False)
	
	
	# 3. ASK FILE 
	path=dialogs.get_path_dialog( str_title="Select a file",main_loop_exists=False)
	
	
	# 4. COPY TO CLIPBOARD VALUE CREATED BY APP
	dialogs.display_value_to_copy('Here you can copy to clipboard',path,main_loop_exists=False)


	# 5. COMBOBOX OPTIONS SELECT + ACTION FOR SELECTED OPTION 
	_act={'name':'OK','fun':lambda x: dialogs.showinfo('Selected ',x ) }
	dialogs.combox_opt_dialog( 'Select an action',options=[1,2], action=_act ,main_loop_exists=False)
	
	# 6. Processing list of strings with multiple options / buttons  [print,lambda x,y: print(str(x).split(),y)]
	buttons_actions=[lambda x,y: dialogs.showinfo('Selected action: ','Selected action with argument '+y ), lambda x,y: dialogs.showinfo('Selected action: ','Selected action with arg '+y )]
	dialogs.ActionsOnList(strtitle='Select',str_list=['asdf','qwer','1234','!@#$'],button_names=['A','B'],functions=buttons_actions, args=[('z for A',),('x for B',)], main_loop_exists=False)
	 
	 
	# 7. Custom dialog
	# list a is needed to pass the value / selected by clicking button 
	a=[] 
	# function will set up value 
	def f(x): a.append(x) 
	
	# define N buttons: list of dict [{name:, fun:},]
	_buttons=[{'name':str(ii), 'fun':lambda x=ii: f(x)} for ii in range(1,7)]
	
	dialogs.custom_dialog( 'Custom dialog', 'Click a button', buttons=_buttons, main_loop_exists=False)
	dialogs.showinfo('Thanks',f'Thank you for clicking {a[0]}',main_loop_exists=False )
	
	# return 


if __name__ == '__main__': 
	main()
	
	