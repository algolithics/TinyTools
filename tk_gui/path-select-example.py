# THIS EXAMPLE SHOWS:
# 1. HOW TO SELECT FILE PATH TO A VARIABLE AND USE IT / CHECK BUTTON1
# 2. SELECT DIR PATH AND SAVE INSIDE ANOTHER ELEMENT + 
# 3. FRame update to adjust width and sisplay full path


import tk_base.tk_gui as tkg
from tk_base.tk_dialogs import  get_path_dialog, showinfo

if __name__ == '__main__': 

	# 1. INITIALIZE
	gui=tkg.TK_GUI('Path selection')
	
	
	
	# 2. DEFINE GUI ELEMENTS 
	frame_grid=[ 
					{'row1': [ {'T':'Button', 'X':'Select a file'  , 'uid':'button1'  } ] }
					, {'row2': [ {'T':'Button', 'X':'Select a folder path to place below'  , 'uid':'button2'  } ] }
					, {'row3': [ {'T':'Label', 'X':''  , 'uid':'label1' } ] }  
					, {'row4': [ {'T':'Button', 'X':'Check file path'  , 'uid':'button3' } ] }  
				]
			
	gui.addFrame( gridListOfLists=frame_grid,frameId='1' ) # APPLY gui elements to GUI	
	
	
	
	# 3. ADD BUTTON ACTIONs	
	# 3. a) SAVE FILE PATH TO A VARIABLE 
	pvar=[] 
	def action1(str_title,ptype,*args): # selected path will be accessed via pvar 
		fp=get_path_dialog( str_title=str_title,ptype=ptype )
		if len(pvar)==0: pvar.append(fp)
		else: pvar[0]=fp
	
	gui.frames['1'].setButtonAction( 'button1', ["Select a file path",'file' ], action1 ) 
	
		
	# 3. b) SAVE DIR PATH TO LABEL ELEMENT
	def action2(str_title,ptype,*args): # selected path will be accessed via pvar 
		_p=get_path_dialog( str_title=str_title,ptype=ptype )
		frame_grid[2]['row3'][0]['X']=_p
		gui.frames['1'].update_frame( frame_grid,  only_update=True) # adjust widths - on global level checking all rows in columns 
		
	gui.frames['1'].setButtonAction( 'button2', [ "Select a file path", 'dir'], action2 ) 
	
	
	# 3. c) CHECK value saved in variable 
	gui.frames['1'].setButtonAction( 'button3', [ "Selected file path", pvar , True], showinfo ) 



	# 4. DISPLAY
	gui.root.mainloop()
	
	