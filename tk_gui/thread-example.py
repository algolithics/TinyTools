# THIS EXAMPLE SHOWS HOW TO USE THREAD AND QUEUE
# TASKS ARE PRODUCES BY USER CLICKING BUTTON
# AND PROCESSED IN A THREAD 

import tk_base.tk_gui as tkg
from tk_base.tk_dialogs import  get_path_dialog, showinfo
from datetime import datetime
from time import sleep as t_sleep

if __name__ == '__main__': 

	# 1. INITIALIZE
	gui=tkg.TK_GUI('Thread + queue',data_receivers=['button_2'])
	
	
	
	# 2. DEFINE GUI ELEMENTS 
	frame_grid=[ 
					{'row1': [ {'T':'Button', 'X':'Create new task'  , 'uid':'button1'  } ] } 
					, {'row2': [ {'T':'Label', 'X':'Tasks awaiting: 0'  , 'uid':'label1' } ] }   
					, {'row3': [ {'T':'Label', 'X':''  , 'uid':'label2' } ] }     
					, {'row4': [ {'T':'Label', 'X':''  , 'uid':'label3' } ] }   
				]
			
	gui.addFrame( gridListOfLists=frame_grid,frameId='1' ) # APPLY gui elements to GUI	
	
	
	
	# 3. BUTTON CREATE TASK AND REFRESH UI
	tmpval=0
	def action1( *args): # selected path will be accessed via pvar 
		global tmpval
		gui.addTask(receiver='button_2',task=tmpval) 
		tmpval+=1
		_x=gui.countTasks('button_2') 
		frame_grid[1]['row2'][0]['X']=f"Tasks awaiting: {_x}"
		frame_grid[3]['row4'][0]['X']=f"{datetime.now()}"
		gui.frames['1'].update_frame( frame_grid,  only_update=True) # adjust widths - on global level checking all rows in columns 
	
	gui.frames['1'].setButtonAction( 'button1', [ ], action1 ) 
	


	# 4. THREAD PROCESSING TASKS AND REFRESHING UI
	# processing 1 task per queue per 1 second/ 1 iter
	def thread_action( ):	
		while True:
			for k,v in gui.items(): # process ALL queues - general example queues.
				if k in ['button_2']: # here process only  button_2 - more general if you want more 
					_task=gui.nextTask( k  ) 
					if _task==None: # BE SURE TO ESCAPE NONE TASK
						continue
					# print(f'\tProcessing {_task}')	
					frame_grid[2]['row3'][0]['X']=f'Result x^x for {_task} = {_task**_task}'
					frame_grid[1]['row2'][0]['X']=f"Tasks awaiting: {gui.countTasks('button_2')}" 
					gui.frames['1'].update_frame( frame_grid,  only_update=True) # adjust widths - on global level checking all rows in columns 
					
			gui.frames['1'].set_textvariable('label3',datetime.now())
			t_sleep(1)
			
	gui.startThread( thread_action  )
	
	
	
	# 5. DISPLAY
	gui.root.mainloop()
	