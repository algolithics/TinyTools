
import numpy as np
from numpy import unique as np_unique
import  sets_simi as setsim
from time import perf_counter
# import modules.sets_simi.sets_simi as setsim


rng = np.random.default_rng(12345)

ints1 = rng.integers(low=9876543210, high=9876543210+200, size=(10,100))
ints2 = rng.integers(low=9876543210, high=9876543210+70, size=(1000,100))

ints=np.concatenate( (ints1,ints2), axis=0)
 
ints_fin=[]  
for ii in range(ints.shape[0]):
	tmp=set(np_unique(ints[ii]))
	ints_fin.append( tmp ) 
	

cmp_res=[]

l_ints_fin=len(ints_fin) 
t1=perf_counter()
for ii in range(l_ints_fin):  
	base= ints_fin[ii]
	tmpdict={'rate':{}} 
	  
	setsim.findSimilarSets(0.9,2,base,ints_fin[ii+1:],tmpdict) # 1.4 sec
	cmp_res.append(tmpdict)  
t2=perf_counter()

print('all done sec: ',t2-t1, 'sets:',len(ints_fin))