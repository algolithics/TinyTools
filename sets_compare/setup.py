# python setup.py build_ext --inplace
 
from distutils.core import setup, Extension
from Cython.Build import cythonize
# import numpy


setup(
    ext_modules=cythonize("sets_simi.pyx")
    #,include_dirs=[numpy.get_include()]
) 