# TABLE class goal:
# place with code ready to apply / memoryview
# easy to use signle class covering 99% Table needs
# fixed pandas join to alow SQL style Nnull handling
# each Table can have unique name, when printing more easy to debug
# convenient and easy way to make table operations: sort, filter, aggregate, derive column

# SEE USAGE EXAMPLES AT THE BOTTOM
	# new 		     

# TABLE class functions: 
	# MAIN/TABULAR:
			# append
			# , apply
			# ,  aggregate
			# , sort
			# , filtered
			# , sort
			# , join 
			# ,  deriveColumn
	# VIEWRS: 
			# prints
			# , printa
			# , isEmpty
	# VALUES CORRECTION: 
			# replaceNa
			# , dropNa
			# , renameColumns
			# , deleteColumns
			# addColumns
			#  deleteRows
			# insertRow
			# updateValue
			# , reorderColumns
			# , changeColumnIndex
			# , removeDupli
	# READERS: 
			# fromDict
			# , fromCsvFile
	# GETTERS: 
			# copy
			# , filtered
			
# ADDITIONAL functions outsdide Table class:
	# def strip_spaces(x)
	# def extractFloatFromPrctStr(x,_round=4): 
	# def scaleFloatsAndRound(x,s=1.0,_round=6):
	# def scaleInts(ii,s=1,_round=6): 
	# def parseInt(ii):