
# TABLE class goal:
# place with code ready to apply / memoryview
# easy to use signle class covering 99% Table needs
# fixed pandas join to alow SQL style Nnull handling
# each Table can have unique name, when printing more easy to debug
# convenient and easy way to make table operations: sort, filter, aggregate, derive column

# SEE USAGE EXAMPLES AT THE BOTTOM
	# new 		     

# TABLE class functions: 
	# MAIN/TABULAR:
			# append
			# , apply
			# ,  aggregate
			# , sort
			# , filtered
			# , sort
			# , join 
			# ,  deriveColumn
	# VIEWRS: 
			# prints
			# , printa
			# , isEmpty
	# VALUES CORRECTION: 
			# replaceNa
			# , dropNa
			# , renameColumns
			# , deleteColumns
			# addColumns
			#  deleteRows
			# insertRow
			# updateValue
			# , reorderColumns
			# , changeColumnIndex
			# , removeDupli
	# READERS: 
			# fromDict
			# , fromCsvFile
	# GETTERS: 
			# copy
			# , filtered
			
# ADDITIONAL functions outsdide Table class:
	# def strip_spaces(x)
	# def extractFloatFromPrctStr(x,_round=4): 
	# def scaleFloatsAndRound(x,s=1.0,_round=6):
	# def scaleInts(ii,s=1,_round=6): 
	# def parseInt(ii):


from  traceback import  format_exc as traceback_format_exc
from pandas import read_csv, concat as pandas_concat, DataFrame, merge as pandas_merge

# operators for filtering
from operator import lt as op_lt, le as op_le, eq as op_eq, ne as op_ne, ge as op_ge, gt as op_gt
from operator import abs as op_abs, add as op_add, mul as op_mul, mod as op_mod, pow as op_pow, sub as op_sub, truediv as op_truediv, floordiv as op_floordiv, concat as op_concat
from operator import indexOf as op_indexOf, contains as op_contains, length_hint as op_length_hint
from modules.randNames import RandName
# from randNames import RandName

class Table:

	def __init__(self,table_name='',df=DataFrame()):  
		self.df=df
		self._r=RandName()
		if table_name=='':
			table_name=self._r.getName( 8, 8 )
			print(f'\nCreated table [{table_name}]')
			
		self.table_name=table_name # self.table_name self._r.getName( 8, 8 )
		
		# operators map to be able to pass oparations for filtering and column derivation/creation based on existing columns 
		
		def _has(x2,x1):#print(f'checking {x1} in {x2}')
			return str(x1) in str(x2)
			
		def _pos(x1,x2):
			return x2.find(x1)
			
		self.operator_map={ '==':op_eq # for filtering mainly
							, '!=':op_ne
							, '<':op_lt
							, '<=':op_le
							, '>':op_gt
							, '>=':op_ge
							
							, '+':op_add # popular operations 
							, '*':op_mul
							, '%':op_mod
							, '^':op_pow
							, '-':op_sub
							, '/':op_truediv
							, '//':op_floordiv
							, 'abs':op_abs
							
							, 'has': _has #string operations	
							, 'pos': _pos							
							, 'len':op_length_hint#string operations
							, '|':op_concat #string operations
							}
		
	def isEmpty(self):
		return self.df.empty
			
	def replaceNa(self,val_or_ddict={'columns':'value'}):
		if type(val_or_ddict)!=dict: self.df.fillna(val_or_ddict, inplace=True)
		else: self.df.fillna(value=val_or_ddict, inplace=True)
			
	def dropNa(self, columns=[], _axis='rows', _how='all' ): # change to any to remove if any column has NA
		if len(columns)>0:
			self.df.dropna(axis=_axis, how=_how, subset=columns, inplace=True)
		else:
			self.df.dropna(axis=_axis, how=_how, inplace=True)
			
	def renameColumns(self,ddict={'old_name':'new_name'}, _print=True ):
		if _print: print(f"\nChanged df columns from {self.df.columns}")
		self.df.rename(columns=ddict, inplace=True)
		if _print: print(f"\tto {self.df.columns}")
	
	def deleteColumns(self,cols=[]):
		try:
			self.df.drop(columns=cols, inplace=True)
		except KeyError:
			tmp=traceback_format_exc()
			print(f"\nCould not drop columns {cols}, error: {tmp}")
			
	def addColumns(self,cols={'newcolname':'value'}):
		for k,v in cols.items():
			if k in self.df:
				print(f"\nCould not add columns {k}, already exists - pass.")
				continue
			try:
				self.df[k]=v
			except KeyError:
				tmp=traceback_format_exc()
				print(f"\nCould not add columns {cols}, error: {tmp}")
		
	# rows index same as for filtering 
	# rows_indexes=self.getIndexesForConditions( cond=[{'column':'name','operator':'==','value':'df_column_name2'}] )
	def deleteRows(self,rows_indexes=[]):
		try:
			self.df.drop(rows_indexes, inplace=True)
		except KeyError:
			tmp=traceback_format_exc()
			print(f"\nCould not drop rows {rows_indexes}, error: {tmp}")
			
	def insertRow(self,values_map={'col':'val'},atIndex=None):
		if atIndex==None:
			atIndex=self.df.index.max()+1
		columns=values_map.keys()
		values=values_map.values()
		# self.df[columns].loc[atIndex] = values
		self.df.loc[atIndex, columns] = values
		
	def updateValue(self,new_value,column,row,byIdx=False):
		try:
			if byIdx: self.df.loc[row,column]=new_value
			else: 
				# column=[ for c in self.df.columns]
				cidx=self.df.columns.get_loc(column)
				self.df.iloc[row,cidx]=new_value
		except KeyError:
			tmp=traceback_format_exc()
			print(f"\nCould not update value for column {column} row {row}, error: {tmp}")
		
	def reorderColumns(self,columns=[]): # all columns reorder 
		self.df = self.df.reindex(columns=columns, copy=False)
		
	def changeColumnIndex(self,col,ii): # for single column reorder
		self.df.insert(ii, col, self.df.pop(col))

	def removeDupli(self, _subset=None, _keep='first', _ignore_index=True):
		self.df.drop_duplicates(subset=_subset, keep='first', inplace=True, ignore_index=_ignore_index)
	
	def append(self, df2append, _ignore_index=True):	
		if type(df2append)==Table:
			df2append=df2append.df
		self.df= pandas_concat([self.df, df2append], ignore_index=_ignore_index)	
		
	# func example: strip_spaces, extractFloatFromPrctStr
	def apply(self,func,toColumn, *args):
		try:
			tmp=None 
			if len(args)>0:
				tmp=self.df[toColumn].apply(func,args=args)
			else:
				tmp=self.df[toColumn].apply(func )
				
			self.df[toColumn]=tmp
		except:
			print(f'Could not apply {func} to df column {toColumn}')
			tmp=traceback_format_exc()
			print(tmp)
	
	# return copy or partial copy 
	def copy(self,columns=[],rows=[],byIdx=False):
		tmp=DataFrame()
		if len(rows)>0:
			if byIdx:
				try:
					tmp=self.df.loc[rows].copy(deep=True)
				except  KeyError:
					return Table(f'error at selectRowsByIndex_{self.table_name}_{self._r.getName( 8, 8 )}')
			else:
				tmp=self.df.iloc[rows].copy(deep=True)
		else: tmp=self.df.copy(deep=True)
		
		if len(columns)>0: tmp=stmp(columns).copy(deep=True)
		
		return Table(f'{self._r.getName( 8, 8 )}_copy_of_{self.table_name}',tmp)
	
	# fun= [sum,count,max,min,median,]
	def aggregate(self,groupBy=[], col_fun_map={'column1':['fun1'], 'column2':['fun2']}, _as_index=False):
		dfa= self.df.groupby(groupBy,as_index=_as_index).agg(col_fun_map )
		# flatten levels of dun
		# print(dfa.columns.get_level_values(0), dfa.columns.get_level_values(1))
		# print(zip(dfa.columns.get_level_values(0), dfa.columns.get_level_values(1)))
		
		tmp_cols=[]
		for zz in zip(dfa.columns.get_level_values(0), dfa.columns.get_level_values(1)):
			tmp=zz[0].lower().replace(' ','_')
			if zz[1]!='':
				tmp+='_'+zz[1].lower().replace(' ','_')
			tmp_cols.append(tmp)
		dfa.columns=tmp_cols
		# [zz[0].lower().replace(' ','_')+'_'+zz[1].lower().replace(' ','_') for zz in zip(dfa.columns.get_level_values(0), dfa.columns.get_level_values(1))]
		
		return Table(f'{self._r.getName( 8, 8 )}_aggregate_{self.table_name}',dfa)
		# return dfa
		
	def printa(self, sel_column=[]):
		print(f'\n ==== Table [{self.table_name}] ALL ROWS ====')
		if len(sel_column)>0: print(self.df[sel_column].to_string() )
		else: print(self.df.to_string() )
		print('\nLENGTH',len(self.df))
		print('\nTYPES\n\n',self.df.dtypes)
		print('\nMissing values N/A per column:')
		print(self.df.isna().sum())
		print('\n\n')
		
	def prints(self,_head=3,_tail=3, _index=False):
		print(f'\n ==== Table [{self.table_name}] sample ====')
		print('HEAD\n',self.df.head(_head).to_string(index=_index))
		colnames=self.df.columns
		cnew=[]
		for c in colnames:
			cnew.append(''.join(['.' for ii in range(len(str(c)))]))
			
		print('TAIL',self.df.tail(_tail).to_string(index=_index, header=cnew))
		print('\nLENGTH',len(self.df))
		print('\nTYPES\n',self.df.dtypes)
		print('\nMissing values N/A per column:')
		print(self.df.isna().sum())
		print('\n\n')
		
	def fromDict(self,ddict={'column_name1':['values'], 'column_name2':['values2']}):
		self.df=DataFrame(ddict)
		
		
	# most often you should passs:
	# 'header' / row num with col names startign with 0, default None - no col names 
	# 'delimiter' / default ;
	# OPT EXCLUDED ERROR PRONE:
	# , names=csv_read_cfg["set_columns_names"] --- 'set_columns_names':None #df[names] True will infer first line, [] will get col names from this config, None 
	# date_parser=lambda x: pandas.datetime.strptime(x, '%Y-%m-%d %H:%M:%S') 
	def fromCsvFile(self,fpath, cur_cfg={}):
		#defaults
		csv_read_cfg={ 'date_columns_to_parse':[]
						, 'dayfirst':False #DD/MM format dates, international and European format.
						, 'infer_datetime_format':False
						, 'delimiter':';'
						, 'index_col':False #  index_col=False force exclude index 
						, 'read_columns_list':None #usecols can be also numbers 
						, 'header':None # header row number - if no col names None 
						, 'dtype':{} # single value or E.g. {‘a’: np.float64, ‘b’: np.int32, ‘c’: ‘Int64’} 
						, 'true_values':['True','true']
						, 'false_values':['False','false']
						, 'skiprows':[] # int or list 
						, 'nrows': None # int 
						, 'quotechar':None # to be able to use delimiter inside values 
						, 'doublequote':False
						, 'comment':None #will exclude the line or anything after in the line
						, 'na_check':True   #na_filter - if sure no NA in the file may read faster using False
						,'encoding':'utf-8'
						# , 'on_bad_lines':'error' #‘error’, ‘warn’, ‘skip’
						# ,'na_values':
						# By default the following values are interpreted as NaN: ‘’, ‘#N/A’, ‘#N/A N/A’, ‘#NA’, ‘-1.#IND’, ‘-1.#QNAN’, ‘-NaN’, ‘-nan’, ‘1.#IND’, ‘1.#QNAN’, ‘<NA>’, ‘N/A’, ‘NA’, ‘NULL’, ‘NaN’, ‘n/a’, ‘nan’, ‘null’.
						}
						
		# overwrite default:
		for k,v in cur_cfg.items():
			if k in csv_read_cfg:
				csv_read_cfg[k]=v
			else:
				print(f'\n*** Bad key in csv_read arg cur_cfg [{k}]\n')
				return
		
		if csv_read_cfg['quotechar']==None:
			del csv_read_cfg['quotechar']
			del csv_read_cfg['doublequote']
		
		try:
			if 'quotechar' in csv_read_cfg:
				self.df= read_csv(fpath
								, delimiter=csv_read_cfg["delimiter"]
								, parse_dates=csv_read_cfg["date_columns_to_parse"]
								, dayfirst=csv_read_cfg["dayfirst"]
								, usecols=csv_read_cfg["read_columns_list"]
								, dtype=csv_read_cfg["dtype"]
								, true_values=csv_read_cfg["true_values"]
								, false_values=csv_read_cfg["false_values"]
								, skiprows=csv_read_cfg["skiprows"]
								, nrows=csv_read_cfg["nrows"]
								, quotechar=csv_read_cfg["quotechar"]
								, doublequote=csv_read_cfg["doublequote"]
								, comment=csv_read_cfg["comment"]
								, na_filter=csv_read_cfg["na_check"]
								, encoding=csv_read_cfg["encoding"]
								 , header=csv_read_cfg["header"]
								, infer_datetime_format=csv_read_cfg["infer_datetime_format"]
								, index_col=csv_read_cfg["index_col"])
			else:
				self.df= read_csv(fpath
						, delimiter=csv_read_cfg["delimiter"]
						, parse_dates=csv_read_cfg["date_columns_to_parse"]
						, dayfirst=csv_read_cfg["dayfirst"]
						, usecols=csv_read_cfg["read_columns_list"]
						, dtype=csv_read_cfg["dtype"]
						, true_values=csv_read_cfg["true_values"]
						, false_values=csv_read_cfg["false_values"]
						, skiprows=csv_read_cfg["skiprows"]
						, nrows=csv_read_cfg["nrows"]
						, comment=csv_read_cfg["comment"]
						, na_filter=csv_read_cfg["na_check"]
						, encoding=csv_read_cfg["encoding"]
						 , header=csv_read_cfg["header"]
						, infer_datetime_format=csv_read_cfg["infer_datetime_format"]
							, index_col=csv_read_cfg["index_col"])
			
		except:
			tmp=traceback_format_exc()
			print(tmp)
			print('\n\tCould not read ',fpath) 
			
			if 'ValueError:' in tmp:
				tmp2=tmp.split('ValueError:')
				print('\n\tWrong column name to parse date? or dtype?',tmp2[1])
			
	def sort(self,ddict={'column':'direction'}): #columns,direction='asc'
		cols,dirs=[],[]
		for k,v in ddict.items():
			cols.append(k)
			if v=='asc': v=True
			elif v=='desc': v=False
			else:
				print(f"Wrong sorting direction {v} should be [asc] or [desc] for column {k}")
				return
			dirs.append(v)
		self.df.sort_values(cols,ascending=dirs, inplace=True)
	
	# derive column using sequence of calculations - every next step is using resulting column as first argument for next step 
	# save_steps=True good for debuging steps of column creation
	def deriveColumn(self,new_column, calc_sequence={'column':'name', 'seq':[{'operator':'==','value':'df_column_name2'}]}, save_steps=False ):
	# for alternative use multi columns: self.df.apply(lambda row: row.['Cost'] - (row.['Price'] * 0.1), axis = 1)
						
		must_have=('column','seq') # cover init exceptios
		for kk in must_have:
			if kk not in calc_sequence:
				print(f"Missing key [{kk}] in calculate [{calc_sequence}], must have all of {must_have}")
				return 				
		
		if save_steps and len(calc_sequence['seq'])<2:
			save_steps=False				
				
		new_column_orig=new_column		
		step_ii=1	
		cur_step_name=new_column
		steps_columns=[]
		if save_steps:
			cur_step_name=	new_column_orig+'_'+str(step_ii)
			steps_columns.append(cur_step_name)
			new_column=cur_step_name
		self.df[new_column]=self.df[calc_sequence['column']]	# init column to update values every next step 
		
		must_have2=('operator','value')
		for calculate in calc_sequence['seq']:			
			_op=self.operator_map[calculate['operator']]
				
			if calculate['operator'] not in self.operator_map:
				print(f"Wrong operator in filtered [{calculate['operator']}], should be one of {self.operator_map.keys()}")
				return 
			
			single_val=True # if sequence needs different handling 
			init_v=calculate['value']
			if type(init_v)==str and 'df_column_' in init_v:
				init_v=init_v.replace('df_column_','')
				if init_v not in self.df:
					print(f"Wrong column definition in [{calculate['value']}], infers {init_v} that deos not exist in {self.df.columns}")
					return
				init_v=self.df[init_v] # column 
				single_val=False
					
			
			# STR LEN
			if _op==op_length_hint: 
				self.df[cur_step_name]=self.df[new_column].apply( (lambda row: _op(str(row))) ) # via lambda calculates actual length 
			
			# STR POS
			elif calculate['operator']=='pos':
			
				if single_val:	
					self.df[cur_step_name]=self.df[new_column].apply(lambda row: str(row)).apply(_op , args=(str(init_v),) ) 
				else:
					init_v=DataFrame({'c0':init_v.apply(lambda row: str(row)).tolist(), new_column:self.df[new_column].apply(lambda row: str(row)).tolist()})
					tmp=init_v.apply(lambda row: _op(row[0],row[1]) ,axis=1)
					self.df[cur_step_name]= tmp.tolist() 					
					
			# CONCAT STR		
			elif _op==op_concat: # cast string to join any values 
			
				if single_val:
					self.df[cur_step_name]=self.df[new_column].apply(lambda row: str(row)).apply(_op , args=(str(init_v),) ) 
				else: 
					self.df[cur_step_name]=_op( self.df[new_column].apply(lambda row: str(row)) , init_v.apply(lambda row: str(row) ) ) 				
				
			# STR HAS STR	
			elif calculate['operator']=='has' : 
				if single_val:			
					self.df[cur_step_name]=self.df[new_column].apply(_op , args=(init_v,) ) 
				else:
					init_v=DataFrame({'c0':init_v.tolist(), new_column:self.df[new_column].tolist()})
					tmp=init_v.apply(lambda row: _op(row[1],row[0]) ,axis=1)
					self.df[cur_step_name]= tmp.tolist() 
			
			# OTHER/MATH OPERATIONS			
			else:
				if single_val:
					self.df[cur_step_name]=self.df[new_column].apply(_op , args=(init_v,) )  #
				else:
					self.df[cur_step_name]=_op(self.df[new_column],init_v)
			
			if save_steps: # FOR STEP DEBUG
				new_column=cur_step_name
				step_ii+=1	
				cur_step_name=	new_column_orig+'_'+str(step_ii)	
				steps_columns.append(cur_step_name) 
		
		if save_steps: # if steps saved rename final column the the chosen name 
			self.df.rename(columns={new_column:new_column_orig}, inplace=True)
	
		return steps_columns[:-2] # return columns to delete easy 
	

	def getIndexesForConditions(self,cond=[{'column':'name','operator':'==','value':'df_column_name2'}] ):
		result=list(True for ii in range(len(self.df))) #self.df[ (self.df.columns[0]==self.df.columns[0] )] # any better way to generate series of True ? 
		must_have=('column','operator','value')
		
		for c in cond:
			for kk in must_have:
				if kk not in c:
					print(f"Missing key [{kk}] in filtered condition [{c}], must have all of {must_have}")
					return 
		
			init_v=c['value']
			if type(init_v)==str and 'df_column_' in init_v:
				init_v=init_v.replace('df_column_','')
				init_v=self.df[init_v] # column 
				
			if c['operator'] not in self.operator_map:
				print(f"Wrong operator in filtered [{c['operator']}], should be one of {self.operator_map.keys()}")
				return 
				
			result=[bool(a*b) for a,b in zip(result,self.operator_map[c['operator']](self.df[c['column']] , init_v).tolist()) ]
		return self.df.loc[result].index



	def filteredNA(self,columns=[]):
		if len(columns)>0:
			return Table(f'{self._r.getName( 8, 8 )}_filtered_{self.table_name}',self.df[self.df[columns].isna().any(axis=1)].copy(deep=True))
		else: # all
			return Table(f'{self._r.getName( 8, 8 )}_filtered_{self.table_name}',self.df[self.df.isna().any(axis=1)].copy(deep=True))
	# if OR needed - concat
	# if value pattern df_colum_ name2 will be used 
	# op operators = < > !=
	def filtered(self, cond=[{'column':'name','operator':'==','value':'df_column_name2'}]):
		
		indx=self.getIndexesForConditions(cond)
		return Table(f'{self._r.getName( 8, 8 )}_filtered_{self.table_name}',self.df.loc[indx].copy(deep=True))
		
		# return Table(f'filtered_{self.table_name}_{self._r.getName( 8, 8 )}',self.df.loc[result].copy(deep=True))
		 # self.table_name self._r.getName( 8, 8 )

	######### FIXED _sql_nulls=True
	# - for outer left/right join nulls are not matched just concat / appended 
	# - for inner excluded 
	# originally DF works like this :
	# If both key columns contain rows where the key is a null value
	# , those rows will be matched against each other. 
	# This is different from usual SQL join behaviour and can lead to unexpected results.
	################
	# on either list of columns existing in both df'sample#
	# OR dict {oring:new, ... }
	# how{‘left’, ‘right’, ‘outer’=full, ‘inner’, ‘cross’}, default ‘inner’
	def join(self,df2join,join_type='inner', on_columns={}, _suffixes=('', '_new'), _sql_nulls=True ): #, left_on=None, right_on=None
		if type(df2join)==Table:
			df2join=df2join.df
		
		if join_type=='cross': 
			self.df= pandas_merge(self.df, df2join, how=join_type, suffixes=_suffixes )
			return
		
		if join_type=='full': 
			join_type='outer'
			
		### REWRITE ON TO LEFT RIGHT
		_left=[]
		_right=[]
		if type(on_columns)==dict:
			for k,v in on_columns.items():
				_left.append(k)				
				_right.append(v)				
		elif type(on_columns) in (list,tuple):
			for ee in on_columns:
				_left.append(ee)				
				_right.append(ee)	
				
		for ii in range(len(on_columns)): # VALIDATE LEFT RIGHT
			if _left[ii] not in self.df:
				print(f"Wrong column to join not existing in current DF [{_left[ii]}] should be one of {self.df.columns}")
				return
					
			if _right[ii] not in df2join:
				print(f"Wrong column to join not existing in new DF [{_right[ii]}] should be one of {df2join.columns}")
				return
				
		if _sql_nulls:	
			na_tf=list(False for ii in range(len(self.df)))
			for ll in _left:
				na_tf=[bool(a + b) for a,b in zip(na_tf,self.df[ll].isna().tolist()) ]
			left_na_df=self.df[na_tf] 
			not_na_left=self.df[[not elem for elem in na_tf]]
			
			
			na_tf=list(False for ii in range(len(df2join)))
			for ll in _right:
				na_tf=[bool(a + b) for a,b in zip(na_tf,df2join[ll].isna().tolist()) ]
			right_na_df=df2join[na_tf] 
			not_na_right=df2join[[not elem for elem in na_tf]]
				
			# now join only non-na:
			tmp_df=pandas_merge(not_na_left, not_na_right, how=join_type, suffixes=_suffixes, left_on=_left, right_on=_right)
			tmp_df_na_left=DataFrame()
			tmp_df_na_right=DataFrame() 
			if join_type in ('outer','left'):
				tmp_df_na_left=pandas_merge(left_na_df, not_na_right.iloc[[0]], how='left', suffixes=_suffixes, left_on=_left, right_on=_right)
			 
			if join_type in ('outer','right'):
				tmp_df_na_right=pandas_merge(not_na_left.iloc[[0]], right_na_df, how='right', suffixes=_suffixes, left_on=_left, right_on=_right)
			
			self.df= pandas_concat([tmp_df, tmp_df_na_left, tmp_df_na_right], ignore_index=True)
			
		else: # pandas merge / not sql compatibile
			self.df= pandas_merge(self.df, df2join, how=join_type, suffixes=_suffixes, left_on=_left, right_on=_right)
		# print(self.df)
		
		

def strip_spaces(x):
	return x.strip()
	

def extractFloatFromPrctStr(x,_round=4): # gives 0.453
	try:
		return round(float( x.replace('%','').strip() )/100.0, _round)
	except:
		return	
		
def scaleFloatsAndRound(x,s=1.0,_round=6):
	return round(x*s, _round)
	
def scaleInts(ii,s=1,_round=6):
	return int( round(ii*s, 0) )
		
def parseInt(ii):
	return int( round(ii, 0) )
	
	
	
	
def examples():
	t=Table()
	
	cur_cfg={'delimiter':','
			, 'infer_datetime_format':True
			, 'date_columns_to_parse':['dates']
			, 'comment':'#'
			, 'skiprows':[0]
			, 'header':0
			, 'index_col':'id'
			}
	t.fromCsvFile('test.csv', cur_cfg) # READ FROM CSV
	t.prints(_index=True)
	
	tt=t.copy(  rows=[ii for ii in range(50,106)],byIdx=False) # COPY SELECTED
	
	
	todel=tt.deriveColumn( 'aaa', calc_sequence={'column':'numbers', 'seq':[{'operator':'^','value':2}
												, {'operator':'|','value':'df_column_numbers'}
												, {'operator':'pos','value':'df_column_numbers'}
												, {'operator':'len','value':4}]}, save_steps=True )
	tt.printa( )
	
	# SQL JOIN
	t.join(  tt ,join_type='full', on_columns={'category':'category','numbers':'numbers'}, _suffixes=('', '_new'), _sql_nulls=True )
	t.prints( )
	
    
	# APPLY FUNC
	t.apply(extractFloatFromPrctStr,'category')
	t.apply(scaleFloatsAndRound,'category',10)
	t.dropNa(columns=['category'])
	t.printa() 
	t.apply(parseInt,'category')
	
	# AGGREGATE
	t4=t.aggregate( groupBy=['category'], col_fun_map={'dates':['count'], 'numbers':['min','max']}, _as_index=False)
	
	# SORT
	t4.sort( {'dates_count':'asc','category':'desc'}) 
	t4.printa()  
	
	# FILTER
	t5=t4.filtered( cond=[{'column':'category','operator':'>','value':2}
						, {'column':'category','operator':'<','value':8}
						, {'column':'dates_count','operator':'>','value':'df_column_numbers_min'}]) # also comparing column values 
	
	t5.sort( { 'category':'desc'})
	t5.printa()  
	
	# ADD COLUMNS with default values 
	t5.addColumns({'zzz':'','qqq':None})
	t5.printa()  
	
	# INSERT at index or row int
	t5.updateValue(7,'category',6,True) # update by index rows
	t5.printa()  
	t5.updateValue(88,'dates_count',0)
	t5.printa()  
	
	# INSERT at index or auto index max +1
	t5.insertRow( values_map={'category':'newcat','dates_count':22 } )
	t5.printa()  
	t5.insertRow( values_map={'category':'newcat2','dates_count':33 },atIndex=0 )
	t5.printa()  
		
	# DELETE	
	t5.deleteRows(0 ) # when knowing index
	t5.printa()  
	# when not knowing the index need to find it first 
	rows_indexes=t5.getIndexesForConditions( cond=[{'column':'dates_count','operator':'==','value':22}] )
	t5.deleteRows( rows_indexes )
	t5.printa()  
	

if __name__ == '__main__':
	examples()
	