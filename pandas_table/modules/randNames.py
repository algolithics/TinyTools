
from random import seed as r_seed, choices as r_choices, randint as r_randint
from string import ascii_letters as s_ascii_letters, digits as s_digits



class RandName:

	def __init__(self, inclSpecial=False): #,minL=1,maxL=9):
		r_seed()
		self._chars=list(s_ascii_letters +  s_digits)
		if inclSpecial:
			self._chars+=list("!#$%&()*+,-./:;<=>?@[\]^_{|}~")
		
	def getInt(self,mmin=0,mmax=9):
		return r_randint(mmin,mmax)
		
	def getName(self,minNameLen=1, maxNameLen=12 ):
		return ''.join(r_choices(self._chars, k=r_randint(minNameLen,maxNameLen) ) ) 
		
		
		
				
def main():
	r=RandName(True)
	print(r._chars,len(r._chars))
	print(r.getName(8,16))
	print(r.getName(4,4))
	print(r.getInt(-32,-5))

if __name__ == '__main__':
	main()
	